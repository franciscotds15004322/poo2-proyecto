package org.franciscoestrada.bean;
import org.franciscoestrada.bean.Proveedor;
import java.util.Date;

public class Compra {
    
    private int idCompra;
    private int numeroDeCompra;
    private Date fechaCompra;
    private Proveedor proveedor;
    private String descripcionCompra;
    private float totalCompra;

    public Compra() {
        
    }

    public Compra(int idCompra, int numeroDeCompra, Date fechaCompra, Proveedor proveedor, String descripcionCompra, float totalCompra) {
        this.idCompra = idCompra;
        this.numeroDeCompra = numeroDeCompra;
        this.fechaCompra = fechaCompra;
        this.proveedor = proveedor;
        this.descripcionCompra = descripcionCompra;
        this.totalCompra = totalCompra;
    }

    public int getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(int idCompra) {
        this.idCompra = idCompra;
    }

    public int getNumeroDeCompra() {
        return numeroDeCompra;
    }

    public void setNumeroDeCompra(int numeroDeCompra) {
        this.numeroDeCompra = numeroDeCompra;
    }

    public Date getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public String getDescripcionCompra() {
        return descripcionCompra;
    }

    public void setDescripcionCompra(String descripcionCompra) {
        this.descripcionCompra = descripcionCompra;
    }

    public float getTotalCompra() {
        return totalCompra;
    }

    public void setTotalCompra(float totalCompra) {
        this.totalCompra = totalCompra;
    }
              
}
