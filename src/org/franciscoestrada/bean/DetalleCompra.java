package org.franciscoestrada.bean;
import org.franciscoestrada.bean.Compra;
import org.franciscoestrada.bean.Producto;

public class DetalleCompra {
    
    private int idDetalleCompra;
    private Compra compra;
    private int lineaNoCompra;
    private Producto producto;
    private String descripcionProductoDC;
    private int unidadesCompradas;
    private float precioCompraUnidad;
    private float totalLineaCompra;

    public DetalleCompra() {
    
    }

    public DetalleCompra(int idDetalleCompra, Compra compra, int lineaNoCompra, Producto producto, String descripcionProductoDC, int unidadesCompradas, float precioCompraUnidad, float totalLineaCompra) {
        this.idDetalleCompra = idDetalleCompra;
        this.compra = compra;
        this.lineaNoCompra = lineaNoCompra;
        this.producto = producto;
        this.descripcionProductoDC = descripcionProductoDC;
        this.unidadesCompradas = unidadesCompradas;
        this.precioCompraUnidad = precioCompraUnidad;
        this.totalLineaCompra = totalLineaCompra;
    }

    public int getIdDetalleCompra() {
        return idDetalleCompra;
    }

    public void setIdDetalleCompra(int idDetalleCompra) {
        this.idDetalleCompra = idDetalleCompra;
    }

    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    public int getLineaNoCompra() {
        return lineaNoCompra;
    }

    public void setLineaNoCompra(int lineaNoCompra) {
        this.lineaNoCompra = lineaNoCompra;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public String getDescripcionProductoDC() {
        return descripcionProductoDC;
    }

    public void setDescripcionProductoDC(String descripcionProductoDC) {
        this.descripcionProductoDC = descripcionProductoDC;
    }

    public int getUnidadesCompradas() {
        return unidadesCompradas;
    }

    public void setUnidadesCompradas(int unidadesCompradas) {
        this.unidadesCompradas = unidadesCompradas;
    }

    public float getPrecioCompraUnidad() {
        return precioCompraUnidad;
    }

    public void setPrecioCompraUnidad(float precioCompraUnidad) {
        this.precioCompraUnidad = precioCompraUnidad;
    }

    public float getTotalLineaCompra() {
        return totalLineaCompra;
    }

    public void setTotalLineaCompra(float totalLineaCompra) {
        this.totalLineaCompra = totalLineaCompra;
    }
       
}
