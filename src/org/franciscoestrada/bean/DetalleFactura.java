package org.franciscoestrada.bean;
import org.franciscoestrada.bean.Factura;
import org.franciscoestrada.bean.Producto;

public class DetalleFactura {
    
    private int idDetalle;
    private Factura factura;
    private int lineaNo;
    private Producto producto;
    private String descripcionDF;
    private int cantidad;
    private float precio;
    private float totalLinea;

    public DetalleFactura() {
    
    }

    public DetalleFactura(int idDetalle, Factura factura, int lineaNo, Producto producto, String descripcionDF, int cantidad, float precio, float totalLinea) {
        this.idDetalle = idDetalle;
        this.factura = factura;
        this.lineaNo = lineaNo;
        this.producto = producto;
        this.descripcionDF = descripcionDF;
        this.cantidad = cantidad;
        this.precio = precio;
        this.totalLinea = totalLinea;
    }

    public int getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(int idDetalle) {
        this.idDetalle = idDetalle;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public int getLineaNo() {
        return lineaNo;
    }

    public void setLineaNo(int lineaNo) {
        this.lineaNo = lineaNo;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public String getDescripcionDF() {
        return descripcionDF;
    }

    public void setDescripcionDF(String descripcionDF) {
        this.descripcionDF = descripcionDF;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getTotalLinea() {
        return totalLinea;
    }

    public void setTotalLinea(float totalLinea) {
        this.totalLinea = totalLinea;
    }


                
}
