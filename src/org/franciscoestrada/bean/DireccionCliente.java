package org.franciscoestrada.bean;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.bean.TipoDireccionCliente;

public class DireccionCliente {
    
    private int idDireccion;
    private Cliente cliente;
    private TipoDireccionCliente tipoDireccionCliente;
    private String direccion;

    public DireccionCliente() {
    }

    public DireccionCliente(int idDireccion, Cliente cliente, TipoDireccionCliente tipoDireccionCliente, String direccion) {
        this.idDireccion = idDireccion;
        this.cliente = cliente;
        this.tipoDireccionCliente = tipoDireccionCliente;
        this.direccion = direccion;
    }

    public int getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(int idDireccion) {
        this.idDireccion = idDireccion;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TipoDireccionCliente getTipoDireccionCliente() {
        return tipoDireccionCliente;
    }

    public void setTipoDireccionCliente(TipoDireccionCliente tipoDireccionCliente) {
        this.tipoDireccionCliente = tipoDireccionCliente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }




               
}
