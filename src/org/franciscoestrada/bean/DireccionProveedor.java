package org.franciscoestrada.bean;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.bean.TipoDireccionProveedor;

public class DireccionProveedor {
    
    private int idDireccion;
    private Proveedor proveedor;
    private TipoDireccionProveedor tipoDireccionProveedor;
    private String direccion;

    public DireccionProveedor() {
    
    }

    public DireccionProveedor(int idDireccion, Proveedor proveedor, TipoDireccionProveedor tipoDireccionProveedor, String direccion) {
        this.idDireccion = idDireccion;
        this.proveedor = proveedor;
        this.tipoDireccionProveedor = tipoDireccionProveedor;
        this.direccion = direccion;
    }

    public int getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(int idDireccion) {
        this.idDireccion = idDireccion;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public TipoDireccionProveedor getTipoDireccionProveedor() {
        return tipoDireccionProveedor;
    }

    public void setTipoDireccionProveedor(TipoDireccionProveedor tipoDireccionProveedor) {
        this.tipoDireccionProveedor = tipoDireccionProveedor;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
}
