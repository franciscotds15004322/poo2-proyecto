package org.franciscoestrada.bean;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.bean.TipoEmailCliente;

public class EmailCliente {

    private int idEmail;
    private Cliente cliente;
    private TipoEmailCliente tipoEmailCliente;
    private String email;

    public EmailCliente() {
    
    }

    public EmailCliente(int idEmail, Cliente cliente, TipoEmailCliente tipoEmailCliente, String email) {
        this.idEmail = idEmail;
        this.cliente = cliente;
        this.tipoEmailCliente = tipoEmailCliente;
        this.email = email;
    }

    public int getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(int idEmail) {
        this.idEmail = idEmail;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TipoEmailCliente getTipoEmailCliente() {
        return tipoEmailCliente;
    }

    public void setTipoEmailCliente(TipoEmailCliente tipoEmailCliente) {
        this.tipoEmailCliente = tipoEmailCliente;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
           
}
