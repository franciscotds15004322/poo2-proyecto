package org.franciscoestrada.bean;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.bean.TipoEmailProveedor;

public class EmailProveedor {

    private int idEmail;
    private Proveedor proveedor;
    private TipoEmailProveedor tipoEmailProveedor;
    private String email;

    public EmailProveedor() {
        
    }

    public EmailProveedor(int idEmail, Proveedor proveedor, TipoEmailProveedor tipoEmailProveedor, String email) {
        this.idEmail = idEmail;
        this.proveedor = proveedor;
        this.tipoEmailProveedor = tipoEmailProveedor;
        this.email = email;
    }

    public int getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(int idEmail) {
        this.idEmail = idEmail;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public TipoEmailProveedor getTipoEmailProveedor() {
        return tipoEmailProveedor;
    }

    public void setTipoEmailProveedor(TipoEmailProveedor tipoEmailProveedor) {
        this.tipoEmailProveedor = tipoEmailProveedor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
        
}
