package org.franciscoestrada.bean;

public class Empaque {
    
    private int idEmpaque;
    private String descripcionEmpaque;

    public Empaque() {
    
    }

    public Empaque(int idEmpaque, String descripcionEmpaque) {
        this.idEmpaque = idEmpaque;
        this.descripcionEmpaque = descripcionEmpaque;
    }

    public int getIdEmpaque() {
        return idEmpaque;
    }

    public void setIdEmpaque(int idEmpaque) {
        this.idEmpaque = idEmpaque;
    }

    public String getDescripcionEmpaque() {
        return descripcionEmpaque;
    }

    public void setDescripcionEmpaque(String descripcionEmpaque) {
        this.descripcionEmpaque = descripcionEmpaque;
    }
    
}
