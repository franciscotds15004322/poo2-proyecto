package org.franciscoestrada.bean;
import org.franciscoestrada.bean.Cliente;
import java.util.Date;

public class Factura {
    private int idFactura;
    private int numeroDeFactura;
    private Date fecha;
    private Cliente cliente;
    private String descripcion;
    private float total;

    public Factura() {
    
    }

    public Factura(int idFactura, int numeroDeFactura, Date fecha, Cliente cliente, String descripcion, float total) {
        this.idFactura = idFactura;
        this.numeroDeFactura = numeroDeFactura;
        this.fecha = fecha;
        this.cliente = cliente;
        this.descripcion = descripcion;
        this.total = total;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public int getNumeroDeFactura() {
        return numeroDeFactura;
    }

    public void setNumeroDeFactura(int numeroDeFactura) {
        this.numeroDeFactura = numeroDeFactura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
    
}
