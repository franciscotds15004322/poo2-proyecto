package org.franciscoestrada.bean;
import java.util.Date;
import org.franciscoestrada.bean.TipoMovimiento;
import org.franciscoestrada.bean.Producto;

public class Inventario {
    
    private int idMovimiento;
    private Date fechaInventario;
    private Producto producto;
    private TipoMovimiento tipoMovimiento;
    private int idFactura;
    private int idCompra;
    private int lineaNoInventario;
    private int cantidad;
    private float precioCompraUnitario;

    public Inventario() {
    }

    public Inventario(int idMovimiento, Date fechaInventario, Producto producto, TipoMovimiento tipoMovimiento, int idFactura, int idCompra, int lineaNoInventario, int cantidad, float precioCompraUnitario) {
        this.idMovimiento = idMovimiento;
        this.fechaInventario = fechaInventario;
        this.producto = producto;
        this.tipoMovimiento = tipoMovimiento;
        this.idFactura = idFactura;
        this.idCompra = idCompra;
        this.lineaNoInventario = lineaNoInventario;
        this.cantidad = cantidad;
        this.precioCompraUnitario = precioCompraUnitario;
    }

    public int getIdMovimiento() {
        return idMovimiento;
    }

    public void setIdMovimiento(int idMovimiento) {
        this.idMovimiento = idMovimiento;
    }

    public Date getFechaInventario() {
        return fechaInventario;
    }

    public void setFechaInventario(Date fechaInventario) {
        this.fechaInventario = fechaInventario;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public TipoMovimiento getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(TipoMovimiento tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public int getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(int idCompra) {
        this.idCompra = idCompra;
    }

    public int getLineaNoInventario() {
        return lineaNoInventario;
    }

    public void setLineaNoInventario(int lineaNoInventario) {
        this.lineaNoInventario = lineaNoInventario;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecioCompraUnitario() {
        return precioCompraUnitario;
    }

    public void setPrecioCompraUnitario(float precioCompraUnitario) {
        this.precioCompraUnitario = precioCompraUnitario;
    }

}