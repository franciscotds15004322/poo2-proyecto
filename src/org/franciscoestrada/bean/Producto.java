package org.franciscoestrada.bean;
import org.franciscoestrada.bean.Empaque;
import org.franciscoestrada.bean.Categoria;
import org.franciscoestrada.bean.Stock;

public class Producto {
    
    private int idProducto;
    private String nombreProducto;
    private String descripcionProducto;
    private float precioUnitario;
    private float precioPorDocena;
    private float precioPorMayor;
    private Empaque empaque;
    private Categoria categoria;
    private Stock stock;

    public Producto() {
    }

    public Producto(int idProducto, String nombreProducto, String descripcionProducto, float precioUnitario, float precioPorDocena, float precioPorMayor, Empaque empaque, Categoria categoria, Stock stock) {
        this.idProducto = idProducto;
        this.nombreProducto = nombreProducto;
        this.descripcionProducto = descripcionProducto;
        this.precioUnitario = precioUnitario;
        this.precioPorDocena = precioPorDocena;
        this.precioPorMayor = precioPorMayor;
        this.empaque = empaque;
        this.categoria = categoria;
        this.stock = stock;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getDescripcionProducto() {
        return descripcionProducto;
    }

    public void setDescripcionProducto(String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }

    public float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public float getPrecioPorDocena() {
        return precioPorDocena;
    }

    public void setPrecioPorDocena(float precioPorDocena) {
        this.precioPorDocena = precioPorDocena;
    }

    public float getPrecioPorMayor() {
        return precioPorMayor;
    }

    public void setPrecioPorMayor(float precioPorMayor) {
        this.precioPorMayor = precioPorMayor;
    }

    public Empaque getEmpaque() {
        return empaque;
    }

    public void setEmpaque(Empaque empaque) {
        this.empaque = empaque;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

}
