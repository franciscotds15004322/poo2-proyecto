package org.franciscoestrada.bean;

public class Stock {
    
    private int idStock;
    private int stock;

    public Stock() {
    
    }

    public Stock(int idStock, int stock) {
        this.idStock = idStock;
        this.stock = stock;
    }

    public int getIdStock() {
        return idStock;
    }

    public void setIdStock(int idStock) {
        this.idStock = idStock;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
      
}
