package org.franciscoestrada.bean;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.bean.TipoTelefonoCliente;
 
public class TelefonoCliente {
    
    private int idTelefono;
    private Cliente cliente;
    private TipoTelefonoCliente tipoTelefonoCliente;
    private String telefono;

    public TelefonoCliente() {
        
    }

    public TelefonoCliente(int idTelefono, Cliente cliente, TipoTelefonoCliente tipoTelefonoCliente, String telefono) {
        this.idTelefono = idTelefono;
        this.cliente = cliente;
        this.tipoTelefonoCliente = tipoTelefonoCliente;
        this.telefono = telefono;
    }

    public int getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(int idTelefono) {
        this.idTelefono = idTelefono;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TipoTelefonoCliente getTipoTelefonoCliente() {
        return tipoTelefonoCliente;
    }

    public void setTipoTelefonoCliente(TipoTelefonoCliente tipoTelefonoCliente) {
        this.tipoTelefonoCliente = tipoTelefonoCliente;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
     
}
