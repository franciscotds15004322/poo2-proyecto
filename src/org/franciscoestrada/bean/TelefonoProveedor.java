package org.franciscoestrada.bean;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.bean.TipoTelefonoProveedor;

public class TelefonoProveedor {
    
    private int idTelefono;
    private Proveedor proveedor;
    private TipoTelefonoProveedor tipoTelefonoProveedor;
    private String telefono; 

    public TelefonoProveedor() {
    }

    public TelefonoProveedor(int idTelefono, Proveedor proveedor, TipoTelefonoProveedor tipoTelefonoProveedor, String telefono) {
        this.idTelefono = idTelefono;
        this.proveedor = proveedor;
        this.tipoTelefonoProveedor = tipoTelefonoProveedor;
        this.telefono = telefono;
    }

    public int getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(int idTelefono) {
        this.idTelefono = idTelefono;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public TipoTelefonoProveedor getTipoTelefonoProveedor() {
        return tipoTelefonoProveedor;
    }

    public void setTipoTelefonoProveedor(TipoTelefonoProveedor tipoTelefonoProveedor) {
        this.tipoTelefonoProveedor = tipoTelefonoProveedor;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }


       
}
