package org.franciscoestrada.bean;

public class TipoDireccionCliente {
    private int idTipo;
    private String descripcion;

    public TipoDireccionCliente() {
    }

    public TipoDireccionCliente(int idTipo, String descripcion) {
        this.idTipo = idTipo;
        this.descripcion = descripcion;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
         
}
