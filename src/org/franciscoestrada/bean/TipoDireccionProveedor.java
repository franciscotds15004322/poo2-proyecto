package org.franciscoestrada.bean;

public class TipoDireccionProveedor {
    private int idTipo;
    private String descripcion;

    public TipoDireccionProveedor() {
        
    }

    public TipoDireccionProveedor(int idTipo, String descripcion) {
        this.idTipo = idTipo;
        this.descripcion = descripcion;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
      
}
