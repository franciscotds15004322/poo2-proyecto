package org.franciscoestrada.bean;

public class TipoEmailCliente {
    private int idTipo;
    private String descripcion;

    public TipoEmailCliente() {
        
    }

    public TipoEmailCliente(int idTipo, String descripcion) {
        this.idTipo = idTipo;
        this.descripcion = descripcion;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
