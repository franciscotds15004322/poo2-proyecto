package org.franciscoestrada.db;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.CallableStatement;
import java.util.Iterator;
import java.util.Map;
import java.sql.Date;


public class ConexionDB {
    
    private Connection conexion;
    private Statement sentencia;    
    private static ConexionDB instancia;
    
    public static ConexionDB getInstancia(){
        if(instancia == null){
            instancia = new ConexionDB();
        }
        return instancia;
    }
    
    public ConexionDB() {
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            conexion = DriverManager.getConnection("jdbc:sqlserver://SERVER1-PC:1433;instanceName=MSSQLSERVER;dataBaseName=Inventario;user=AdminInventario;password=pancho123");
            sentencia = conexion.createStatement();
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }catch(InstantiationException e){
            e.printStackTrace();
        }catch(IllegalAccessException e){
            e.printStackTrace();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public ResultSet hacerConsulta(String consulta){
        ResultSet resultado = null;
        try{
            resultado = sentencia.executeQuery(consulta);
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    public ResultSet ejecutarProcedimiento(String nombreprocedimiento, Map parametros, boolean respuesta){
        ResultSet resultado = null;
        try{
            CallableStatement procedimiento = conexion.prepareCall(nombreprocedimiento);
            Iterator iterador = parametros.entrySet().iterator();
            while(iterador.hasNext()){
                Map.Entry e = (Map.Entry)iterador.next();
                String clase = e.getValue().getClass().toString();
                int longitud = clase.length();
                String tipoDato = clase.substring(clase.lastIndexOf(".")+1,longitud);
                if(tipoDato.equals("String")){
                    procedimiento.setString(e.getKey().toString(),e.getValue().toString());
                }else if(tipoDato.equals("Integer")){
                    procedimiento.setInt(e.getKey().toString(),Integer.parseInt(e.getValue().toString()));
                }else if(tipoDato.equals("Double")){
                    procedimiento.setDouble(e.getKey().toString(),Double.parseDouble(e.getValue().toString()));
                }else if(tipoDato.equals("Boolean")){
                    procedimiento.setBoolean(e.getKey().toString(),Boolean.parseBoolean(e.getValue().toString()));
                }else if(tipoDato.equals("Float")){
                    procedimiento.setFloat(e.getKey().toString(),Float.parseFloat(e.getValue().toString()));
                }else if(tipoDato.equals("Date")){ //PROBLEMA CON LA FECHA
                    procedimiento.setDate(e.getKey().toString(),Date.valueOf(e.getValue().toString()));
                }
            }
            if(respuesta == true){
                resultado = procedimiento.executeQuery();
            }else{
                procedimiento.execute();
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return resultado;    
    }
   
    public Connection getConexion() {
        return conexion;
    }
}

