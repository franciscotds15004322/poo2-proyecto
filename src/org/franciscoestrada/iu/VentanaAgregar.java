package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JButton;

public class VentanaAgregar extends JDialog implements ActionListener {
    private JButton btnGuardar;
    private JButton btnCancelar;

    public VentanaAgregar(String titulo, int ancho, int alto) {
        this.setTitle(titulo);
        this.setSize(ancho,alto);
        this.setLayout(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        btnGuardar = new JButton("Guardar");
        btnGuardar.setBounds(10,350,100,50);
        btnGuardar.addActionListener(this);
        btnCancelar = new JButton("Cancelar");
        btnCancelar.setBounds(140,350,100,50);
        btnCancelar.addActionListener(this); //Apuntan al boton
        this.getContentPane().add(btnGuardar);
        this.getContentPane().add(btnCancelar);
        this.setVisible(true);
    }
    
    public void actionPerformed(ActionEvent e){
        
    }

    public JButton getBtnGuardar() {
        return btnGuardar;
    }

    public void setBtnGuardar(JButton btnGuardar) {
        this.btnGuardar = btnGuardar;
    }

    public JButton getBtnCancelar() {
        return btnCancelar;
    }

    public void setBtnCancelar(JButton btnCancelar) {
        this.btnCancelar = btnCancelar;
    }
    
}
