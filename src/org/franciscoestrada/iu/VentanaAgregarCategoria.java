package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Categoria;
import org.franciscoestrada.modelo.ModeloDatosCategoria;

public class VentanaAgregarCategoria extends VentanaAgregar implements ActionListener {
    
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;
    private ModeloDatosCategoria modelo;
    
    public VentanaAgregarCategoria(String titulo, int ancho, int alto, ModeloDatosCategoria modelo) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,40,100,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setBounds(90,40,150,20);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            modelo.agregar(new Categoria(0,txtDescripcion.getText()));
            this.dispose();
        }
    }
}
