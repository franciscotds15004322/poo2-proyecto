package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.modelo.ModeloDatosCliente;

public class VentanaAgregarCliente extends VentanaAgregar implements ActionListener {
    
    private JLabel lblNit;
    private JTextField txtNit;
    private JLabel lblDpi;
    private JTextField txtDpi;
    private JLabel lblNombre;
    private JTextField txtNombre;
    private ModeloDatosCliente modelo;
    
    public VentanaAgregarCliente(String titulo, int ancho, int alto, ModeloDatosCliente modelo) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        lblNit = new JLabel("Nit");
        lblNit.setBounds(5,40,100,20);
        txtNit = new JTextField();
        txtNit.setBounds(90,40,150,20);
        lblDpi = new JLabel("Dpi");
        lblDpi.setBounds(5,70,100,20);
        txtDpi = new JTextField();
        txtDpi.setBounds(90,70,150,20);
        lblNombre = new JLabel("Nombre");
        lblNombre.setBounds(5,100,100,20);
        txtNombre = new JTextField();
        txtNombre.setBounds(90,100,150,20);

        this.getContentPane().add(lblNit);
        this.getContentPane().add(txtNit);
        this.getContentPane().add(lblDpi);
        this.getContentPane().add(txtDpi);
        this.getContentPane().add(lblNombre);
        this.getContentPane().add(txtNombre);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            modelo.agregar(new Cliente(0,txtNit.getText(),txtDpi.getText(),txtNombre.getText()));
            this.dispose();
        }
    }
}
