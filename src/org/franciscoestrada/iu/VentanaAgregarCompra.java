package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Compra;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.modelo.ModeloDatosCompra;

public class VentanaAgregarCompra extends VentanaAgregar implements ActionListener {
    
    private JLabel lblNumeroDeCompra;
    private JTextField txtNumeroDeCompra;
    private JLabel lblFecha;
    private JTextField txtFecha;
    private JLabel lblIdProveedor;
    private JTextField txtIdProveedor;
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;    
    private JLabel lblTotal;
    private JTextField txtTotal;
    private ModeloDatosCompra modelo;
    
    public VentanaAgregarCompra(String titulo, int ancho, int alto, ModeloDatosCompra modelo) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        lblNumeroDeCompra = new JLabel("Numero Compra");
        lblNumeroDeCompra.setBounds(5,40,100,20);
        txtNumeroDeCompra = new JTextField();
        txtNumeroDeCompra.setBounds(90,40,150,20);
        lblFecha = new JLabel("Fecha");
        lblFecha.setBounds(5,70,100,20);
        txtFecha = new JTextField();
        txtFecha.setBounds(90,70,150,20);
        lblIdProveedor = new JLabel("Id Cliente");
        lblIdProveedor.setBounds(5,100,100,20);
        txtIdProveedor = new JTextField();
        txtIdProveedor.setBounds(90,100,150,20);
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,130,100,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setBounds(90,130,150,20);
        lblTotal = new JLabel("Total");
        lblTotal.setBounds(5,160,100,20);
        txtTotal = new JTextField();
        txtTotal.setBounds(90,160,150,20);      

        this.getContentPane().add(lblNumeroDeCompra);
        this.getContentPane().add(txtNumeroDeCompra);
        this.getContentPane().add(lblFecha);
        this.getContentPane().add(txtFecha);
        this.getContentPane().add(lblIdProveedor);
        this.getContentPane().add(txtIdProveedor);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
        this.getContentPane().add(lblTotal);
        this.getContentPane().add(txtTotal);
        
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            int var= Integer.parseInt(txtIdProveedor.getText());
            float var2= Float.parseFloat(txtTotal.getText());
            int var4= Integer.parseInt(txtNumeroDeCompra.getText());
            Date fecha;
            fecha = new Date(txtFecha.getText());
            modelo.agregar(new Compra(0,var4,fecha,
            new Proveedor(var," "," "," "," "), txtDescripcion.getText(),var2));
            this.dispose();
        }
    }
}
