package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.DetalleFactura;
import org.franciscoestrada.bean.Producto;
import org.franciscoestrada.bean.Factura;
import org.franciscoestrada.bean.Empaque;
import org.franciscoestrada.bean.Categoria;
import org.franciscoestrada.bean.Stock;
import org.franciscoestrada.bean.Cliente;
import java.util.Date;
import org.franciscoestrada.modelo.ModeloDatosDetalleFactura;

public class VentanaAgregarDetalleFactura extends VentanaAgregar implements ActionListener {
    
    private JLabel lblIdFactura;
    private JTextField txtIdFactura;
    private JLabel lblLineaNo;
    private JTextField txtLineaNo;
    private JLabel lblIdProducto;
    private JTextField txtIdProducto;
    private JLabel lblDescripcionDF;
    private JTextField txtDescripcionDF;    
    private JLabel lblCantidad;
    private JTextField txtCantidad;
    private JLabel lblPrecio;
    private JTextField txtPrecio;
    private JLabel lblTotalLinea;
    private JTextField txtTotalLinea;  
    private ModeloDatosDetalleFactura modelo;
    
    public VentanaAgregarDetalleFactura(String titulo, int ancho, int alto, ModeloDatosDetalleFactura modelo) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        lblIdFactura = new JLabel("Id Factura");
        lblIdFactura.setBounds(5,40,100,20);
        txtIdFactura = new JTextField();
        txtIdFactura.setBounds(90,40,150,20);
        lblLineaNo = new JLabel("Linea No");
        lblLineaNo.setBounds(5,70,100,20);
        txtLineaNo = new JTextField();
        txtLineaNo.setBounds(90,70,150,20);
        lblIdProducto = new JLabel("Id Producto");
        lblIdProducto.setBounds(5,100,100,20);
        txtIdProducto = new JTextField();
        txtIdProducto.setBounds(90,100,150,20);
        lblDescripcionDF = new JLabel("Descripcion DF");
        lblDescripcionDF.setBounds(5,130,100,20);
        txtDescripcionDF = new JTextField();
        txtDescripcionDF.setBounds(90,130,150,20);
        lblCantidad = new JLabel("Cantidad");
        lblCantidad.setBounds(5,160,100,20);
        txtCantidad = new JTextField();
        txtCantidad.setBounds(90,160,150,20);
        lblPrecio = new JLabel("Precio");
        lblPrecio.setBounds(5,190,100,20);
        txtPrecio = new JTextField();
        txtPrecio.setBounds(90,190,150,20);
        lblTotalLinea = new JLabel("Total Linea");
        lblTotalLinea.setBounds(5,220,100,20);
        txtTotalLinea = new JTextField();
        txtTotalLinea.setBounds(90,220,150,20);       

        this.getContentPane().add(lblIdFactura);
        this.getContentPane().add(txtIdFactura);
        this.getContentPane().add(lblLineaNo);
        this.getContentPane().add(txtLineaNo);
        this.getContentPane().add(lblIdProducto);
        this.getContentPane().add(txtIdProducto);
        this.getContentPane().add(lblDescripcionDF);
        this.getContentPane().add(txtDescripcionDF);
        this.getContentPane().add(lblCantidad);
        this.getContentPane().add(txtCantidad);
        this.getContentPane().add(lblPrecio);
        this.getContentPane().add(txtPrecio);
        this.getContentPane().add(lblTotalLinea);
        this.getContentPane().add(txtTotalLinea);
        
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            float var5= Float.parseFloat(txtPrecio.getText());
            float var6= Float.parseFloat(txtTotalLinea.getText());
            int var1= Integer.parseInt(txtIdFactura.getText());
            int var2= Integer.parseInt(txtLineaNo.getText());
            int var3= Integer.parseInt(txtIdProducto.getText());
            int var4= Integer.parseInt(txtCantidad.getText());
            Date fecha;
            fecha = new Date(82,4,1,10,30,15);
            modelo.agregar(new DetalleFactura(0,new Factura(var1,0,fecha,
            new Cliente(0," "," "," ")," ",0),var2,                   
            new Producto(var3," "," ",0,0,0,
            new Empaque(0," "), new Categoria(0," "), new Stock(0,0)),
            txtDescripcionDF.getText(),var4,var5,var6));
            this.dispose();
        }
    }
}
