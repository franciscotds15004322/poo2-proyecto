package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.bean.TipoEmailCliente;
import org.franciscoestrada.bean.EmailCliente;
import org.franciscoestrada.modelo.ModeloDatosEmailCliente;

public class VentanaAgregarEmailCliente extends VentanaAgregar implements ActionListener {
    
    private JLabel lblIdCliente;
    private JTextField txtIdCliente;
    private JLabel lblIdTipo;
    private JTextField txtIdTipo;
    private JLabel lblEmail;
    private JTextField txtEmail;
    private ModeloDatosEmailCliente modelo;
    
    public VentanaAgregarEmailCliente(String titulo, int ancho, int alto, ModeloDatosEmailCliente modelo) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        lblIdCliente = new JLabel("Id Cliente");
        lblIdCliente.setBounds(5,40,100,20);
        txtIdCliente = new JTextField();
        txtIdCliente.setBounds(90,40,150,20);
        lblIdTipo = new JLabel("Id Tipo");
        lblIdTipo.setBounds(5,70,100,20);
        txtIdTipo = new JTextField();
        txtIdTipo.setBounds(90,70,150,20);
        lblEmail = new JLabel("Email");
        lblEmail.setBounds(5,100,100,20);
        txtEmail = new JTextField();
        txtEmail.setBounds(90,100,150,20);

        this.getContentPane().add(lblIdCliente);
        this.getContentPane().add(txtIdCliente);
        this.getContentPane().add(lblIdTipo);
        this.getContentPane().add(txtIdTipo);
        this.getContentPane().add(lblEmail);
        this.getContentPane().add(txtEmail);
        
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            int var1= Integer.parseInt(txtIdCliente.getText());
            int var2= Integer.parseInt(txtIdTipo.getText());
            modelo.agregar(new EmailCliente(0,
            new Cliente(var1," "," "," "), 
            new TipoEmailCliente(var2," "), 
            txtEmail.getText()));
            this.dispose();
        }
    }
}
