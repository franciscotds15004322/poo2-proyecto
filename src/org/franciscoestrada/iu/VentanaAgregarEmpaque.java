package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Empaque;
import org.franciscoestrada.modelo.ModeloDatosEmpaque;

public class VentanaAgregarEmpaque extends VentanaAgregar implements ActionListener {
    
    private JLabel lblEmpaque;
    private JTextField txtEmpaque;
    private ModeloDatosEmpaque modelo;
    
    public VentanaAgregarEmpaque(String titulo, int ancho, int alto, ModeloDatosEmpaque modelo) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        lblEmpaque = new JLabel("Descripcion Empaque");
        lblEmpaque.setBounds(5,40,100,20);
        txtEmpaque = new JTextField();
        txtEmpaque.setBounds(150,40,150,20);
        this.getContentPane().add(lblEmpaque);
        this.getContentPane().add(txtEmpaque);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            modelo.agregar(new Empaque(0,txtEmpaque.getText()));
            this.dispose();
        }
    }
}
