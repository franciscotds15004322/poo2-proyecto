package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Factura;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.modelo.ModeloDatosFactura;

public class VentanaAgregarFactura extends VentanaAgregar implements ActionListener {
    
    private JLabel lblNumeroDeFactura;
    private JTextField txtNumeroDeFactura;
    private JLabel lblFecha;
    private JTextField txtFecha;
    private JLabel lblIdCliente;
    private JTextField txtIdCliente;
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;    
    private JLabel lblTotal;
    private JTextField txtTotal;
    private ModeloDatosFactura modelo;
    
    public VentanaAgregarFactura(String titulo, int ancho, int alto, ModeloDatosFactura modelo) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        lblNumeroDeFactura = new JLabel("Numero Factura");
        lblNumeroDeFactura.setBounds(5,40,100,20);
        txtNumeroDeFactura = new JTextField();
        txtNumeroDeFactura.setBounds(90,40,150,20);
        lblFecha = new JLabel("Fecha");
        lblFecha.setBounds(5,70,100,20);
        txtFecha = new JTextField();
        txtFecha.setBounds(90,70,150,20);
        lblIdCliente = new JLabel("Id Cliente");
        lblIdCliente.setBounds(5,100,100,20);
        txtIdCliente = new JTextField();
        txtIdCliente.setBounds(90,100,150,20);
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,130,100,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setBounds(90,130,150,20);
        lblTotal = new JLabel("Total");
        lblTotal.setBounds(5,160,100,20);
        txtTotal = new JTextField();
        txtTotal.setBounds(90,160,150,20);      

        this.getContentPane().add(lblNumeroDeFactura);
        this.getContentPane().add(txtNumeroDeFactura);
        this.getContentPane().add(lblFecha);
        this.getContentPane().add(txtFecha);
        this.getContentPane().add(lblIdCliente);
        this.getContentPane().add(txtIdCliente);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
        this.getContentPane().add(lblTotal);
        this.getContentPane().add(txtTotal);
        
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            int var= Integer.parseInt(txtIdCliente.getText());
            float var2= Float.parseFloat(txtTotal.getText());
            Date fecha;
            fecha = new Date(txtFecha.getText());
            int var4= Integer.parseInt(txtNumeroDeFactura.getText());
            modelo.agregar(new Factura(0,var4,fecha,
            new Cliente(var," "," "," "), txtDescripcion.getText(),var2));
            this.dispose();
        }
    }
}
