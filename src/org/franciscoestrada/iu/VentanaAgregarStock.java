package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Stock;
import org.franciscoestrada.modelo.ModeloDatosStock;

public class VentanaAgregarStock extends VentanaAgregar implements ActionListener {
    
    private JLabel lblStock;
    private JTextField txtStock;
    private ModeloDatosStock modelo;
    
    public VentanaAgregarStock(String titulo, int ancho, int alto, ModeloDatosStock modelo) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        lblStock = new JLabel("stock");
        lblStock.setBounds(5,40,100,20);
        txtStock = new JTextField();
        txtStock.setBounds(90,40,150,20);
        this.getContentPane().add(lblStock);
        this.getContentPane().add(txtStock);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            int var= Integer.parseInt(txtStock.getText());
            modelo.agregar(new Stock(0,var));
            this.dispose();
        }
    }
}
