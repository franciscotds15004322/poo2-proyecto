package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.bean.TipoTelefonoProveedor;
import org.franciscoestrada.bean.TelefonoProveedor;
import org.franciscoestrada.modelo.ModeloDatosTelefonoProveedor;

public class VentanaAgregarTelefonoProveedor extends VentanaAgregar implements ActionListener {
    
    private JLabel lblIdProveedor;
    private JTextField txtIdProveedor;
    private JLabel lblIdTipo;
    private JTextField txtIdTipo;
    private JLabel lblTelefono;
    private JTextField txtTelefono;
    private ModeloDatosTelefonoProveedor modelo;
    
    public VentanaAgregarTelefonoProveedor(String titulo, int ancho, int alto, ModeloDatosTelefonoProveedor modelo) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        lblIdProveedor = new JLabel("Id Proveedor");
        lblIdProveedor.setBounds(5,40,100,20);
        txtIdProveedor = new JTextField();
        txtIdProveedor.setBounds(90,40,150,20);
        lblIdTipo = new JLabel("Id Tipo");
        lblIdTipo.setBounds(5,70,100,20);
        txtIdTipo = new JTextField();
        txtIdTipo.setBounds(90,70,150,20);
        lblTelefono = new JLabel("Telefono");
        lblTelefono.setBounds(5,100,100,20);
        txtTelefono = new JTextField();
        txtTelefono.setBounds(90,100,150,20);

        this.getContentPane().add(lblIdProveedor);
        this.getContentPane().add(txtIdProveedor);
        this.getContentPane().add(lblIdTipo);
        this.getContentPane().add(txtIdTipo);
        this.getContentPane().add(lblTelefono);
        this.getContentPane().add(txtTelefono);
        
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            int var1= Integer.parseInt(txtIdProveedor.getText());
            int var2= Integer.parseInt(txtIdTipo.getText());
            modelo.agregar(new TelefonoProveedor(0,
            new Proveedor(var1," "," "," "," "), 
            new TipoTelefonoProveedor(var2," "), 
            txtTelefono.getText()));
            this.dispose();
        }
    }
}


