package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.TipoDireccionProveedor;
import org.franciscoestrada.modelo.ModeloDatosTipoDireccionProveedor;

public class VentanaAgregarTipoDireccionProveedor extends VentanaAgregar implements ActionListener {
    
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;
    private ModeloDatosTipoDireccionProveedor modelo;
    
    public VentanaAgregarTipoDireccionProveedor(String titulo, int ancho, int alto, ModeloDatosTipoDireccionProveedor modelo) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,40,100,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setBounds(150,40,150,20);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            modelo.agregar(new TipoDireccionProveedor(0,txtDescripcion.getText()));
            this.dispose();
        }
    }
}
