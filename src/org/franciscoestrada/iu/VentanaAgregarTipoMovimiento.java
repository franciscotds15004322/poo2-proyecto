package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.TipoMovimiento;
import org.franciscoestrada.modelo.ModeloDatosTipoMovimiento;

public class VentanaAgregarTipoMovimiento extends VentanaAgregar implements ActionListener {
    
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;
    private ModeloDatosTipoMovimiento modelo;
    
    public VentanaAgregarTipoMovimiento(String titulo, int ancho, int alto, ModeloDatosTipoMovimiento modelo) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,40,100,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setBounds(150,40,150,20);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            modelo.agregar(new TipoMovimiento(0,txtDescripcion.getText()));
            this.dispose();
        }
    }
}
