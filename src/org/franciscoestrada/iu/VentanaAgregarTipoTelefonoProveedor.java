package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.TipoTelefonoProveedor;
import org.franciscoestrada.modelo.ModeloDatosTipoTelefonoProveedor;

public class VentanaAgregarTipoTelefonoProveedor extends VentanaAgregar implements ActionListener {
    
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;
    private ModeloDatosTipoTelefonoProveedor modelo;
    
    public VentanaAgregarTipoTelefonoProveedor(String titulo, int ancho, int alto, ModeloDatosTipoTelefonoProveedor modelo) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,40,100,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setBounds(150,40,150,20);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            modelo.agregar(new TipoTelefonoProveedor(0,txtDescripcion.getText()));
            this.dispose();
        }
    }
}
