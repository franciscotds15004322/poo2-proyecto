package org.franciscoestrada.iu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.AbstractTableModel;

public class VentanaCatalogo extends JFrame implements ActionListener{
    private JButton btnAgregar;
    private JButton btnModificar;
    private JButton btnReporte;
    private JButton btnBuscar;   
    private JButton btnEliminar;
    private JButton btnSalir;
    private JTable tblDatos;
    private JScrollPane ScrDatos;
    private AbstractTableModel modelo;
    
    public VentanaCatalogo(String titulo, int ancho, int alto, AbstractTableModel modelo) {
        this.modelo = modelo;
        this.setTitle(titulo);
        this.setSize(ancho,alto);
        this.setLayout(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(this);//Ver
        btnAgregar = new JButton("Agregar");
        btnAgregar.setBounds(10,10,120,60);
        btnAgregar.addActionListener(this);
        btnModificar = new JButton("Modificar");
        btnModificar.setBounds(10,80,120,60);
        btnModificar.addActionListener(this); 
        btnReporte = new JButton("Reporte");
        btnReporte.setBounds(10,150,120,60);
        btnReporte.addActionListener(this);
        btnEliminar = new JButton("Eliminar");
        btnEliminar.setBounds(10,220,120,60);
        btnEliminar.addActionListener(this);
        tblDatos = new JTable();
        tblDatos.setModel(modelo);
        ScrDatos = new JScrollPane();
        ScrDatos.setBounds(140,10,600,500);
        ScrDatos.setViewportView(tblDatos);
        this.getContentPane().add(ScrDatos);
        this.getContentPane().add(btnAgregar);
        this.getContentPane().add(btnModificar);
        this.getContentPane().add(btnReporte);
        this.getContentPane().add(btnEliminar);
        this.setVisible(true);
                
    }
    
    public void actionPerformed(ActionEvent e){ 

    }

    public JButton getBtnAgregar() {
        return btnAgregar;
    }

    public void setBtnAgregar(JButton btnAgregar) {
        this.btnAgregar = btnAgregar;
    }

    public JButton getBtnModificar() {
        return btnModificar;
    }

    public void setBtnModificar(JButton btnModificar) {
        this.btnModificar = btnModificar;
    }

    public JButton getBtnReporte() {
        return btnReporte;
    }

    public void setBtnReporte(JButton btnReporte) {
        this.btnReporte = btnReporte;
    }

    public JButton getBtnBuscar() {
        return btnBuscar;
    }

    public void setBtnBuscar(JButton btnBuscar) {
        this.btnBuscar = btnBuscar;
    }

    public JButton getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(JButton btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

    public JButton getBtnSalir() {
        return btnSalir;
    }

    public void setBtnSalir(JButton btnSalir) {
        this.btnSalir = btnSalir;
    }

    public JTable getTblDatos() {
        return tblDatos;
    }

    public void setTblDatos(JTable tblDatos) {
        this.tblDatos = tblDatos;
    }

    public JScrollPane getScrDatos() {
        return ScrDatos;
    }

    public void setScrDatos(JScrollPane ScrDatos) {
        this.ScrDatos = ScrDatos;
    }

    public AbstractTableModel getModelo() {
        return modelo;
    }

    public void setModelo(AbstractTableModel modelo) {
        this.modelo = modelo;
    }
    
}
