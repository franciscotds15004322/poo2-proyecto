package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Categoria;
import org.franciscoestrada.modelo.ModeloDatosCategoria;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoCategoria extends VentanaCatalogo implements ActionListener {
    
    public VentanaCatalogoCategoria() {       
        super("Ventana Catalogo Categoria",850,550,new ModeloDatosCategoria());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarCategoria("Agregar Categoria",300,450,(ModeloDatosCategoria)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Categoria elemento = ((ModeloDatosCategoria)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarCategoria("Modificar Categoria",300,450,(ModeloDatosCategoria)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                Categoria elemento = ((ModeloDatosCategoria)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idCategoria",elemento.getIdCategoria());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteCategoria.jasper", "Categorias");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosCategoria)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}