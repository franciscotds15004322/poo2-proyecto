package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.modelo.ModeloDatosCliente;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoCliente extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoCliente() {
        super("Ventana Catalogo Clientes",850,550,new ModeloDatosCliente());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarCliente("Agregar Cliente",500,450,(ModeloDatosCliente)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Cliente elemento = ((ModeloDatosCliente)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarCliente("Modificar Cliente",500,450,(ModeloDatosCliente)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                Cliente elemento = ((ModeloDatosCliente)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idCliente",elemento.getIdCliente());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteCliente.jasper", "Clientes");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosCliente)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}