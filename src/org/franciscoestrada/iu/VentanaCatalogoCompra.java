package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Compra;
import org.franciscoestrada.modelo.ModeloDatosCompra;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoCompra extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoCompra() {
        super("Ventana Catalogo Compras",850,550,new ModeloDatosCompra());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarCompra("Agregar Compra",600,450,(ModeloDatosCompra)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Compra elemento = ((ModeloDatosCompra)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarCompra("Modificar Compra",600,450,(ModeloDatosCompra)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                Compra elemento = ((ModeloDatosCompra)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idCompra",elemento.getIdCompra());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteCompra.jasper", "Compras");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosCompra)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}
