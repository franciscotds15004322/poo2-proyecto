package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.DetalleCompra;
import org.franciscoestrada.modelo.ModeloDatosDetalleCompra;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoDetalleCompra extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoDetalleCompra() {
        super("Ventana Catalogo Detalle Compras",850,550,new ModeloDatosDetalleCompra());    
    }
        
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarDetalleCompra("Agregar Detalle Compra",600,450,(ModeloDatosDetalleCompra)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                DetalleCompra elemento = ((ModeloDatosDetalleCompra)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarDetalleCompra("Modificar Detalle Compra",600,450,(ModeloDatosDetalleCompra)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                DetalleCompra elemento = ((ModeloDatosDetalleCompra)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idDetalleCompra",elemento.getIdDetalleCompra());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteDetalleCompra.jasper", "Detalle Compras");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosDetalleCompra)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}