package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.DetalleFactura;
import org.franciscoestrada.modelo.ModeloDatosDetalleFactura;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoDetalleFactura extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoDetalleFactura() {
        super("Ventana Catalogo Detalle Facturas",850,550,new ModeloDatosDetalleFactura());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarDetalleFactura("Agregar Detalle Factura",600,450,(ModeloDatosDetalleFactura)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                DetalleFactura elemento = ((ModeloDatosDetalleFactura)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarDetalleFactura("Modificar Detalle Factura",600,450,(ModeloDatosDetalleFactura)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                DetalleFactura elemento = ((ModeloDatosDetalleFactura)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idDetalle",elemento.getIdDetalle());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteDetalleFactura.jasper", "Detalle Factura");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosDetalleFactura)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}