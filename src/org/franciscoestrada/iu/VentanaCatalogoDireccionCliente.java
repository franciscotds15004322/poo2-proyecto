package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.DireccionCliente;
import org.franciscoestrada.modelo.ModeloDatosDireccionCliente;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoDireccionCliente extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoDireccionCliente() {
        super("Ventana Catalogo Direcciones Clientes",850,550,new ModeloDatosDireccionCliente());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarDireccionCliente("Agregar Direccion Cliente",600,450,(ModeloDatosDireccionCliente)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                DireccionCliente elemento = ((ModeloDatosDireccionCliente)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarDireccionCliente("Modificar Direccion Cliente",600,450,(ModeloDatosDireccionCliente)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                DireccionCliente elemento = ((ModeloDatosDireccionCliente)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idDireccion",elemento.getIdDireccion());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteDireccionCliente.jasper", "Direccion Clientes");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosDireccionCliente)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}