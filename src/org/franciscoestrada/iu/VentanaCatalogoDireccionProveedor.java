package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.DireccionProveedor;
import org.franciscoestrada.modelo.ModeloDatosDireccionProveedor;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoDireccionProveedor extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoDireccionProveedor() {
        super("Ventana Catalogo Direcciones Proveedor",850,550,new ModeloDatosDireccionProveedor());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarDireccionProveedor("Agregar Direccion Proveedor",600,450,(ModeloDatosDireccionProveedor)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                DireccionProveedor elemento = ((ModeloDatosDireccionProveedor)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarDireccionProveedor("Modificar Direccion Proveedor",600,450,(ModeloDatosDireccionProveedor)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                DireccionProveedor elemento = ((ModeloDatosDireccionProveedor)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idDireccion",elemento.getIdDireccion());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteDireccionProveedor.jasper", "Direccion Proveedores");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosDireccionProveedor)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}
