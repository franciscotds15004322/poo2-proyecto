package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.EmailCliente;
import org.franciscoestrada.modelo.ModeloDatosEmailCliente;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoEmailCliente extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoEmailCliente() {
        super("Ventana Catalogo Email Clientes",850,550,new ModeloDatosEmailCliente());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarEmailCliente("Agregar EmailCliente",600,450,(ModeloDatosEmailCliente)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                EmailCliente elemento = ((ModeloDatosEmailCliente)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarEmailCliente("Modificar Email Cliente",600,450,(ModeloDatosEmailCliente)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                EmailCliente elemento = ((ModeloDatosEmailCliente)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idEmail",elemento.getIdEmail());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteEmailCliente.jasper", "Email Clientes");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosEmailCliente)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}
