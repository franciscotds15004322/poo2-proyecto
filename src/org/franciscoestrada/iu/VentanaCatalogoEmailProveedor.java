package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.EmailProveedor;
import org.franciscoestrada.modelo.ModeloDatosEmailProveedor;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoEmailProveedor extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoEmailProveedor() {
        super("Ventana Catalogo Email Proveedor",850,550,new ModeloDatosEmailProveedor());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarEmailProveedor("Agregar EmailProveedor",600,450,(ModeloDatosEmailProveedor)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                EmailProveedor elemento = ((ModeloDatosEmailProveedor)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarEmailProveedor("Modificar EmailProveedor",600,450,(ModeloDatosEmailProveedor)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                EmailProveedor elemento = ((ModeloDatosEmailProveedor)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idEmail",elemento.getIdEmail());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteEmailProveedor.jasper", "Email Proveedores");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosEmailProveedor)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}
