package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Empaque;
import org.franciscoestrada.modelo.ModeloDatosEmpaque;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoEmpaque extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoEmpaque() {
        super("Ventana Catalogo Empaques",850,550,new ModeloDatosEmpaque());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarEmpaque("Agregar Empaque",500,450,(ModeloDatosEmpaque)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Empaque elemento = ((ModeloDatosEmpaque)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarEmpaque("Modificar Empaque",500,450,(ModeloDatosEmpaque)getModelo(), elemento);
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                Empaque elemento = ((ModeloDatosEmpaque)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idEmpaque",elemento.getIdEmpaque());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteEmpaque.jasper", "Empaques");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosEmpaque)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}