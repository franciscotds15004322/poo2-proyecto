package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Factura;
import org.franciscoestrada.modelo.ModeloDatosFactura;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoFactura extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoFactura() {
        super("Ventana Catalogo Facturas",850,550,new ModeloDatosFactura());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarFactura("Agregar Factura",600,450,(ModeloDatosFactura)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Factura elemento = ((ModeloDatosFactura)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarFactura("Modificar Factura",600,450,(ModeloDatosFactura)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                Factura elemento = ((ModeloDatosFactura)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idFactura",elemento.getIdFactura());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteFactura.jasper", "Facturas");
            }else{
               javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosFactura)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}

    