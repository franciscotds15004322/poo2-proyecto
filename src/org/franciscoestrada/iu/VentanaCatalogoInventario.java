package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Inventario;
import org.franciscoestrada.modelo.ModeloDatosInventario;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoInventario extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoInventario() {
        super("Ventana Catalogo Inventario",850,550,new ModeloDatosInventario());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarInventario("Agregar Inventario",600,450,(ModeloDatosInventario)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Inventario elemento = ((ModeloDatosInventario)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarInventario("Modificar Inventario",600,450,(ModeloDatosInventario)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                Inventario elemento = ((ModeloDatosInventario)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idMovimiento",elemento.getIdMovimiento());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteInventario.jasper", "Inventarios");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosInventario)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}