package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Producto;
import org.franciscoestrada.modelo.ModeloDatosProducto;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoProducto extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoProducto() {
        super("Ventana Catalogo Productos",850,550,new ModeloDatosProducto());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarProducto("Agregar Producto",600,450,(ModeloDatosProducto)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Producto elemento = ((ModeloDatosProducto)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarProducto("Modificar Producto",600,450,(ModeloDatosProducto)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                Producto elemento = ((ModeloDatosProducto)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idProducto",elemento.getIdProducto());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteProducto.jasper", "Productos");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
            ((ModeloDatosProducto)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}
