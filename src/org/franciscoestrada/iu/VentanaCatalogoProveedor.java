package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.modelo.ModeloDatosProveedor;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoProveedor extends VentanaCatalogo implements ActionListener{

    public VentanaCatalogoProveedor() {
        super("Ventana Catalogo Proveedor",850,550,new ModeloDatosProveedor());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarProveedor("Agregar Proveedor",500,450,(ModeloDatosProveedor)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Proveedor elemento = ((ModeloDatosProveedor)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarProveedor("Modificar Proveedor",500,450,(ModeloDatosProveedor)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                Proveedor elemento = ((ModeloDatosProveedor)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idProveedor",elemento.getIdProveedor());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteProveedor.jasper", "Proveedores");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosProveedor)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}