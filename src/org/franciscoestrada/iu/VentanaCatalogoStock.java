package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Stock;
import org.franciscoestrada.modelo.ModeloDatosStock;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoStock extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoStock() {
        super("Ventana Catalogo Stocks",850,550,new ModeloDatosStock());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarStock("Agregar Stock",500,450,(ModeloDatosStock)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Stock elemento = ((ModeloDatosStock)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarStock("Modificar Stock",500,450,(ModeloDatosStock)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                Stock elemento = ((ModeloDatosStock)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idStock",elemento.getIdStock());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteStock.jasper", "Stocks");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosStock)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}