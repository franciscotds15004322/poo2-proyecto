package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TelefonoCliente;
import org.franciscoestrada.modelo.ModeloDatosTelefonoCliente;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoTelefonoCliente extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoTelefonoCliente() {
        super("Ventana Catalogo Telefono Clientes",850,550,new ModeloDatosTelefonoCliente());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarTelefonoCliente("Agregar Telefono Cliente",600,450,(ModeloDatosTelefonoCliente)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                TelefonoCliente elemento = ((ModeloDatosTelefonoCliente)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarTelefonoCliente("Modificar Telefono Cliente",600,450,(ModeloDatosTelefonoCliente)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                TelefonoCliente elemento = ((ModeloDatosTelefonoCliente)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idTelefono",elemento.getIdTelefono());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteTelefonoCliente.jasper", "Telefono Clientes");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosTelefonoCliente)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}
