package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TelefonoProveedor;
import org.franciscoestrada.modelo.ModeloDatosTelefonoProveedor;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoTelefonoProveedor extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoTelefonoProveedor() {
        super("Ventana Catalogo Telefono Proveedor",850,550,new ModeloDatosTelefonoProveedor());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarTelefonoProveedor("Agregar Telefono Proveedor",600,450,(ModeloDatosTelefonoProveedor)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                TelefonoProveedor elemento = ((ModeloDatosTelefonoProveedor)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarTelefonoProveedor("Modificar Telefono Proveedor",600,450,(ModeloDatosTelefonoProveedor)getModelo(), elemento);
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                TelefonoProveedor elemento = ((ModeloDatosTelefonoProveedor)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idTelefono",elemento.getIdTelefono());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteTelefonoProveedor.jasper", "Telefono Proveedores");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosTelefonoProveedor)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}
