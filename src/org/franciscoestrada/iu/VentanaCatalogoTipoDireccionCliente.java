package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TipoDireccionCliente;
import org.franciscoestrada.modelo.ModeloDatosTipoDireccionCliente;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoTipoDireccionCliente extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoTipoDireccionCliente() {
        super("Ventana Catalogo Tipo Direcciones Clientes",850,550,new ModeloDatosTipoDireccionCliente());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarTipoDireccionCliente("Agregar Tipo Direccion Cliente",500,450,(ModeloDatosTipoDireccionCliente)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                TipoDireccionCliente elemento = ((ModeloDatosTipoDireccionCliente)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarTipoDireccionCliente("Modificar Tipo Direccion Cliente",500,450,(ModeloDatosTipoDireccionCliente)getModelo(), elemento);
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                TipoDireccionCliente elemento = ((ModeloDatosTipoDireccionCliente)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idTipo",elemento.getIdTipo());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteTipoDireccionCliente.jasper", "Tipo Direcciones Clientes");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
            ((ModeloDatosTipoDireccionCliente)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}    