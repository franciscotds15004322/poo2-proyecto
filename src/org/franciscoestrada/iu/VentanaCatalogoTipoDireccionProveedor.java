package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TipoDireccionProveedor;
import org.franciscoestrada.modelo.ModeloDatosTipoDireccionProveedor;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoTipoDireccionProveedor extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoTipoDireccionProveedor() {
        super("Ventana Catalogo Tipo Direcciones Proveedor",850,550,new ModeloDatosTipoDireccionProveedor());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarTipoDireccionProveedor("Agregar Tipo Direccion Proveedor",500,450,(ModeloDatosTipoDireccionProveedor)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                TipoDireccionProveedor elemento = ((ModeloDatosTipoDireccionProveedor)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarTipoDireccionProveedor("Modificar Tipo DireccionProveedor",500,450,(ModeloDatosTipoDireccionProveedor)getModelo(), elemento);
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                TipoDireccionProveedor elemento = ((ModeloDatosTipoDireccionProveedor)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idTipo",elemento.getIdTipo());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteTipoDireccionProveedor.jasper", "Tipo Direccion Proveedores");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosTipoDireccionProveedor)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}    