package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TipoEmailCliente;
import org.franciscoestrada.modelo.ModeloDatosTipoEmailCliente;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoTipoEmailCliente extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoTipoEmailCliente() {
        super("Ventana Catalogo Tipo Email Clientes",850,550,new ModeloDatosTipoEmailCliente());    
    }

    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarTipoEmailCliente("Agregar Tipo Email Cliente",500,450,(ModeloDatosTipoEmailCliente)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                TipoEmailCliente elemento = ((ModeloDatosTipoEmailCliente)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarTipoEmailCliente("Modificar Tipo Email Cliente",500,450,(ModeloDatosTipoEmailCliente)getModelo(), elemento);
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                TipoEmailCliente elemento = ((ModeloDatosTipoEmailCliente)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idTipo",elemento.getIdTipo());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteTipoEmailCliente.jasper", "Tipo Email Clientes");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosTipoEmailCliente)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}    