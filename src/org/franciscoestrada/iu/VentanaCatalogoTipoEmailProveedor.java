package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TipoEmailProveedor;
import org.franciscoestrada.modelo.ModeloDatosTipoEmailProveedor;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoTipoEmailProveedor extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoTipoEmailProveedor() {
        super("Ventana Catalogo Tipo Email Proveedor",850,550,new ModeloDatosTipoEmailProveedor());    
    }   

    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarTipoEmailProveedor("Agregar Tipo Email Proveedor",500,450,(ModeloDatosTipoEmailProveedor)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                TipoEmailProveedor elemento = ((ModeloDatosTipoEmailProveedor)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarTipoEmailProveedor("Modificar Tipo Email Proveedor",500,450,(ModeloDatosTipoEmailProveedor)getModelo(), elemento);
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                TipoEmailProveedor elemento = ((ModeloDatosTipoEmailProveedor)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idTipo",elemento.getIdTipo());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteTipoEmailProveedor.jasper", "Tipo Email Proveedores");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
            ((ModeloDatosTipoEmailProveedor)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}    