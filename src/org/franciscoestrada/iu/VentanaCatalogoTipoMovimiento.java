package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TipoMovimiento;
import org.franciscoestrada.modelo.ModeloDatosTipoMovimiento;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoTipoMovimiento extends VentanaCatalogo implements ActionListener {

    public VentanaCatalogoTipoMovimiento() {
        super("Ventana Catalogo Tipo Movimiento",850,550,new ModeloDatosTipoMovimiento());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarTipoMovimiento("Agregar Tipo Movimiento",500,450,(ModeloDatosTipoMovimiento)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                TipoMovimiento elemento = ((ModeloDatosTipoMovimiento)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarTipoMovimiento("Modificar Tipo Movimiento",500,450,(ModeloDatosTipoMovimiento)getModelo(), elemento);
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                TipoMovimiento elemento = ((ModeloDatosTipoMovimiento)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idTipoMovimiento",elemento.getIdTipoMovimiento());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteTipoMovimiento.jasper", "Tipo Movimiento");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosTipoMovimiento)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}