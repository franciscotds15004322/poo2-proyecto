package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TipoTelefonoCliente;
import org.franciscoestrada.modelo.ModeloDatosTipoTelefonoCliente;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoTipoTelefonoCliente extends VentanaCatalogo {

    public VentanaCatalogoTipoTelefonoCliente() {
        super("Ventana Catalogo Tipo Telefono Clientes",850,550,new ModeloDatosTipoTelefonoCliente());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarTipoTelefonoCliente("Agregar Tipo Telefono Cliente",500,450,(ModeloDatosTipoTelefonoCliente)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                TipoTelefonoCliente elemento = ((ModeloDatosTipoTelefonoCliente)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarTipoTelefonoCliente("Modificar Tipo Telefono Cliente",500,450,(ModeloDatosTipoTelefonoCliente)getModelo(), elemento);
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                TipoTelefonoCliente elemento = ((ModeloDatosTipoTelefonoCliente)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idTipo",elemento.getIdTipo());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteTipoTelefonoCliente.jasper", "Tipo Telefono Clientees");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
            ((ModeloDatosTipoTelefonoCliente)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}