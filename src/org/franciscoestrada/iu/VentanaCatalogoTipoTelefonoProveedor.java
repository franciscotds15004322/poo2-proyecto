 package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TipoTelefonoProveedor;
import org.franciscoestrada.modelo.ModeloDatosTipoTelefonoProveedor;
import org.franciscoestrada.reporte.GenerarReporte;

public class VentanaCatalogoTipoTelefonoProveedor extends VentanaCatalogo {

    public VentanaCatalogoTipoTelefonoProveedor() {
        super("Ventana Catalogo Tipo Telefono Proveedores",850,550,new ModeloDatosTipoTelefonoProveedor());    
    }
    
    public void actionPerformed(ActionEvent e){ 
        if(e.getSource() == getBtnAgregar()){
            new VentanaAgregarTipoTelefonoProveedor("Agregar Tipo Telefono Proveedor",500,450,(ModeloDatosTipoTelefonoProveedor)getModelo());
        }else if(e.getSource() == getBtnModificar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                TipoTelefonoProveedor elemento = ((ModeloDatosTipoTelefonoProveedor)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarTipoTelefonoProveedor("Modificar Tipo Telefono Proveedor",500,450,(ModeloDatosTipoTelefonoProveedor)getModelo(), elemento);
            }
            else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnReporte()) {
            if(getTblDatos().getSelectedRow()  != -1){
                Map parametros = new HashMap();
                TipoTelefonoProveedor elemento = ((ModeloDatosTipoTelefonoProveedor)getModelo()).getElemento(getTblDatos().getSelectedRow());
                parametros.put("_idTipo",elemento.getIdTipo());
                GenerarReporte.getInstancia().generarReporte(parametros,"ReporteTipoTelefonoProveedor.jasper", "Tipo Telefono Proveedores");
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }else if(e.getSource() == getBtnEliminar()) {
            if(getTblDatos().getSelectedRow()  != -1){
                ((ModeloDatosTipoTelefonoProveedor)getModelo()).eliminar(getTblDatos().getSelectedRow());
            }else{
                javax.swing.JOptionPane.showMessageDialog(null,"Debe seleccionar un registro");
            }
        }
    }
}