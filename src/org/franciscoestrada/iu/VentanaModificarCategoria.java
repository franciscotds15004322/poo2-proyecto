package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Categoria;
import org.franciscoestrada.modelo.ModeloDatosCategoria;

public class VentanaModificarCategoria extends VentanaAgregar implements ActionListener {
    
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;
    private ModeloDatosCategoria modelo;
    private Categoria categoria;
    
    public VentanaModificarCategoria(String titulo, int ancho, int alto, ModeloDatosCategoria modelo, Categoria categoria) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.categoria = categoria;
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,40,100,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setText(categoria.getDescripcionCategoria());
        txtDescripcion.setBounds(90,40,150,20);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            categoria.setDescripcionCategoria(txtDescripcion.getText());
            modelo.modificar(categoria);
            this.dispose();
        }
    }
}