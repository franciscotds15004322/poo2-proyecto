package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.modelo.ModeloDatosCliente;

public class VentanaModificarCliente extends VentanaAgregar implements ActionListener {
    
    private JLabel lblNit;
    private JTextField txtNit;
    private JLabel lblDpi;
    private JTextField txtDpi;
    private JLabel lblNombre;
    private JTextField txtNombre;
    private ModeloDatosCliente modelo;
    private Cliente cliente;
    
    public VentanaModificarCliente(String titulo, int ancho, int alto, ModeloDatosCliente modelo, Cliente cliente) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.cliente = cliente;
        lblNit = new JLabel("Nit");
        lblNit.setBounds(5,40,100,20);
        txtNit = new JTextField();
        txtNit.setText(cliente.getNit());
        txtNit.setBounds(90,40,150,20);
        lblDpi = new JLabel("Dpi");
        lblDpi.setBounds(5,70,100,20);
        txtDpi = new JTextField();
        txtDpi.setText(cliente.getDpi());
        txtDpi.setBounds(90,70,150,20);
        lblNombre = new JLabel("Nombre");
        lblNombre.setBounds(5,100,100,20);
        txtNombre = new JTextField();
        txtNombre.setText(cliente.getNombre());
        txtNombre.setBounds(90,100,150,20);
        
        this.getContentPane().add(lblNit);
        this.getContentPane().add(txtNit);
        this.getContentPane().add(lblDpi);
        this.getContentPane().add(txtDpi);
        this.getContentPane().add(lblNombre);
        this.getContentPane().add(txtNombre);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            cliente.setNit(txtNit.getText());
            cliente.setDpi(txtDpi.getText());
            cliente.setNombre(txtNombre.getText());
            modelo.modificar(cliente);
            this.dispose();
        }
    }
}