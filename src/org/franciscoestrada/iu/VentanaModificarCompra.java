package org.franciscoestrada.iu;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Compra;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.modelo.ModeloDatosCompra;

public class VentanaModificarCompra extends VentanaAgregar implements ActionListener {
    
    private JLabel lblNumeroDeCompra;
    private JTextField txtNumeroDeCompra;
    private JLabel lblFecha;
    private JTextField txtFecha;
    private JLabel lblIdProveedor;
    private JTextField txtIdProveedor;
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;    
    private JLabel lblTotal;
    private JTextField txtTotal;  
    private ModeloDatosCompra modelo;
    private Compra compra;
    
    public VentanaModificarCompra(String titulo, int ancho, int alto, ModeloDatosCompra modelo, Compra compra) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.compra = compra;

        lblNumeroDeCompra = new JLabel("Numero Compra");
        lblNumeroDeCompra.setBounds(5,40,100,20);
        txtNumeroDeCompra = new JTextField();
        txtNumeroDeCompra.setText(String.valueOf(compra.getNumeroDeCompra()));
        txtNumeroDeCompra.setBounds(90,40,150,20);
        lblFecha = new JLabel("Fecha");
        lblFecha.setBounds(5,70,100,20);
        txtFecha = new JTextField();
        txtFecha.setText(String.valueOf(compra.getFechaCompra()));
        txtFecha.setBounds(90,70,150,20);
        lblIdProveedor = new JLabel("Id Proveedor");
        lblIdProveedor.setBounds(5,100,100,20);
        txtIdProveedor = new JTextField();
        txtIdProveedor.setText(String.valueOf(compra.getProveedor().getIdProveedor()));
        txtIdProveedor.setBounds(90,100,150,20);
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,130,100,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setText(compra.getDescripcionCompra());
        txtDescripcion.setBounds(90,130,150,20);
        lblTotal = new JLabel("Total");
        lblTotal.setBounds(5,160,100,20);
        txtTotal = new JTextField();
        txtTotal.setText(String.valueOf(compra.getTotalCompra()));
        txtTotal.setBounds(90,160,150,20);      

        this.getContentPane().add(lblNumeroDeCompra);
        this.getContentPane().add(txtNumeroDeCompra);
        this.getContentPane().add(lblFecha);
        this.getContentPane().add(txtFecha);
        this.getContentPane().add(lblIdProveedor);
        this.getContentPane().add(txtIdProveedor);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
        this.getContentPane().add(lblTotal);
        this.getContentPane().add(txtTotal);
            
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            int var= Integer.parseInt(txtIdProveedor.getText());
            float var2= Float.parseFloat(txtTotal.getText());
            Date fecha;
            fecha = new Date(txtFecha.getText());
            int var4= Integer.parseInt(txtNumeroDeCompra.getText());           
            compra.setNumeroDeCompra(var4);
            compra.setFechaCompra(fecha);
            compra.setProveedor(new Proveedor(var," "," ",""," "));
            compra.setDescripcionCompra(txtDescripcion.getText());
            compra.setTotalCompra(var2);
            modelo.modificar(compra);
            this.dispose();
        }
    }
}