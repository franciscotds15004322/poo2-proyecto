package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Compra;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.bean.Producto;
import org.franciscoestrada.bean.Empaque;
import org.franciscoestrada.bean.Categoria;
import org.franciscoestrada.bean.Stock;
import org.franciscoestrada.bean.DetalleCompra;
import org.franciscoestrada.modelo.ModeloDatosDetalleCompra;

public class VentanaModificarDetalleCompra extends VentanaAgregar implements ActionListener {
    
    private JLabel lblIdCompra;
    private JTextField txtIdCompra;
    private JLabel lblLineaNo;
    private JTextField txtLineaNo;
    private JLabel lblIdProducto;
    private JTextField txtIdProducto;
    private JLabel lblDescripcionDC;
    private JTextField txtDescripcionDC;    
    private JLabel lblCantidad;
    private JTextField txtCantidad;
    private JLabel lblPrecio;
    private JTextField txtPrecio;
    private JLabel lblTotalLinea;
    private JTextField txtTotalLinea;   
    private ModeloDatosDetalleCompra modelo;
    private DetalleCompra detalleCompra;
    
    public VentanaModificarDetalleCompra(String titulo, int ancho, int alto, ModeloDatosDetalleCompra modelo, DetalleCompra detalleCompra) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.detalleCompra = detalleCompra;

        lblIdCompra = new JLabel("Id Compra");
        lblIdCompra.setBounds(5,40,100,20);
        txtIdCompra = new JTextField();
        txtIdCompra.setText(String.valueOf(detalleCompra.getCompra().getIdCompra()));
        txtIdCompra.setBounds(90,40,150,20);
        lblLineaNo = new JLabel("Linea No");
        lblLineaNo.setBounds(5,70,100,20);
        txtLineaNo = new JTextField();
        txtLineaNo.setText(String.valueOf(detalleCompra.getLineaNoCompra()));
        txtLineaNo.setBounds(90,70,150,20);
        lblIdProducto = new JLabel("Id Producto");
        lblIdProducto.setBounds(5,100,100,20);
        txtIdProducto = new JTextField();
        txtIdProducto.setText(String.valueOf(detalleCompra.getProducto().getIdProducto()));
        txtIdProducto.setBounds(90,100,150,20);
        lblDescripcionDC = new JLabel("Descripcion DC");
        lblDescripcionDC.setBounds(5,130,100,20);
        txtDescripcionDC = new JTextField();
        txtDescripcionDC.setText(detalleCompra.getDescripcionProductoDC());
        txtDescripcionDC.setBounds(90,130,150,20);
        lblCantidad = new JLabel("Cantidad");
        lblCantidad.setBounds(5,160,100,20);
        txtCantidad = new JTextField();
        txtCantidad.setText(String.valueOf(detalleCompra.getUnidadesCompradas()));
        txtCantidad.setBounds(90,160,150,20);
        lblPrecio = new JLabel("Precio");
        lblPrecio.setBounds(5,190,100,20);
        txtPrecio = new JTextField();
        txtPrecio.setText(String.valueOf(detalleCompra.getPrecioCompraUnidad()));
        txtPrecio.setBounds(90,190,150,20);
        lblTotalLinea = new JLabel("Total Linea");
        lblTotalLinea.setBounds(5,220,100,20);
        txtTotalLinea = new JTextField();
        txtTotalLinea.setText(String.valueOf(detalleCompra.getTotalLineaCompra()));
        txtTotalLinea.setBounds(90,220,150,20);       

        this.getContentPane().add(lblIdCompra);
        this.getContentPane().add(txtIdCompra);
        this.getContentPane().add(lblLineaNo);
        this.getContentPane().add(txtLineaNo);
        this.getContentPane().add(lblIdProducto);
        this.getContentPane().add(txtIdProducto);
        this.getContentPane().add(lblDescripcionDC);
        this.getContentPane().add(txtDescripcionDC);
        this.getContentPane().add(lblCantidad);
        this.getContentPane().add(txtCantidad);
        this.getContentPane().add(lblPrecio);
        this.getContentPane().add(txtPrecio);
        this.getContentPane().add(lblTotalLinea);
        this.getContentPane().add(txtTotalLinea);
        
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            float var5= Float.parseFloat(txtPrecio.getText());
            float var6= Float.parseFloat(txtTotalLinea.getText());
            int var1= Integer.parseInt(txtIdCompra.getText());
            int var2= Integer.parseInt(txtLineaNo.getText());
            int var3= Integer.parseInt(txtIdProducto.getText());
            int var4= Integer.parseInt(txtCantidad.getText());
            Date fecha;
            fecha = new Date(82,4,1,10,30,15);
            detalleCompra.setCompra(new Compra(var1,0,fecha,
            new Proveedor(0," "," "," "," ")," ",0));
            detalleCompra.setLineaNoCompra(var2);
            detalleCompra.setProducto(new Producto(var3," "," ",0,0,0,
            new Empaque(0," "), new Categoria(0," "), new Stock(0,0)));
            detalleCompra.setDescripcionProductoDC(txtDescripcionDC.getText());
            detalleCompra.setUnidadesCompradas(var4);
            detalleCompra.setPrecioCompraUnidad(var5);
            detalleCompra.setTotalLineaCompra(var6);                                               
            modelo.modificar(detalleCompra);
            this.dispose();
        }
    }
}