package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Factura;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.bean.Producto;
import org.franciscoestrada.bean.Empaque;
import org.franciscoestrada.bean.Categoria;
import org.franciscoestrada.bean.Stock;
import org.franciscoestrada.bean.DetalleFactura;
import org.franciscoestrada.modelo.ModeloDatosDetalleFactura;

public class VentanaModificarDetalleFactura extends VentanaAgregar implements ActionListener {
    
    private JLabel lblIdFactura;
    private JTextField txtIdFactura;
    private JLabel lblLineaNo;
    private JTextField txtLineaNo;
    private JLabel lblIdProducto;
    private JTextField txtIdProducto;
    private JLabel lblDescripcionDF;
    private JTextField txtDescripcionDF;    
    private JLabel lblCantidad;
    private JTextField txtCantidad;
    private JLabel lblPrecio;
    private JTextField txtPrecio;
    private JLabel lblTotalLinea;
    private JTextField txtTotalLinea;    
    private ModeloDatosDetalleFactura modelo;
    private DetalleFactura detalleFactura;
    
    public VentanaModificarDetalleFactura(String titulo, int ancho, int alto, ModeloDatosDetalleFactura modelo, DetalleFactura detalleFactura) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.detalleFactura = detalleFactura;

        lblIdFactura = new JLabel("Id Factura");
        lblIdFactura.setBounds(5,40,100,20);
        txtIdFactura = new JTextField();
        txtIdFactura.setText(String.valueOf(detalleFactura.getFactura().getIdFactura()));
        txtIdFactura.setBounds(90,40,150,20);
        lblLineaNo = new JLabel("Linea No");
        lblLineaNo.setBounds(5,70,100,20);
        txtLineaNo = new JTextField();
        txtLineaNo.setText(String.valueOf(detalleFactura.getLineaNo()));
        txtLineaNo.setBounds(90,70,150,20);
        lblIdProducto = new JLabel("Id Producto");
        lblIdProducto.setBounds(5,100,100,20);
        txtIdProducto = new JTextField();
        txtIdProducto.setText(String.valueOf(detalleFactura.getProducto().getIdProducto()));
        txtIdProducto.setBounds(90,100,150,20);
        lblDescripcionDF = new JLabel("Descripcion DF");
        lblDescripcionDF.setBounds(5,130,100,20);
        txtDescripcionDF = new JTextField();
        txtDescripcionDF.setText(detalleFactura.getDescripcionDF());
        txtDescripcionDF.setBounds(90,130,150,20);
        lblCantidad = new JLabel("Cantidad");
        lblCantidad.setBounds(5,160,100,20);
        txtCantidad = new JTextField();
        txtCantidad.setText(String.valueOf(detalleFactura.getCantidad()));
        txtCantidad.setBounds(90,160,150,20);
        lblPrecio = new JLabel("Precio");
        lblPrecio.setBounds(5,190,100,20);
        txtPrecio = new JTextField();
        txtPrecio.setText(String.valueOf(detalleFactura.getPrecio()));
        txtPrecio.setBounds(90,190,150,20);
        lblTotalLinea = new JLabel("Total Linea");
        lblTotalLinea.setBounds(5,220,100,20);
        txtTotalLinea = new JTextField();
        txtTotalLinea.setText(String.valueOf(detalleFactura.getTotalLinea()));
        txtTotalLinea.setBounds(90,220,150,20);       

        this.getContentPane().add(lblIdFactura);
        this.getContentPane().add(txtIdFactura);
        this.getContentPane().add(lblLineaNo);
        this.getContentPane().add(txtLineaNo);
        this.getContentPane().add(lblIdProducto);
        this.getContentPane().add(txtIdProducto);
        this.getContentPane().add(lblDescripcionDF);
        this.getContentPane().add(txtDescripcionDF);
        this.getContentPane().add(lblCantidad);
        this.getContentPane().add(txtCantidad);
        this.getContentPane().add(lblPrecio);
        this.getContentPane().add(txtPrecio);
        this.getContentPane().add(lblTotalLinea);
        this.getContentPane().add(txtTotalLinea);
        
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            float var5= Float.parseFloat(txtPrecio.getText());
            float var6= Float.parseFloat(txtTotalLinea.getText());
            int var1= Integer.parseInt(txtIdFactura.getText());
            int var2= Integer.parseInt(txtLineaNo.getText());
            int var3= Integer.parseInt(txtIdProducto.getText());
            int var4= Integer.parseInt(txtCantidad.getText());
            Date fecha;
            fecha = new Date(82,4,1,10,30,15);
            detalleFactura.setFactura(new Factura(var1,0,fecha,
            new Cliente(0," "," "," ")," ",0));
            detalleFactura.setLineaNo(var2);
            detalleFactura.setProducto(new Producto(var3," "," ",0,0,0,
            new Empaque(0," "), new Categoria(0," "), new Stock(0,0)));
            detalleFactura.setDescripcionDF(txtDescripcionDF.getText());
            detalleFactura.setCantidad(var4);
            detalleFactura.setPrecio(var5);
            detalleFactura.setTotalLinea(var6);                                               
            modelo.modificar(detalleFactura);
            this.dispose();
        }
    }
}