package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.bean.TipoDireccionCliente;
import org.franciscoestrada.bean.DireccionCliente;
import org.franciscoestrada.modelo.ModeloDatosDireccionCliente;

public class VentanaModificarDireccionCliente extends VentanaAgregar implements ActionListener {
    
    private JLabel lblIdCliente;
    private JTextField txtIdCliente;
    private JLabel lblIdTipo;
    private JTextField txtIdTipo;
    private JLabel lblDireccion;
    private JTextField txtDireccion;
    private ModeloDatosDireccionCliente modelo;
    private DireccionCliente direccionCliente;
    
    public VentanaModificarDireccionCliente(String titulo, int ancho, int alto, ModeloDatosDireccionCliente modelo, DireccionCliente direccionCliente) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.direccionCliente = direccionCliente;

        lblIdCliente = new JLabel("Id Cliente");
        lblIdCliente.setBounds(5,40,100,20);
        txtIdCliente = new JTextField();
        txtIdCliente.setText(String.valueOf(direccionCliente.getCliente().getIdCliente()));
        txtIdCliente.setBounds(90,40,150,20);
        lblIdTipo = new JLabel("Id Tipo");
        lblIdTipo.setBounds(5,70,100,20);
        txtIdTipo = new JTextField();
        txtIdTipo.setText(String.valueOf(direccionCliente.getTipoDireccionCliente().getIdTipo()));
        txtIdTipo.setBounds(90,70,150,20);
        lblDireccion = new JLabel("Direccion");
        lblDireccion.setBounds(5,100,100,20);
        txtDireccion = new JTextField();
        txtDireccion.setText(direccionCliente.getDireccion());
        txtDireccion.setBounds(90,100,150,20);

        this.getContentPane().add(lblIdCliente);
        this.getContentPane().add(txtIdCliente);
        this.getContentPane().add(lblIdTipo);
        this.getContentPane().add(txtIdTipo);
        this.getContentPane().add(lblDireccion);
        this.getContentPane().add(txtDireccion);
                
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            int var1= Integer.parseInt(txtIdCliente.getText());
            int var2= Integer.parseInt(txtIdTipo.getText());
            direccionCliente.setCliente(new Cliente(var1,"","",""));
            direccionCliente.setTipoDireccionCliente(new TipoDireccionCliente(var2,""));
            direccionCliente.setDireccion(txtDireccion.getText());                            
            modelo.modificar(direccionCliente);
            this.dispose();
        }
    }
}

