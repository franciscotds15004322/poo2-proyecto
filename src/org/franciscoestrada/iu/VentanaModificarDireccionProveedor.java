package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.bean.TipoDireccionProveedor;
import org.franciscoestrada.bean.DireccionProveedor;
import org.franciscoestrada.modelo.ModeloDatosDireccionProveedor;

public class VentanaModificarDireccionProveedor extends VentanaAgregar implements ActionListener {
    
    private JLabel lblIdProveedor;
    private JTextField txtIdProveedor;
    private JLabel lblIdTipo;
    private JTextField txtIdTipo;
    private JLabel lblDireccion;
    private JTextField txtDireccion;
    private ModeloDatosDireccionProveedor modelo;
    private DireccionProveedor direccionProveedor;
    
    public VentanaModificarDireccionProveedor(String titulo, int ancho, int alto, ModeloDatosDireccionProveedor modelo, DireccionProveedor direccionProveedor) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.direccionProveedor = direccionProveedor;

        lblIdProveedor = new JLabel("Id Proveedor");
        lblIdProveedor.setBounds(5,40,100,20);
        txtIdProveedor = new JTextField();
        txtIdProveedor.setText(String.valueOf(direccionProveedor.getProveedor().getIdProveedor()));
        txtIdProveedor.setBounds(90,40,150,20);
        lblIdTipo = new JLabel("Id Tipo");
        lblIdTipo.setBounds(5,70,100,20);
        txtIdTipo = new JTextField();
        txtIdTipo.setText(String.valueOf(direccionProveedor.getTipoDireccionProveedor().getIdTipo()));
        txtIdTipo.setBounds(90,70,150,20);
        lblDireccion = new JLabel("Direccion");
        lblDireccion.setBounds(5,100,100,20);
        txtDireccion = new JTextField();
        txtDireccion.setText(direccionProveedor.getDireccion());
        txtDireccion.setBounds(90,100,150,20);

        this.getContentPane().add(lblIdProveedor);
        this.getContentPane().add(txtIdProveedor);
        this.getContentPane().add(lblIdTipo);
        this.getContentPane().add(txtIdTipo);
        this.getContentPane().add(lblDireccion);
        this.getContentPane().add(txtDireccion);
                
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            int var1= Integer.parseInt(txtIdProveedor.getText());
            int var2= Integer.parseInt(txtIdTipo.getText());
            direccionProveedor.setProveedor(new Proveedor(var1,"","","",""));
            direccionProveedor.setTipoDireccionProveedor(new TipoDireccionProveedor(var2,""));
            direccionProveedor.setDireccion(txtDireccion.getText());                            
            modelo.modificar(direccionProveedor);
            this.dispose();
        }
    }
}

