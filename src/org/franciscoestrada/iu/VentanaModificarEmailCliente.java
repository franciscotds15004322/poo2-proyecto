package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.bean.TipoEmailCliente;
import org.franciscoestrada.bean.EmailCliente;
import org.franciscoestrada.modelo.ModeloDatosEmailCliente;

public class VentanaModificarEmailCliente extends VentanaAgregar implements ActionListener {
    
    private JLabel lblIdCliente;
    private JTextField txtIdCliente;
    private JLabel lblIdTipo;
    private JTextField txtIdTipo;
    private JLabel lblEmail;
    private JTextField txtEmail;
    private ModeloDatosEmailCliente modelo;
    private EmailCliente emailCliente;
    
    public VentanaModificarEmailCliente(String titulo, int ancho, int alto, ModeloDatosEmailCliente modelo, EmailCliente emailCliente) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.emailCliente = emailCliente;

        lblIdCliente = new JLabel("Id Cliente");
        lblIdCliente.setBounds(5,40,100,20);
        txtIdCliente = new JTextField();
        txtIdCliente.setText(String.valueOf(emailCliente.getCliente().getIdCliente()));
        txtIdCliente.setBounds(90,40,150,20);
        lblIdTipo = new JLabel("Id Tipo");
        lblIdTipo.setBounds(5,70,100,20);
        txtIdTipo = new JTextField();
        txtIdTipo.setText(String.valueOf(emailCliente.getTipoEmailCliente().getIdTipo()));
        txtIdTipo.setBounds(90,70,150,20);
        lblEmail = new JLabel("Email");
        lblEmail.setBounds(5,100,100,20);
        txtEmail = new JTextField();
        txtEmail.setText(emailCliente.getEmail());
        txtEmail.setBounds(90,100,150,20);

        this.getContentPane().add(lblIdCliente);
        this.getContentPane().add(txtIdCliente);
        this.getContentPane().add(lblIdTipo);
        this.getContentPane().add(txtIdTipo);
        this.getContentPane().add(lblEmail);
        this.getContentPane().add(txtEmail);
                
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            int var1= Integer.parseInt(txtIdCliente.getText());
            int var2= Integer.parseInt(txtIdTipo.getText());
            emailCliente.setCliente(new Cliente(var1,"","",""));
            emailCliente.setTipoEmailCliente(new TipoEmailCliente(var2,""));
            emailCliente.setEmail(txtEmail.getText());                            
            modelo.modificar(emailCliente);
            this.dispose();
        }
    }
}


