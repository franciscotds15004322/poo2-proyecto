package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.bean.TipoEmailProveedor;
import org.franciscoestrada.bean.EmailProveedor;
import org.franciscoestrada.modelo.ModeloDatosEmailProveedor;

public class VentanaModificarEmailProveedor extends VentanaAgregar implements ActionListener {
    
    private JLabel lblIdProveedor;
    private JTextField txtIdProveedor;
    private JLabel lblIdTipo;
    private JTextField txtIdTipo;
    private JLabel lblEmail;
    private JTextField txtEmail;
    private ModeloDatosEmailProveedor modelo;
    private EmailProveedor emailProveedor;
    
    public VentanaModificarEmailProveedor(String titulo, int ancho, int alto, ModeloDatosEmailProveedor modelo, EmailProveedor emailProveedor) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.emailProveedor = emailProveedor;

        lblIdProveedor = new JLabel("Id Proveedor");
        lblIdProveedor.setBounds(5,40,100,20);
        txtIdProveedor = new JTextField();
        txtIdProveedor.setText(String.valueOf(emailProveedor.getProveedor().getIdProveedor()));
        txtIdProveedor.setBounds(90,40,150,20);
        lblIdTipo = new JLabel("Id Tipo");
        lblIdTipo.setBounds(5,70,100,20);
        txtIdTipo = new JTextField();
        txtIdTipo.setText(String.valueOf(emailProveedor.getTipoEmailProveedor().getIdTipo()));
        txtIdTipo.setBounds(90,70,150,20);
        lblEmail = new JLabel("Email");
        lblEmail.setBounds(5,100,100,20);
        txtEmail = new JTextField();
        txtEmail.setText(emailProveedor.getEmail());
        txtEmail.setBounds(90,100,150,20);

        this.getContentPane().add(lblIdProveedor);
        this.getContentPane().add(txtIdProveedor);
        this.getContentPane().add(lblIdTipo);
        this.getContentPane().add(txtIdTipo);
        this.getContentPane().add(lblEmail);
        this.getContentPane().add(txtEmail);
                
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            int var1= Integer.parseInt(txtIdProveedor.getText());
            int var2= Integer.parseInt(txtIdTipo.getText());
            emailProveedor.setProveedor(new Proveedor(var1,"","","",""));
            emailProveedor.setTipoEmailProveedor(new TipoEmailProveedor(var2,""));
            emailProveedor.setEmail(txtEmail.getText());                            
            modelo.modificar(emailProveedor);
            this.dispose();
        }
    }
}


