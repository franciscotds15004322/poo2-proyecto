package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Empaque;
import org.franciscoestrada.modelo.ModeloDatosEmpaque;

public class VentanaModificarEmpaque extends VentanaAgregar implements ActionListener {
    
    private JLabel lblEmpaque;
    private JTextField txtEmpaque;
    private ModeloDatosEmpaque modelo;
    private Empaque empaque;
    
    public VentanaModificarEmpaque(String titulo, int ancho, int alto, ModeloDatosEmpaque modelo, Empaque empaque) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.empaque = empaque;
        lblEmpaque = new JLabel("Descripcion Empaque");
        lblEmpaque.setBounds(5,40,150,20);
        txtEmpaque = new JTextField();
        txtEmpaque.setText(empaque.getDescripcionEmpaque());
        txtEmpaque.setBounds(160,40,150,20);
        this.getContentPane().add(lblEmpaque);
        this.getContentPane().add(txtEmpaque);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            empaque.setDescripcionEmpaque(txtEmpaque.getText());
            modelo.modificar(empaque);
            this.dispose();
        }
    }
}