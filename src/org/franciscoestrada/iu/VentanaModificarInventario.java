package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Producto;
import org.franciscoestrada.bean.Empaque;
import org.franciscoestrada.bean.Categoria;
import org.franciscoestrada.bean.Stock;
import org.franciscoestrada.bean.Inventario;
import org.franciscoestrada.bean.TipoMovimiento;
import org.franciscoestrada.modelo.ModeloDatosInventario;

public class VentanaModificarInventario extends VentanaAgregar implements ActionListener {
    
    private JLabel lblFechaInventario;
    private JTextField txtFechaInventario;
    private JLabel lblIdProducto;
    private JTextField txtIdProducto;
    private JLabel lblIdTipoMovimiento;
    private JTextField txtIdTipoMovimiento;
    private JLabel lblIdFactura;
    private JTextField txtIdFactura;    
    private JLabel lblIdCompra;
    private JTextField txtIdCompra;
    private JLabel lblLineaNoInventario;
    private JTextField txtLineaNoInventario;
    private JLabel lblCantidad;
    private JTextField txtCantidad;
    private JLabel lblPrecioCompraUnitario;
    private JTextField txtPrecioCompraUnitario;  
    private ModeloDatosInventario modelo;
    private Inventario inventario;
    
    public VentanaModificarInventario(String titulo, int ancho, int alto, ModeloDatosInventario modelo, Inventario inventario) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.inventario = inventario;
      
        lblFechaInventario = new JLabel("Fecha Inventario");
        lblFechaInventario.setBounds(5,40,100,20);
        txtFechaInventario = new JTextField();
        txtFechaInventario.setText(String.valueOf(inventario.getFechaInventario()));
        txtFechaInventario.setBounds(90,40,150,20);
        lblIdProducto = new JLabel("Id Producto");
        lblIdProducto.setBounds(5,70,100,20);
        txtIdProducto = new JTextField();
        txtIdProducto.setText(String.valueOf(inventario.getProducto().getIdProducto()));
        txtIdProducto.setBounds(90,70,150,20);
        lblIdTipoMovimiento = new JLabel("Id Tipo Movimiento");
        lblIdTipoMovimiento.setBounds(5,100,100,20);
        txtIdTipoMovimiento = new JTextField();
        txtIdTipoMovimiento.setText(String.valueOf(inventario.getTipoMovimiento().getIdTipoMovimiento()));
        txtIdTipoMovimiento.setBounds(90,100,150,20);
        lblIdFactura = new JLabel("Id Factura");
        lblIdFactura.setBounds(5,130,100,20);
        txtIdFactura = new JTextField();
        txtIdFactura.setText(String.valueOf(inventario.getIdFactura()));
        txtIdFactura.setBounds(90,130,150,20);
        lblIdCompra = new JLabel("Id Compra");
        lblIdCompra.setBounds(5,160,100,20);
        txtIdCompra = new JTextField();
        txtIdCompra.setText(String.valueOf(inventario.getIdCompra()));
        txtIdCompra.setBounds(90,160,150,20);
        lblLineaNoInventario = new JLabel("Linea No Inventario");
        lblLineaNoInventario.setBounds(5,190,100,20);
        txtLineaNoInventario = new JTextField();
        txtLineaNoInventario.setText(String.valueOf(inventario.getLineaNoInventario()));
        txtLineaNoInventario.setBounds(90,190,150,20);
        lblCantidad = new JLabel("Cantidad");
        lblCantidad.setBounds(5,220,100,20);
        txtCantidad = new JTextField();
        txtCantidad.setText(String.valueOf(inventario.getCantidad()));
        txtCantidad.setBounds(90,220,150,20);
        lblPrecioCompraUnitario = new JLabel("Precio Compra Unitario");
        lblPrecioCompraUnitario.setBounds(5,250,100,20);
        txtPrecioCompraUnitario = new JTextField();
        txtPrecioCompraUnitario.setText(String.valueOf(inventario.getPrecioCompraUnitario()));
        txtPrecioCompraUnitario.setBounds(90,250,150,20);        

        this.getContentPane().add(lblFechaInventario);
        this.getContentPane().add(txtFechaInventario);
        this.getContentPane().add(lblIdProducto);
        this.getContentPane().add(txtIdProducto);
        this.getContentPane().add(lblIdTipoMovimiento);
        this.getContentPane().add(txtIdTipoMovimiento);
        this.getContentPane().add(lblIdFactura);
        this.getContentPane().add(txtIdFactura);
        this.getContentPane().add(lblIdCompra);
        this.getContentPane().add(txtIdCompra);
        this.getContentPane().add(lblLineaNoInventario);
        this.getContentPane().add(txtLineaNoInventario);
        this.getContentPane().add(lblCantidad);
        this.getContentPane().add(txtCantidad);
        this.getContentPane().add(lblPrecioCompraUnitario);
        this.getContentPane().add(txtPrecioCompraUnitario);
        
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            int var1= Integer.parseInt(txtIdProducto.getText());
            int var2= Integer.parseInt(txtIdTipoMovimiento.getText());
            int var3= Integer.parseInt(txtIdFactura.getText());
            int var4= Integer.parseInt(txtIdCompra.getText());
            int var5= Integer.parseInt(txtLineaNoInventario.getText());
            int var6= Integer.parseInt(txtCantidad.getText());
            Date fecha;
            fecha = new Date(txtFechaInventario.getText());
            float var7= Float.parseFloat(txtPrecioCompraUnitario.getText());
            inventario.setFechaInventario(fecha);
            inventario.setProducto(new Producto(var1," "," ",0,0,0,
            new Empaque(0," "), new Categoria(0," "), new Stock(0,0)));
            inventario.setTipoMovimiento(new TipoMovimiento(var2,""));
            inventario.setIdFactura(var3);
            inventario.setIdCompra(var4);
            inventario.setLineaNoInventario(var5);
            inventario.setCantidad(var6);
            inventario.setPrecioCompraUnitario(var7);                            
            modelo.modificar(inventario);
            this.dispose();
        }
    }
}
