package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Producto;
import org.franciscoestrada.bean.Empaque;
import org.franciscoestrada.bean.Categoria;
import org.franciscoestrada.bean.Stock;
import org.franciscoestrada.modelo.ModeloDatosProducto;

public class VentanaModificarProducto extends VentanaAgregar implements ActionListener {
    
    private JLabel lblNombreProducto;
    private JTextField txtNombreProducto;
    private JLabel lblDescripcionProducto;
    private JTextField txtDescripcionProducto;
    private JLabel lblPrecioUnitario;
    private JTextField txtPrecioUnitario;
    private JLabel lblPrecioPorDocena;
    private JTextField txtPrecioPorDocena;    
    private JLabel lblPrecioPorMayor;
    private JTextField txtPrecioPorMayor;
    private JLabel lblIdEmpaque;
    private JTextField txtIdEmpaque;
    private JLabel lblIdCategoria;
    private JTextField txtIdCategoria;
    private JLabel lblIdStock;
    private JTextField txtIdStock;  
    private ModeloDatosProducto modelo;
    private Producto producto;
    
    public VentanaModificarProducto(String titulo, int ancho, int alto, ModeloDatosProducto modelo, Producto producto) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.producto = producto;

        lblNombreProducto = new JLabel("Nombre");
        lblNombreProducto.setBounds(5,40,100,20);
        txtNombreProducto = new JTextField();
        txtNombreProducto.setText(producto.getNombreProducto());
        txtNombreProducto.setBounds(90,40,150,20);
        lblDescripcionProducto = new JLabel("Descripcion");
        lblDescripcionProducto.setBounds(5,70,100,20);
        txtDescripcionProducto = new JTextField();
        txtDescripcionProducto.setText(producto.getDescripcionProducto());
        txtDescripcionProducto.setBounds(90,70,150,20);
        lblPrecioUnitario = new JLabel("P Unitario");
        lblPrecioUnitario.setBounds(5,100,100,20);
        txtPrecioUnitario = new JTextField();
        txtPrecioUnitario.setText(String.valueOf(producto.getPrecioUnitario()));
        txtPrecioUnitario.setBounds(90,100,150,20);
        lblPrecioPorDocena = new JLabel("P Docena");
        lblPrecioPorDocena.setBounds(5,130,100,20);
        txtPrecioPorDocena = new JTextField();
        txtPrecioPorDocena.setText(String.valueOf(producto.getPrecioPorDocena()));
        txtPrecioPorDocena.setBounds(90,130,150,20);
        lblPrecioPorMayor = new JLabel("P Mayor");
        lblPrecioPorMayor.setBounds(5,160,100,20);
        txtPrecioPorMayor = new JTextField();
        txtPrecioPorMayor.setText(String.valueOf(producto.getPrecioPorMayor()));
        txtPrecioPorMayor.setBounds(90,160,150,20);
        lblIdEmpaque = new JLabel("Id Empaque");
        lblIdEmpaque.setBounds(5,190,100,20);
        txtIdEmpaque = new JTextField();
        txtIdEmpaque.setText(String.valueOf(producto.getEmpaque().getIdEmpaque()));
        txtIdEmpaque.setBounds(90,190,150,20);
        lblIdCategoria = new JLabel("Id Categoria");
        lblIdCategoria.setBounds(5,220,100,20);
        txtIdCategoria = new JTextField();
        txtIdCategoria.setText(String.valueOf(producto.getCategoria().getIdCategoria()));
        txtIdCategoria.setBounds(90,220,150,20);
        lblIdStock = new JLabel("Id Stock");
        lblIdStock.setBounds(5,250,100,20);
        txtIdStock = new JTextField();
        txtIdStock.setText(String.valueOf(producto.getStock().getIdStock()));
        txtIdStock.setBounds(90,250,150,20); 
        
        
        this.getContentPane().add(lblNombreProducto);
        this.getContentPane().add(txtNombreProducto);
        this.getContentPane().add(lblDescripcionProducto);
        this.getContentPane().add(txtDescripcionProducto);
        this.getContentPane().add(lblPrecioUnitario);
        this.getContentPane().add(txtPrecioUnitario);
        this.getContentPane().add(lblPrecioPorDocena);
        this.getContentPane().add(txtPrecioPorDocena);
        this.getContentPane().add(lblPrecioPorMayor);
        this.getContentPane().add(txtPrecioPorMayor);
        this.getContentPane().add(lblIdEmpaque);
        this.getContentPane().add(txtIdEmpaque);
        this.getContentPane().add(lblIdCategoria);
        this.getContentPane().add(txtIdCategoria);
        this.getContentPane().add(lblIdStock);
        this.getContentPane().add(txtIdStock);
        
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            float var= Float.parseFloat(txtPrecioUnitario.getText());
            float var2= Float.parseFloat(txtPrecioPorDocena.getText());
            float var3= Float.parseFloat(txtPrecioPorMayor.getText());
            int var4= Integer.parseInt(txtIdEmpaque.getText());
            int var5= Integer.parseInt(txtIdCategoria.getText());
            int var6= Integer.parseInt(txtIdStock.getText());
            producto.setNombreProducto(txtNombreProducto.getText());
            producto.setDescripcionProducto(txtDescripcionProducto.getText());
            producto.setPrecioUnitario(var);
            producto.setPrecioPorMayor(var2);
            producto.setPrecioPorDocena(var3);
            producto.setEmpaque(new Empaque(var4," "));
            producto.setCategoria(new Categoria(var5," "));
            producto.setStock(new Stock(var6,0));                            
            modelo.modificar(producto);
            this.dispose();
        }
    }
}