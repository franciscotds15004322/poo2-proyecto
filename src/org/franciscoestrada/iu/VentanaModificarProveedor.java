package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.modelo.ModeloDatosProveedor;

public class VentanaModificarProveedor extends VentanaAgregar implements ActionListener {
    
    private JLabel lblNit;
    private JTextField txtNit;
    private JLabel lblNombre;
    private JTextField txtNombre;
    private JLabel lblPaginaWeb;
    private JTextField txtPaginaWeb;
    private JLabel lblContacto;
    private JTextField txtContacto;
    private ModeloDatosProveedor modelo;
    private Proveedor proveedor;
    
    public VentanaModificarProveedor(String titulo, int ancho, int alto, ModeloDatosProveedor modelo, Proveedor proveedor) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.proveedor = proveedor;
        lblNit = new JLabel("Nit");
        lblNit.setBounds(5,40,100,20);
        txtNit = new JTextField();
        txtNit.setText(proveedor.getNit());
        txtNit.setBounds(90,40,150,20);
        lblNombre = new JLabel("Dpi");
        lblNombre.setBounds(5,70,100,20);
        txtNombre = new JTextField();
        txtNombre.setText(proveedor.getNombre());
        txtNombre.setBounds(90,70,150,20);
        lblPaginaWeb = new JLabel("Nombre");
        lblPaginaWeb.setBounds(5,100,100,20);
        txtPaginaWeb = new JTextField();
        txtPaginaWeb.setText(proveedor.getPaginaWeb());
        txtPaginaWeb.setBounds(90,100,150,20);
        lblContacto = new JLabel("Contacto");
        lblContacto.setBounds(5,130,100,20);
        txtContacto = new JTextField();
        txtContacto.setText(proveedor.getContacto());
        txtContacto.setBounds(90,130,150,20);
        
        this.getContentPane().add(lblNit);
        this.getContentPane().add(txtNit);
        this.getContentPane().add(lblNombre);
        this.getContentPane().add(txtNombre);
        this.getContentPane().add(lblPaginaWeb);
        this.getContentPane().add(txtPaginaWeb);
        this.getContentPane().add(lblContacto);
        this.getContentPane().add(txtContacto);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            proveedor.setNit(txtNit.getText());
            proveedor.setNombre(txtNombre.getText());
            proveedor.setPaginaWeb(txtPaginaWeb.getText());
            proveedor.setContacto(txtContacto.getText());
            modelo.modificar(proveedor);
            this.dispose();
        }
    }
}