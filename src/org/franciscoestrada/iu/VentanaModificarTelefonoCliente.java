package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.bean.TipoTelefonoCliente;
import org.franciscoestrada.bean.TelefonoCliente;
import org.franciscoestrada.modelo.ModeloDatosTelefonoCliente;

public class VentanaModificarTelefonoCliente extends VentanaAgregar implements ActionListener {
    
    private JLabel lblIdCliente;
    private JTextField txtIdCliente;
    private JLabel lblIdTipo;
    private JTextField txtIdTipo;
    private JLabel lblTelefono;
    private JTextField txtTelefono;
    private ModeloDatosTelefonoCliente modelo;
    private TelefonoCliente telefonoCliente;
    
    public VentanaModificarTelefonoCliente(String titulo, int ancho, int alto, ModeloDatosTelefonoCliente modelo, TelefonoCliente telefonoCliente) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.telefonoCliente = telefonoCliente;

        lblIdCliente = new JLabel("Id Cliente");
        lblIdCliente.setBounds(5,40,100,20);
        txtIdCliente = new JTextField();
        txtIdCliente.setText(String.valueOf(telefonoCliente.getCliente().getIdCliente()));
        txtIdCliente.setBounds(90,40,150,20);
        lblIdTipo = new JLabel("Id Tipo");
        lblIdTipo.setBounds(5,70,100,20);
        txtIdTipo = new JTextField();
        txtIdTipo.setText(String.valueOf(telefonoCliente.getTipoTelefonoCliente().getIdTipo()));
        txtIdTipo.setBounds(90,70,150,20);
        lblTelefono = new JLabel("Telefono");
        lblTelefono.setBounds(5,100,100,20);
        txtTelefono = new JTextField();
        txtTelefono.setText(telefonoCliente.getTelefono());
        txtTelefono.setBounds(90,100,150,20);

        this.getContentPane().add(lblIdCliente);
        this.getContentPane().add(txtIdCliente);
        this.getContentPane().add(lblIdTipo);
        this.getContentPane().add(txtIdTipo);
        this.getContentPane().add(lblTelefono);
        this.getContentPane().add(txtTelefono);
                
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            int var1= Integer.parseInt(txtIdCliente.getText());
            int var2= Integer.parseInt(txtIdTipo.getText());
            telefonoCliente.setCliente(new Cliente(var1,"","",""));
            telefonoCliente.setTipoTelefonoCliente(new TipoTelefonoCliente(var2,""));
            telefonoCliente.setTelefono(txtTelefono.getText());                            
            modelo.modificar(telefonoCliente);
            this.dispose();
        }
    }
}
