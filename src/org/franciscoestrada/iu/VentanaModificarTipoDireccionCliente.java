package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.TipoDireccionCliente;
import org.franciscoestrada.modelo.ModeloDatosTipoDireccionCliente;

public class VentanaModificarTipoDireccionCliente extends VentanaAgregar implements ActionListener {
    
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;
    private ModeloDatosTipoDireccionCliente modelo;
    private TipoDireccionCliente tipoDireccionCliente;
    
    public VentanaModificarTipoDireccionCliente(String titulo, int ancho, int alto, ModeloDatosTipoDireccionCliente modelo, TipoDireccionCliente tipoDireccionCliente) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.tipoDireccionCliente = tipoDireccionCliente;
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,40,150,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setText(tipoDireccionCliente.getDescripcion());
        txtDescripcion.setBounds(160,40,150,20);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            tipoDireccionCliente.setDescripcion(txtDescripcion.getText());
            modelo.modificar(tipoDireccionCliente);
            this.dispose();
        }
    }
}