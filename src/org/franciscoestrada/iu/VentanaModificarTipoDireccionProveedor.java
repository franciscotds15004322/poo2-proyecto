package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.TipoDireccionProveedor;
import org.franciscoestrada.modelo.ModeloDatosTipoDireccionProveedor;

public class VentanaModificarTipoDireccionProveedor extends VentanaAgregar implements ActionListener {
    
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;
    private ModeloDatosTipoDireccionProveedor modelo;
    private TipoDireccionProveedor tipoDireccionProveedor;
    
    public VentanaModificarTipoDireccionProveedor(String titulo, int ancho, int alto, ModeloDatosTipoDireccionProveedor modelo, TipoDireccionProveedor tipoDireccionProveedor) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.tipoDireccionProveedor = tipoDireccionProveedor;
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,40,150,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setText(tipoDireccionProveedor.getDescripcion());
        txtDescripcion.setBounds(160,40,150,20);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            tipoDireccionProveedor.setDescripcion(txtDescripcion.getText());
            modelo.modificar(tipoDireccionProveedor);
            this.dispose();
        }
    }
}