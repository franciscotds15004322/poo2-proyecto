package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.TipoEmailCliente;
import org.franciscoestrada.modelo.ModeloDatosTipoEmailCliente;

public class VentanaModificarTipoEmailCliente extends VentanaAgregar implements ActionListener {
    
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;
    private ModeloDatosTipoEmailCliente modelo;
    private TipoEmailCliente tipoEmailCliente;
    
    public VentanaModificarTipoEmailCliente(String titulo, int ancho, int alto, ModeloDatosTipoEmailCliente modelo, TipoEmailCliente tipoEmailCliente) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.tipoEmailCliente = tipoEmailCliente;
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,40,150,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setText(tipoEmailCliente.getDescripcion());
        txtDescripcion.setBounds(160,40,150,20);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            tipoEmailCliente.setDescripcion(txtDescripcion.getText());
            modelo.modificar(tipoEmailCliente);
            this.dispose();
        }
    }
}