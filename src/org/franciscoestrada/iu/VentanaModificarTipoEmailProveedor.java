package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.TipoEmailProveedor;
import org.franciscoestrada.modelo.ModeloDatosTipoEmailProveedor;

public class VentanaModificarTipoEmailProveedor extends VentanaAgregar implements ActionListener {
    
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;
    private ModeloDatosTipoEmailProveedor modelo;
    private TipoEmailProveedor tipoEmailProveedor;
    
    public VentanaModificarTipoEmailProveedor(String titulo, int ancho, int alto, ModeloDatosTipoEmailProveedor modelo, TipoEmailProveedor tipoEmailProveedor) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.tipoEmailProveedor = tipoEmailProveedor;
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,40,150,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setText(tipoEmailProveedor.getDescripcion());
        txtDescripcion.setBounds(160,40,150,20);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            tipoEmailProveedor.setDescripcion(txtDescripcion.getText());
            modelo.modificar(tipoEmailProveedor);
            this.dispose();
        }
    }
}