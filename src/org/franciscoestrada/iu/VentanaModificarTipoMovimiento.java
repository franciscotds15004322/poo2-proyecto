package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.TipoMovimiento;
import org.franciscoestrada.modelo.ModeloDatosTipoMovimiento;

public class VentanaModificarTipoMovimiento extends VentanaAgregar implements ActionListener {
    
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;
    private ModeloDatosTipoMovimiento modelo;
    private TipoMovimiento tipoMovimiento;
    
    public VentanaModificarTipoMovimiento(String titulo, int ancho, int alto, ModeloDatosTipoMovimiento modelo, TipoMovimiento tipoMovimiento) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.tipoMovimiento = tipoMovimiento;
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,40,150,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setText(tipoMovimiento.getDescripcion());
        txtDescripcion.setBounds(160,40,150,20);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            tipoMovimiento.setDescripcion(txtDescripcion.getText());
            modelo.modificar(tipoMovimiento);
            this.dispose();
        }
    }
}