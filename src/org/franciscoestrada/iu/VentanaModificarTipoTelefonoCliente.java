package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.TipoTelefonoCliente;
import org.franciscoestrada.modelo.ModeloDatosTipoTelefonoCliente;

public class VentanaModificarTipoTelefonoCliente extends VentanaAgregar implements ActionListener {
    
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;
    private ModeloDatosTipoTelefonoCliente modelo;
    private TipoTelefonoCliente tipoTelefonoCliente;
    
    public VentanaModificarTipoTelefonoCliente(String titulo, int ancho, int alto, ModeloDatosTipoTelefonoCliente modelo, TipoTelefonoCliente tipoTelefonoCliente) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.tipoTelefonoCliente = tipoTelefonoCliente;
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,40,150,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setText(tipoTelefonoCliente.getDescripcion());
        txtDescripcion.setBounds(160,40,150,20);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            tipoTelefonoCliente.setDescripcion(txtDescripcion.getText());
            modelo.modificar(tipoTelefonoCliente);
            this.dispose();
        }
    }
}