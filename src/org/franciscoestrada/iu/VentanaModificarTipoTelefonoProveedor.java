package org.franciscoestrada.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.franciscoestrada.bean.TipoTelefonoProveedor;
import org.franciscoestrada.modelo.ModeloDatosTipoTelefonoProveedor;

public class VentanaModificarTipoTelefonoProveedor extends VentanaAgregar implements ActionListener {
    
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;
    private ModeloDatosTipoTelefonoProveedor modelo;
    private TipoTelefonoProveedor tipoTelefonoProveedor;
    
    public VentanaModificarTipoTelefonoProveedor(String titulo, int ancho, int alto, ModeloDatosTipoTelefonoProveedor modelo, TipoTelefonoProveedor tipoTelefonoProveedor) {
        super(titulo, ancho, alto);
        this.modelo = modelo;
        this.tipoTelefonoProveedor = tipoTelefonoProveedor;
        lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setBounds(5,40,150,20);
        txtDescripcion = new JTextField();
        txtDescripcion.setText(tipoTelefonoProveedor.getDescripcion());
        txtDescripcion.setBounds(160,40,150,20);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            tipoTelefonoProveedor.setDescripcion(txtDescripcion.getText());
            modelo.modificar(tipoTelefonoProveedor);
            this.dispose();
        }
    }
}