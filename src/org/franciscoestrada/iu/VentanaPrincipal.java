package org.franciscoestrada.iu;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class VentanaPrincipal extends JFrame implements ActionListener {
    private JDesktopPane panelVentana;
    private JMenuBar menuPrincipal;
    private JMenu menuClientes;
    private JMenu menuProveedores;
    private JMenu menuProducto;
    private JMenu menuComprasYFacturas;
    //Clientes
    private JMenuItem clientes;
    private JMenuItem direccionClientes;
    private JMenuItem tipoDireccionClientes;
    private JMenuItem telefonoClientes;
    private JMenuItem tipoTelefonoClientes;
    private JMenuItem emailClientes;
    private JMenuItem tipoEmailClientes;
    //Proveedores
    private JMenuItem proveedores;
    private JMenuItem direccionProveedores;
    private JMenuItem tipoDireccionProveedores;
    private JMenuItem telefonoProveedores;
    private JMenuItem tipoTelefonoProveedores;
    private JMenuItem emailProveedores;
    private JMenuItem tipoEmailProveedores;
    //Productos
    private JMenuItem productos;
    private JMenuItem categoria;
    private JMenuItem stocks;
    private JMenuItem empaques;
    //Catalogos
    private JMenuItem factura;
    private JMenuItem compra;
    private JMenuItem detalleFactura;
    private JMenuItem detalleCompra;
    private JMenuItem tiposDeMovimiento;
    private JMenuItem inventarios;
        
    public VentanaPrincipal() {
        this.setTitle(" CONTROL DE PUNTO DE VENTA 5.0 ");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setLocationRelativeTo(null);
        this.setIconImage(new ImageIcon(getClass().getResource("/org/franciscoestrada/recursos/inventar.png")).getImage());
        panelVentana = new JDesktopPane(){
            @Override
            public void paintComponent(Graphics g){
                g.drawImage(new ImageIcon(getClass().getResource("/org/franciscoestrada/recursos/fondo.jpg")).getImage(), 0, 0,getWidth(),
                        getHeight(),this);
            }                    
        };
        this.setJMenuBar(getMenuPrincipal());
        this.getContentPane().add(panelVentana,BorderLayout.CENTER);
        this.setVisible(true);                  
    }

    public JMenuBar getMenuPrincipal() {
        if(menuPrincipal == null){
            menuPrincipal = new JMenuBar();
            menuPrincipal.add(getMenuClientes());
            menuPrincipal.add(getMenuProveedores());
            menuPrincipal.add(getMenuProducto());
            menuPrincipal.add(getMenuComprasYFacturas());
        }
        return menuPrincipal;
    }
    
    // Menu Clientes
    public JMenu getMenuClientes() {
        if(menuClientes == null){
            menuClientes = new JMenu("Clientes");
            menuClientes.setIcon(new ImageIcon(getClass().getResource("/org/franciscoestrada/recursos/clientes.png")));
            menuClientes.setMnemonic('C');
            menuClientes.setToolTipText("Clientes");
            menuClientes.add(getClientes());
            menuClientes.add(getDireccionClientes());
            menuClientes.add(getTipoDireccionClientes());
            menuClientes.add(getTelefonoClientes());
            menuClientes.add(getTipoTelefonoClientes());
            menuClientes.add(getEmailClientes());
            menuClientes.add(getTipoEmailClientes());
        }
        return menuClientes;
    }
    
    // Menu Proveedores
    public JMenu getMenuProveedores() {
        if(menuProveedores == null){
            menuProveedores = new JMenu("Proveedores");
            menuProveedores.setIcon(new ImageIcon(getClass().getResource("/org/franciscoestrada/recursos/proveedores.png")));
            menuProveedores.setMnemonic('P');
            menuProveedores.setToolTipText("Proveedores");
            menuProveedores.add(getProveedores());
            menuProveedores.add(getDireccionProveedores());
            menuProveedores.add(getTipoDireccionProveedores());
            menuProveedores.add(getTelefonoProveedores());
            menuProveedores.add(getTipoTelefonoProveedores());
            menuProveedores.add(getEmailProveedores());
            menuProveedores.add(getTipoEmailProveedores());
        }
        return menuProveedores;
    }

    // Menu Producto
    public JMenu getMenuProducto() {
        if(menuProducto == null){
            menuProducto = new JMenu("Producto");
            menuProducto.setIcon(new ImageIcon(getClass().getResource("/org/franciscoestrada/recursos/producto.png")));
            menuProducto.setMnemonic('u');
            menuProducto.setToolTipText("Producto");
            menuProducto.add(getProductos());
            menuProducto.add(getCategoria());
            menuProducto.add(getStocks());
            menuProducto.add(getEmpaques());
        }
        return menuProducto;
    }    
    // Menu Catalogos
    public JMenu getMenuComprasYFacturas() {
        if(menuComprasYFacturas == null){
            menuComprasYFacturas = new JMenu("Compras Y Facturas");
            menuComprasYFacturas.setIcon(new ImageIcon(getClass().getResource("/org/franciscoestrada/recursos/catalogo.png")));
            menuComprasYFacturas.setMnemonic('Y');
            menuComprasYFacturas.setToolTipText("Compras Y Facturas");
            menuComprasYFacturas.add(getFactura());
            menuComprasYFacturas.add(getCompra());
            menuComprasYFacturas.add(getDetalleFactura());
            menuComprasYFacturas.add(getDetalleCompra());
            menuComprasYFacturas.add(getInventarios());
            menuComprasYFacturas.add(getTiposDeMovimiento());            
        }
        return menuComprasYFacturas;
    }
    
    // JMenuItems Clientes
    public JMenuItem getClientes() {
        if(clientes == null){
            clientes = new JMenuItem("Clientes");
            clientes.setMnemonic('l');
            clientes.setToolTipText("Clientes");
            clientes.addActionListener(this);
        }        
        return clientes;
    }

    public JMenuItem getDireccionClientes() {
        if(direccionClientes == null){
            direccionClientes = new JMenuItem("Direccion Clientes");
            direccionClientes.setMnemonic('D');
            direccionClientes.setToolTipText("Direccion Clientes");
            direccionClientes.addActionListener(this);
        }
        return direccionClientes;
    }

    public JMenuItem getTipoDireccionClientes() {
        if(tipoDireccionClientes == null){
            tipoDireccionClientes = new JMenuItem("Tipo Direccion Clientes");
            tipoDireccionClientes.setMnemonic('i');
            tipoDireccionClientes.setToolTipText("Tipo Direccion Clientes");
            tipoDireccionClientes.addActionListener(this);
        }
        return tipoDireccionClientes;
    }

    public JMenuItem getTelefonoClientes() {
        if(telefonoClientes == null){
            telefonoClientes = new JMenuItem("Telefono Clientes");
            telefonoClientes.setMnemonic('T');
            telefonoClientes.setToolTipText("Telefono Clientes");
            telefonoClientes.addActionListener(this);
        }
        return telefonoClientes;
    }

    public JMenuItem getTipoTelefonoClientes() {
        if(tipoTelefonoClientes == null){
            tipoTelefonoClientes = new JMenuItem("Tipo Telefono Clientes");
            tipoTelefonoClientes.setMnemonic('o');
            tipoTelefonoClientes.setToolTipText("Tipo Telefono Clientes");
            tipoTelefonoClientes.addActionListener(this);
        }
        return tipoTelefonoClientes;
    }

    public JMenuItem getEmailClientes() {
        if(emailClientes == null){
            emailClientes = new JMenuItem("Email Clientes");
            emailClientes.setMnemonic('E');
            emailClientes.setToolTipText("Email Clientes");
            emailClientes.addActionListener(this);
        }
        return emailClientes;
    }

    public JMenuItem getTipoEmailClientes() {
        if(tipoEmailClientes == null){
            tipoEmailClientes = new JMenuItem("Tipo Email Clientes");
            tipoEmailClientes.setMnemonic('p');
            tipoEmailClientes.setToolTipText("Tipo Email Clientes");
            tipoEmailClientes.addActionListener(this);
        }
        return tipoEmailClientes;
    }
    
    // JMenuItems Proveedores
    public JMenuItem getProveedores() {
        if(proveedores == null){
            proveedores = new JMenuItem("Proveedores");
            proveedores.setMnemonic('r');
            proveedores.setToolTipText("Proveedores");
            proveedores.addActionListener(this);
        }        
        return proveedores;
    }

    public JMenuItem getDireccionProveedores() {
        if(direccionProveedores == null){
            direccionProveedores = new JMenuItem("Direccion Proveedores");
            direccionProveedores.setMnemonic('D');
            direccionProveedores.setToolTipText("Direccion Proveedores");
            direccionProveedores.addActionListener(this);
        }
        return direccionProveedores;
    }

    public JMenuItem getTipoDireccionProveedores() {
        if(tipoDireccionProveedores == null){
            tipoDireccionProveedores = new JMenuItem("Tipo Direccion Proveedores");
            tipoDireccionProveedores.setMnemonic('i');
            tipoDireccionProveedores.setToolTipText("Tipo Direccion Proveedores");
            tipoDireccionProveedores.addActionListener(this);
        }
        return tipoDireccionProveedores;
    }

    public JMenuItem getTelefonoProveedores() {
        if(telefonoProveedores == null){
            telefonoProveedores = new JMenuItem("Telefono Proveedores");
            telefonoProveedores.setMnemonic('T');
            telefonoProveedores.setToolTipText("Telefono Proveedores");
            telefonoProveedores.addActionListener(this);
        }
        return telefonoProveedores;
    }

    public JMenuItem getTipoTelefonoProveedores() {
        if(tipoTelefonoProveedores == null){
            tipoTelefonoProveedores = new JMenuItem("Tipo Telefono Proveedores");
            tipoTelefonoProveedores.setMnemonic('o');
            tipoTelefonoProveedores.setToolTipText("Tipo Telefono Proveedores");
            tipoTelefonoProveedores.addActionListener(this);
        }
        return tipoTelefonoProveedores;
    }

    public JMenuItem getEmailProveedores() {
        if(emailProveedores == null){
            emailProveedores = new JMenuItem("Email Proveedores");
            emailProveedores.setMnemonic('E');
            emailProveedores.setToolTipText("Email Proveedores");
            emailProveedores.addActionListener(this);
        }
        return emailProveedores;
    }

    public JMenuItem getTipoEmailProveedores() {
        if(tipoEmailProveedores == null){
            tipoEmailProveedores = new JMenuItem("Tipo Email Proveedores");
            tipoEmailProveedores.setMnemonic('p');
            tipoEmailProveedores.setToolTipText("Tipo Email Proveedores");
            tipoEmailProveedores.addActionListener(this);
        }
        return tipoEmailProveedores;
    }   
    
    // JMenuItems Producto
    public JMenuItem getProductos() {
        if(productos == null){
            productos = new JMenuItem("Productos");
            productos.setMnemonic('r');
            productos.setToolTipText("Productos");
            productos.addActionListener(this);
        }        
        return productos;
    }
    
    public JMenuItem getCategoria() {
        if(categoria == null){
            categoria = new JMenuItem("Categorias");
            categoria.setMnemonic('a');
            categoria.setToolTipText("Categorias");
            categoria.addActionListener(this);
        }
        return categoria;
    }

    public JMenuItem getStocks() {
        if(stocks == null){
            stocks = new JMenuItem("Stocks");
            stocks.setMnemonic('s');
            stocks.setToolTipText("Stocks");
            stocks.addActionListener(this);
        }
        return stocks;
    }

    public JMenuItem getEmpaques() {
        if(empaques == null){
            empaques = new JMenuItem("Empaques");
            empaques.setMnemonic('e');
            empaques.setToolTipText("Empaques");
            empaques.addActionListener(this);
        }
        return empaques;
    }

    // JMenuItems Compras Y Facturas

    public JMenuItem getFactura() {
        if(factura == null){
            factura = new JMenuItem("Factura");
            factura.setMnemonic('F');
            factura.setToolTipText("Factura");
            factura.addActionListener(this);
        }
        return factura;
    }

    public JMenuItem getCompra() {
        if(compra == null){
            compra = new JMenuItem("Compra");
            compra.setMnemonic('o');
            compra.setToolTipText("Compra");
            compra.addActionListener(this);
        }
        return compra;
    }

    public JMenuItem getDetalleFactura() {
        if(detalleFactura == null){
            detalleFactura = new JMenuItem("Detalle Factura");
            detalleFactura.setMnemonic('D');
            detalleFactura.setToolTipText("Detalle Factura");
            detalleFactura.addActionListener(this);
        }
        return detalleFactura;
    }

    public JMenuItem getDetalleCompra() {
        if(detalleCompra == null){
            detalleCompra = new JMenuItem("Detalle Compra");
            detalleCompra.setMnemonic('p');
            detalleCompra.setToolTipText("Detalle Compra");
            detalleCompra.addActionListener(this);
        }
        return detalleCompra;
    }

    public JMenuItem getInventarios() {
        if(inventarios == null){
            inventarios = new JMenuItem("Inventarios");
            inventarios.setMnemonic('I');
            inventarios.setToolTipText("Inventarios");
            inventarios.addActionListener(this);
        }
        return inventarios;
    }
            
    public JMenuItem getTiposDeMovimiento() {
        if(tiposDeMovimiento == null){
            tiposDeMovimiento = new JMenuItem("Tipos de Movimiento");
            tiposDeMovimiento.setMnemonic('T');
            tiposDeMovimiento.setToolTipText("Tipos de Movimiento");
            tiposDeMovimiento.addActionListener(this);
        }
        return tiposDeMovimiento;
    }
    
// Action event    
    public void actionPerformed(ActionEvent e){
       if(e.getSource() == clientes){
           new VentanaCatalogoCliente();
        }
       if(e.getSource() == direccionClientes){
           new VentanaCatalogoDireccionCliente();
        } 
       if(e.getSource() == tipoDireccionClientes){
           new VentanaCatalogoTipoDireccionCliente();
        } 
       if(e.getSource() == telefonoClientes){
           new VentanaCatalogoTelefonoCliente();
        } 
       if(e.getSource() == tipoTelefonoClientes){
           new VentanaCatalogoTipoTelefonoCliente();
        } 
       if(e.getSource() == emailClientes){
           new VentanaCatalogoEmailCliente();
        }
       if(e.getSource() == tipoEmailClientes){
           new VentanaCatalogoTipoEmailCliente();
        }
       if(e.getSource() == proveedores){
           new VentanaCatalogoProveedor();
        }
       if(e.getSource() == direccionProveedores){
           new VentanaCatalogoDireccionProveedor();
        } 
       if(e.getSource() == tipoDireccionProveedores){
           new VentanaCatalogoTipoDireccionProveedor();
        } 
       if(e.getSource() == telefonoProveedores){
           new VentanaCatalogoTelefonoProveedor();
        } 
       if(e.getSource() == tipoTelefonoProveedores){
           new VentanaCatalogoTipoTelefonoProveedor();
        } 
       if(e.getSource() == emailProveedores){
           new VentanaCatalogoEmailProveedor();
        }
       if(e.getSource() == tipoEmailProveedores){
           new VentanaCatalogoTipoEmailProveedor();
        }
       if(e.getSource() == productos){
           new VentanaCatalogoProducto();
        }
        if(e.getSource() == categoria){
           new VentanaCatalogoCategoria();
        } 
       if(e.getSource() == stocks){
           new VentanaCatalogoStock();
        } 
       if(e.getSource() == empaques){
           new VentanaCatalogoEmpaque();
        }
       if(e.getSource() == factura){
           new VentanaCatalogoFactura();
        }
       if(e.getSource() == compra){
           new VentanaCatalogoCompra();
        }
       if(e.getSource() == detalleFactura){
           new VentanaCatalogoDetalleFactura();
        }
        if(e.getSource() == detalleCompra){
           new VentanaCatalogoDetalleCompra();
        } 
       if(e.getSource() == inventarios){
           new VentanaCatalogoInventario();
        } 
       if(e.getSource() == tiposDeMovimiento){
           new VentanaCatalogoTipoMovimiento();
        } 
    }    
}
