package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Compra;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorCompra {
    
    private ArrayList<Compra> lista = new ArrayList<Compra>();

    public ManejadorCompra() {
        
    }

    public ArrayList<Compra> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_compra");
        try{
            while(datos.next()){
                lista.add( new Compra(datos.getInt("idCompra"),datos.getInt("numeroDeCompra"),datos.getDate("fechaCompra"),
                new Proveedor(datos.getInt("idProveedor"),datos.getString("nit"),datos.getString("nombre"),datos.getString("paginaWeb"),datos.getString("contacto")),
                datos.getString("descripcionCompra"),datos.getFloat("totalCompra")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    
  
    public void agregar(Compra compra){
       Map parametros = new HashMap();
       parametros.put("numeroDeCompra",compra.getNumeroDeCompra());
       parametros.put("fechaCompra",compra.getFechaCompra());
       parametros.put("idProveedor",compra.getProveedor().getIdProveedor());
       parametros.put("descripcionCompra",compra.getDescripcionCompra());
       parametros.put("totalCompra",compra.getTotalCompra());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_compraAgregar(?,?,?,?,?)}", parametros, false);
   }
   
   public void eliminar(Compra compra){
       Map parametros = new HashMap();
       parametros.put("idCompra",compra.getIdCompra());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_compraEliminar(?)}", parametros, false);
   }
   
   public void modificar(Compra compra){
       Map parametros = new HashMap();
       parametros.put("idCompra",compra.getIdCompra());
       parametros.put("numeroDeCompra",compra.getNumeroDeCompra());
       parametros.put("fechaCompra",compra.getFechaCompra());
       parametros.put("idProveedor",compra.getProveedor().getIdProveedor());
       parametros.put("descripcionCompra",compra.getDescripcionCompra());
       parametros.put("totalCompra",compra.getTotalCompra());      
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_compraModificar(?,?,?,?,?,?)}", parametros, false);
   }

}
    
