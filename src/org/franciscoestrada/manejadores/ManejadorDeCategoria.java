package org.franciscoestrada.manejadores;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Categoria;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorDeCategoria {
   
    private ArrayList<Categoria> lista = new ArrayList<Categoria>();   

    public ManejadorDeCategoria() {
        
    }
    
   public ArrayList<Categoria> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_categorias");
        try{
            while(datos.next()){
                lista.add( new Categoria(datos.getInt("idCategoria"),datos.getString("descripcionCategoria")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }
   
   public void agregar(Categoria categoria){
       Map parametros = new HashMap();
       parametros.put("descripcion",categoria.getDescripcionCategoria());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_CategoriaAgregar(?)}", parametros, false);
   }
   
   public void eliminar(Categoria categoria){
       Map parametros = new HashMap();
       parametros.put("idCategoria",categoria.getIdCategoria());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_CategoriaEliminar(?)}", parametros, false);
   }
   
   public void modificar(Categoria categoria){
       Map parametros = new HashMap();
       parametros.put("idCategoria",categoria.getIdCategoria());
       parametros.put("descripcion",categoria.getDescripcionCategoria());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_ModificarCategoria(?,?)}", parametros, false);
   }
    
}
