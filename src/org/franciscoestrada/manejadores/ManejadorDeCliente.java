package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorDeCliente {
    
    private ArrayList<Cliente> lista = new ArrayList<Cliente>();

    public ManejadorDeCliente() {
        
    }

    public ArrayList<Cliente> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("SELECT * FROM vw_clientes");
        try{
            while(datos.next()){
                lista.add( new Cliente(datos.getInt("idCliente"),datos.getString("nit"),datos.getString("dpi"),datos.getString("nombre")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    

    public void agregar(Cliente cliente){
       Map parametros = new HashMap();
       parametros.put("nit",cliente.getNit());
       parametros.put("dpi",cliente.getDpi());
       parametros.put("nombre",cliente.getNombre());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_clienteAgrega(?,?,?)}", parametros, false);
   }
   
   public void eliminar(Cliente cliente){
       Map parametros = new HashMap();
       parametros.put("idCliente",cliente.getIdCliente());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_clienteEliminar(?)}", parametros, false);
   }
   
   public void modificar(Cliente cliente){
       Map parametros = new HashMap();
       parametros.put("idCliente",cliente.getIdCliente());
       parametros.put("nit",cliente.getNit());
       parametros.put("dpi",cliente.getDpi());
       parametros.put("nombre",cliente.getNombre());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_ModificarCliente(?,?,?,?)}", parametros, false);
   }

}
