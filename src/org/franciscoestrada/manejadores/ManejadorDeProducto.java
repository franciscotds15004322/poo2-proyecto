package org.franciscoestrada.manejadores;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Producto;
import org.franciscoestrada.bean.Empaque;
import org.franciscoestrada.bean.Categoria;
import org.franciscoestrada.bean.Stock;
import org.franciscoestrada.db.ConexionDB;


public class ManejadorDeProducto {
    
    private ArrayList<Producto> lista = new ArrayList<Producto>();   

    public ManejadorDeProducto() {
        
    }

   public ArrayList<Producto> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_productos");
        try{
            while(datos.next()){
                lista.add( new Producto(datos.getInt("idProducto"),datos.getString("nombreProducto"),datos.getString("descripcionProducto"),datos.getFloat("precioUnitario"),datos.getFloat("precioPorDocena"),datos.getFloat("precioPorMayor"),
                new Empaque(datos.getInt("idEmpaque"),datos.getString("descripcionEmpaque")),
                new Categoria(datos.getInt("idCategoria"),datos.getString("descripcionCategoria")),
                new Stock(datos.getInt("idStock"),datos.getInt("stock"))));        
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    
    
    public void agregar(Producto producto){
       Map parametros = new HashMap();
       parametros.put("nombreProducto",producto.getNombreProducto());
       parametros.put("descripcionProducto",producto.getDescripcionProducto());
       parametros.put("precioUnitario",producto.getPrecioUnitario());
       parametros.put("precioPorDocena",producto.getPrecioPorDocena());
       parametros.put("precioPorMayor",producto.getPrecioPorMayor());
       parametros.put("idEmpaque",producto.getEmpaque().getIdEmpaque());
       parametros.put("idCategoria",producto.getCategoria().getIdCategoria());
       parametros.put("idStock",producto.getStock().getIdStock());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_productoAgregar(?,?,?,?,?,?,?,?)}", parametros, false);
   }
   
   public void eliminar(Producto producto){
       Map parametros = new HashMap();
       parametros.put("idProducto",producto.getIdProducto());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_productoEliminar(?)}", parametros, false);
   }
   
   public void modificar(Producto producto){
       Map parametros = new HashMap();
       parametros.put("idProducto",producto.getIdProducto());
       parametros.put("nombreProducto",producto.getNombreProducto());
       parametros.put("descripcionProducto",producto.getDescripcionProducto());
       parametros.put("precioUnitario",producto.getPrecioUnitario());
       parametros.put("precioPorDocena",producto.getPrecioPorDocena());
       parametros.put("precioPorMayor",producto.getPrecioPorMayor());
       parametros.put("idEmpaque",producto.getEmpaque().getIdEmpaque());
       parametros.put("idCategoria",producto.getCategoria().getIdCategoria());
       parametros.put("idStock",producto.getStock().getIdStock());       
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_modificarProducto(?,?,?,?,?,?,?,?,?)}", parametros, false);
   }

}
