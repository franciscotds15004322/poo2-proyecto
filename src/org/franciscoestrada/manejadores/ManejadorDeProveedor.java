package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorDeProveedor {
    
 private ArrayList<Proveedor> lista = new ArrayList<Proveedor>();

    public ManejadorDeProveedor() {
        
    }

    public ArrayList<Proveedor> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_proveedores");
        try{
            while(datos.next()){
                lista.add( new Proveedor(datos.getInt("idProveedor"),datos.getString("nit"),datos.getString("nombre"),datos.getString("paginaWeb"),datos.getString("contacto")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }
    
    public void agregar(Proveedor proveedor){
       Map parametros = new HashMap();
       parametros.put("nit",proveedor.getNit());
       parametros.put("nombre",proveedor.getNombre());
       parametros.put("paginaWeb",proveedor.getPaginaWeb());
       parametros.put("contacto",proveedor.getContacto());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_proveedorAgregar(?,?,?,?)}", parametros, false);
   }
   
   public void eliminar(Proveedor proveedor){
       Map parametros = new HashMap();
       parametros.put("idProveedor",proveedor.getIdProveedor());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_proveedorEliminar(?)}", parametros, false);
   }
   
   public void modificar(Proveedor proveedor){
       Map parametros = new HashMap();
       parametros.put("idProveedor",proveedor.getIdProveedor());
       parametros.put("nit",proveedor.getNit());
       parametros.put("nombre",proveedor.getNombre());
       parametros.put("paginaWeb",proveedor.getPaginaWeb());
       parametros.put("contacto",proveedor.getContacto());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_proveedorModificar(?,?,?,?,?)}", parametros, false);
   }

}