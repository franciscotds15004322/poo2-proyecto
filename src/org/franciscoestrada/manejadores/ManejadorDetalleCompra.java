package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Producto;
import org.franciscoestrada.bean.Compra;
import org.franciscoestrada.bean.DetalleCompra;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.bean.Empaque;
import org.franciscoestrada.bean.Stock;
import org.franciscoestrada.bean.Categoria;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorDetalleCompra {

    private ArrayList<DetalleCompra> lista = new ArrayList<DetalleCompra>();

    public ManejadorDetalleCompra() {
        
    }

    public ArrayList<DetalleCompra> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_detalleCompra ");
        try{
            while(datos.next()){
                lista.add( new DetalleCompra(datos.getInt("idDetalleCompra"),
                new Compra(datos.getInt("idCompra"),datos.getInt("numeroDeCompra"),datos.getDate("fechaCompra"),
                new Proveedor(datos.getInt("idProveedor"),datos.getString("nit"),datos.getString("nombre"),datos.getString("paginaWeb"),datos.getString("contacto")),
                datos.getString("descripcionCompra"),datos.getFloat("totalCompra")),
                datos.getInt("lineaNoCompra"),
                new Producto(datos.getInt("idProducto"),datos.getString("nombreProducto"),datos.getString("descripcionProducto"),datos.getFloat("precioUnitario"),datos.getFloat("precioPorDocena"),datos.getFloat("precioPorMayor"),
                new Empaque(datos.getInt("idEmpaque"),datos.getString("descripcionEmpaque")),
                new Categoria(datos.getInt("idCategoria"),datos.getString("descripcionCategoria")),
                new Stock(datos.getInt("idStock"),datos.getInt("stock"))),
                datos.getString("descripcionProductoDC"),datos.getInt("unidadesCompradas"),datos.getFloat("precioCompraUnidad"),datos.getFloat("totalLineaCompra")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    
    
    public void agregar(DetalleCompra detalleCompra){
       Map parametros = new HashMap();
       parametros.put("idCompra",detalleCompra.getCompra().getIdCompra());
       parametros.put("lineaNoCompra",detalleCompra.getLineaNoCompra());
       parametros.put("idProducto",detalleCompra.getProducto().getIdProducto());
       parametros.put("descripcionProductoDC",detalleCompra.getDescripcionProductoDC());
       parametros.put("unidadesCompradas",detalleCompra.getUnidadesCompradas());
       parametros.put("precioCompraUnidad",detalleCompra.getPrecioCompraUnidad());
       parametros.put("totalLineaCompra",detalleCompra.getTotalLineaCompra());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_detalleCompraAgregar(?,?,?,?,?,?,?)}", parametros, false);
   }
   
   public void eliminar(DetalleCompra detalleCompra){
       Map parametros = new HashMap();
       parametros.put("idDetalleCompra",detalleCompra.getIdDetalleCompra());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_detalleCompraEliminar(?)}", parametros, false);
   }
   
   public void modificar(DetalleCompra detalleCompra){
       Map parametros = new HashMap();
       parametros.put("idDetalleCompra",detalleCompra.getIdDetalleCompra());
       parametros.put("idCompra",detalleCompra.getCompra().getIdCompra());
       parametros.put("lineaNoCompra",detalleCompra.getLineaNoCompra());
       parametros.put("idProducto",detalleCompra.getProducto().getIdProducto());
       parametros.put("descripcionProductoDC",detalleCompra.getDescripcionProductoDC());
       parametros.put("unidadesCompradas",detalleCompra.getUnidadesCompradas());
       parametros.put("precioCompraUnidad",detalleCompra.getPrecioCompraUnidad());
       parametros.put("totalLineaCompra",detalleCompra.getTotalLineaCompra());     
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_detalleCompraModificar(?,?,?,?,?,?,?,?)}", parametros, false);
   }

}

