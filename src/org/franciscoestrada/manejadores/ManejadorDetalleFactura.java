package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Producto;
import org.franciscoestrada.bean.Factura;
import org.franciscoestrada.bean.DetalleFactura;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.bean.Empaque;
import org.franciscoestrada.bean.Stock;
import org.franciscoestrada.bean.Categoria;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorDetalleFactura {

    private ArrayList<DetalleFactura> lista = new ArrayList<DetalleFactura>();

    public ManejadorDetalleFactura() {
        
    }

    public ArrayList<DetalleFactura> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_detalleFactura");
        try{
            while(datos.next()){
                lista.add( new DetalleFactura(datos.getInt("idDetalle"),
                new Factura(datos.getInt("idFactura"),datos.getInt("numeroDeFactura"),datos.getDate("fecha"),
                new Cliente(datos.getInt("idCliente"),datos.getString("nit"),datos.getString("dpi"),datos.getString("nombre")),
                datos.getString("descripcion"),datos.getFloat("total")),
                datos.getInt("lineaNo"),
                new Producto(datos.getInt("idProducto"),datos.getString("nombreProducto"),datos.getString("descripcionProducto"),datos.getFloat("precioUnitario"),datos.getFloat("precioPorDocena"),datos.getFloat("precioPorMayor"),
                new Empaque(datos.getInt("idEmpaque"),datos.getString("descripcionEmpaque")),
                new Categoria(datos.getInt("idCategoria"),datos.getString("descripcionCategoria")),
                new Stock(datos.getInt("idStock"),datos.getInt("stock"))),
                datos.getString("descripcionDF"),datos.getInt("cantidad"),datos.getFloat("precio"),datos.getFloat("totalLinea")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }  
    
    public void agregar(DetalleFactura detalleFactura){
       Map parametros = new HashMap();
       parametros.put("idFactura",detalleFactura.getFactura().getIdFactura());
       parametros.put("lineaNo",detalleFactura.getLineaNo());
       parametros.put("idProducto",detalleFactura.getProducto().getIdProducto());
       parametros.put("descripcionDF",detalleFactura.getDescripcionDF());
       parametros.put("cantidad",detalleFactura.getCantidad());
       parametros.put("precio",detalleFactura.getPrecio());
       parametros.put("totalLinea",detalleFactura.getTotalLinea());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_detalleFacturaAgregar(?,?,?,?,?,?,?)}", parametros, false);
   }
   
   public void eliminar(DetalleFactura detalleFactura){
       Map parametros = new HashMap();
       parametros.put("idDetalle",detalleFactura.getIdDetalle());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_detalleFacturaEliminar(?)}", parametros, false);
   }
   
   public void modificar(DetalleFactura detalleFactura){
       Map parametros = new HashMap();
       parametros.put("idDetalle",detalleFactura.getIdDetalle());
       parametros.put("idFactura",detalleFactura.getFactura().getIdFactura());
       parametros.put("lineaNo",detalleFactura.getLineaNo());
       parametros.put("idProducto",detalleFactura.getProducto().getIdProducto());
       parametros.put("descripcionDF",detalleFactura.getDescripcionDF());
       parametros.put("cantidad",detalleFactura.getCantidad());
       parametros.put("precio",detalleFactura.getPrecio());
       parametros.put("totalLinea",detalleFactura.getTotalLinea());     
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_detalleFacturaModificar(?,?,?,?,?,?,?,?)}", parametros, false);
   }

}

