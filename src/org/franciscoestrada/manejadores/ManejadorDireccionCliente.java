package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.DireccionCliente;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.bean.TipoDireccionCliente;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorDireccionCliente {

    private ArrayList<DireccionCliente> lista = new ArrayList<DireccionCliente>();

    public ManejadorDireccionCliente() {
        
    }

    public ArrayList<DireccionCliente> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from sv_DireccionClientes ");
        try{
            while(datos.next()){
                lista.add( new DireccionCliente(datos.getInt("idDireccion"),
                new Cliente(datos.getInt("idCliente"),datos.getString("nit"),datos.getString("dpi"),datos.getString("nombre")),
                new TipoDireccionCliente(datos.getInt("idTipo"),datos.getString("descripcion")),datos.getString("direccion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    

    public void agregar(DireccionCliente direccionCliente){
       Map parametros = new HashMap();
       parametros.put("idCliente",direccionCliente.getCliente().getIdCliente());
       parametros.put("idTipo",direccionCliente.getTipoDireccionCliente().getIdTipo());
       parametros.put("direccion",direccionCliente.getDireccion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_direccionClienteAgregar(?,?,?)}", parametros, false);
   }
   
   public void eliminar(DireccionCliente direccionCliente){
       Map parametros = new HashMap();
       parametros.put("idDireccion",direccionCliente.getIdDireccion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_direccionClienteEliminar(?)}", parametros, false);
   }
   
   public void modificar(DireccionCliente direccionCliente){
       Map parametros = new HashMap();
       parametros.put("idDireccion",direccionCliente.getIdDireccion());
       parametros.put("idCliente",direccionCliente.getCliente().getIdCliente());
       parametros.put("idTipo",direccionCliente.getTipoDireccionCliente().getIdTipo());
       parametros.put("direccion",direccionCliente.getDireccion());      
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_direccionClienteModificar(?,?,?,?)}", parametros, false);
   }
}    
