package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.DireccionProveedor;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.bean.TipoDireccionProveedor;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorDireccionProveedor {
    
    private ArrayList<DireccionProveedor> lista = new ArrayList<DireccionProveedor>();

    public ManejadorDireccionProveedor() {
        
    }

    public ArrayList<DireccionProveedor> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from sv_DireccionProveedor");
        try{
            while(datos.next()){
                lista.add( new DireccionProveedor(datos.getInt("idDireccion"),
                new Proveedor(datos.getInt("idProveedor"),datos.getString("nit"),datos.getString("nombre"),datos.getString("paginaWeb"),datos.getString("contacto")),
                new TipoDireccionProveedor(datos.getInt("idTipo"),datos.getString("descripcion")),datos.getString("direccion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    

    public void agregar(DireccionProveedor direccionProveedor){
       Map parametros = new HashMap();
       parametros.put("idProveedor",direccionProveedor.getProveedor().getIdProveedor());
       parametros.put("idTipo",direccionProveedor.getTipoDireccionProveedor().getIdTipo());
       parametros.put("direccion",direccionProveedor.getDireccion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_direccionProveedorAgregar(?,?,?)}", parametros, false);
   }
   
   public void eliminar(DireccionProveedor direccionProveedor){
       Map parametros = new HashMap();
       parametros.put("idDireccion",direccionProveedor.getIdDireccion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_direccionProveedorEliminar(?)}", parametros, false);
   }
   
   public void modificar(DireccionProveedor direccionProveedor){
       Map parametros = new HashMap();
       parametros.put("idDireccion",direccionProveedor.getIdDireccion());
       parametros.put("idProveedor",direccionProveedor.getProveedor().getIdProveedor());
       parametros.put("idTipo",direccionProveedor.getTipoDireccionProveedor().getIdTipo());
       parametros.put("direccion",direccionProveedor.getDireccion());     
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_direccionProveedorModificar(?,?,?,?)}", parametros, false);
   }
}    

