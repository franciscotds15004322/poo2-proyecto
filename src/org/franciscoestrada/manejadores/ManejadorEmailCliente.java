package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.EmailCliente;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.bean.TipoEmailCliente;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorEmailCliente {

    private ArrayList<EmailCliente> lista = new ArrayList<EmailCliente>();

    public ManejadorEmailCliente() {
        
    }

    public ArrayList<EmailCliente> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_EmailClientes");
        try{
            while(datos.next()){
                lista.add( new EmailCliente(datos.getInt("idEmail"),
                new Cliente(datos.getInt("idCliente"),datos.getString("nit"),datos.getString("dpi"),datos.getString("nombre")),
                new TipoEmailCliente(datos.getInt("idTipo"),datos.getString("descripcion")),datos.getString("email")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    

    public void agregar(EmailCliente emailCliente){
       Map parametros = new HashMap();
       parametros.put("idCliente",emailCliente.getCliente().getIdCliente());
       parametros.put("idTipo",emailCliente.getTipoEmailCliente().getIdTipo());
       parametros.put("email",emailCliente.getEmail());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_emailClienteAgregar(?,?,?)}", parametros, false);
   }
   
   public void eliminar(EmailCliente emailCliente){
       Map parametros = new HashMap();
       parametros.put("idEmail",emailCliente.getIdEmail());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_emailClienteEliminar(?)}", parametros, false);
   }
   
   public void modificar(EmailCliente emailCliente){
       Map parametros = new HashMap();
       parametros.put("idEmail",emailCliente.getIdEmail());
       parametros.put("idCliente",emailCliente.getCliente().getIdCliente());
       parametros.put("idTipo",emailCliente.getTipoEmailCliente().getIdTipo());
       parametros.put("email",emailCliente.getEmail());       
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_emailClienteModificar(?,?,?,?)}", parametros, false);
   }
}      


