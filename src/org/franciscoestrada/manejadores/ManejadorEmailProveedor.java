package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.EmailProveedor;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.bean.TipoEmailProveedor;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorEmailProveedor {
    
    private ArrayList<EmailProveedor> lista = new ArrayList<EmailProveedor>();

    public ManejadorEmailProveedor() {
        
    }

    public ArrayList<EmailProveedor> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_EmailProveedor");
        try{
            while(datos.next()){
                lista.add( new EmailProveedor(datos.getInt("idEmail"),
                new Proveedor(datos.getInt("idProveedor"),datos.getString("nit"),datos.getString("nombre"),datos.getString("paginaWeb"),datos.getString("contacto")),
                new TipoEmailProveedor(datos.getInt("idTipo"),datos.getString("descripcion")),datos.getString("email")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    

    public void agregar(EmailProveedor emailProveedor){
       Map parametros = new HashMap();
       parametros.put("idProveedor",emailProveedor.getProveedor().getIdProveedor());
       parametros.put("idTipo",emailProveedor.getTipoEmailProveedor().getIdTipo());
       parametros.put("email",emailProveedor.getEmail());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_emailProveedorAgregar(?,?,?)}", parametros, false);
   }
   
   public void eliminar(EmailProveedor emailProveedor){
       Map parametros = new HashMap();
       parametros.put("idEmail",emailProveedor.getIdEmail());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_emailProveedorEliminar(?)}", parametros, false);
   }
   
   public void modificar(EmailProveedor emailProveedor){
       Map parametros = new HashMap();
       parametros.put("idEmail",emailProveedor.getIdEmail());
       parametros.put("idProveedor",emailProveedor.getProveedor().getIdProveedor());
       parametros.put("idTipo",emailProveedor.getTipoEmailProveedor().getIdTipo());
       parametros.put("email",emailProveedor.getEmail());       
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_emailProveedorModificar(?,?,?,?)}", parametros, false);
   }
}      


