package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Empaque;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorEmpaque {
    
    private ArrayList<Empaque> lista = new ArrayList<Empaque>();

    public ManejadorEmpaque() {
        
    }

    public ArrayList<Empaque> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_Empaque");
        try{
            while(datos.next()){
                lista.add( new Empaque(datos.getInt("idEmpaque"),datos.getString("descripcionEmpaque")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    
    
    public void agregar(Empaque empaque){
       Map parametros = new HashMap();
       parametros.put("descripcionEmpaque",empaque.getDescripcionEmpaque());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_empaqueAgregar(?)}", parametros, false);
   }
   
   public void eliminar(Empaque empaque){
       Map parametros = new HashMap();
       parametros.put("idEmpaque",empaque.getIdEmpaque());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_empaqueEliminar(?)}", parametros, false);
   }
   
   public void modificar(Empaque empaque){
       Map parametros = new HashMap();
       parametros.put("idEmpaque",empaque.getIdEmpaque());
       parametros.put("descripcionEmpaque",empaque.getDescripcionEmpaque());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_empaqueModificar(?,?)}", parametros, false);
   }

} 
    


