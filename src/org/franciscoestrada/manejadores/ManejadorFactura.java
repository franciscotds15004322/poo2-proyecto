package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Factura;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorFactura {
 
    private ArrayList<Factura> lista = new ArrayList<Factura>();

    public ManejadorFactura() {
        
    }

    public ArrayList<Factura> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_factura");
        try{
            while(datos.next()){
                lista.add( new Factura(datos.getInt("idFactura"),datos.getInt("numeroDeFactura"),datos.getDate("fecha"),
                new Cliente(datos.getInt("idCliente"),datos.getString("nit"),datos.getString("dpi"),datos.getString("nombre")),
                datos.getString("descripcion"),datos.getFloat("total")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    
    
    public void agregar(Factura factura){
       Map parametros = new HashMap();
       parametros.put("numeroDeFactura",factura.getNumeroDeFactura());
       parametros.put("fecha",factura.getFecha());
       parametros.put("idCliente",factura.getCliente().getIdCliente());
       parametros.put("descripcion",factura.getDescripcion());
       parametros.put("total",factura.getTotal());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_facturaAgregar(?,?,?,?,?)}", parametros, false);
   }
   
   public void eliminar(Factura factura){
       Map parametros = new HashMap();
       parametros.put("idFactura",factura.getIdFactura());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_facturaEliminar(?)}", parametros, false);
   }
   
   public void modificar(Factura factura){
       Map parametros = new HashMap();
       parametros.put("idFactura",factura.getIdFactura());
       parametros.put("numeroDeFactura",factura.getNumeroDeFactura());
       parametros.put("fecha",factura.getFecha());
       parametros.put("idCliente",factura.getCliente().getIdCliente());
       parametros.put("descripcion",factura.getDescripcion());
       parametros.put("total",factura.getTotal());       
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_FacturaModificar(?,?,?,?,?,?)}", parametros, false);
   }

}

