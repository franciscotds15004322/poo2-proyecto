package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Inventario;
import org.franciscoestrada.bean.TipoMovimiento;
import org.franciscoestrada.bean.Producto;
import org.franciscoestrada.bean.Empaque;
import org.franciscoestrada.bean.Categoria;
import org.franciscoestrada.bean.Stock;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorInventario {
    
    private ArrayList<Inventario> lista = new ArrayList<Inventario>();

    public ManejadorInventario() {
        
    }

    public ArrayList<Inventario> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_inventario");
        try{
            while(datos.next()){
                lista.add( new Inventario(datos.getInt("idMovimiento"),datos.getDate("fechaInventario"),
                new Producto(datos.getInt("idProducto"),datos.getString("nombreProducto"),datos.getString("descripcionProducto"),datos.getFloat("precioUnitario"),datos.getFloat("precioPorDocena"),datos.getFloat("precioPorMayor"),
                new Empaque(datos.getInt("idEmpaque"),datos.getString("descripcionEmpaque")),
                new Categoria(datos.getInt("idCategoria"),datos.getString("descripcionCategoria")),
                new Stock(datos.getInt("idStock"),datos.getInt("stock"))),
                new TipoMovimiento(datos.getInt("idTipoMovimiento"),datos.getString("descripcion")),                      
                datos.getInt("IdFactura"),datos.getInt("IdCompra"),datos.getInt("lineaNoInventario"),datos.getInt("cantidad"),datos.getFloat("precioCompraUnitario")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    

    public void agregar(Inventario inventario){
       Map parametros = new HashMap();
       parametros.put("fechaInventario",inventario.getFechaInventario());
       parametros.put("idProducto",inventario.getProducto().getIdProducto());
       parametros.put("idTipoMovimiento",inventario.getTipoMovimiento().getIdTipoMovimiento());
       parametros.put("idFactura",inventario.getIdFactura());
       parametros.put("idCompra",inventario.getIdCompra());
       parametros.put("lineaNoInventario",inventario.getLineaNoInventario());
       parametros.put("cantidad",inventario.getCantidad());
       parametros.put("precioCompraUnitario",inventario.getPrecioCompraUnitario());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_inventarioAgregar(?,?,?,?,?,?,?,?)}", parametros, false);
   }
   
   public void eliminar(Inventario inventario){
       Map parametros = new HashMap();
       parametros.put("idMovimiento",inventario.getIdMovimiento());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_inventarioEliminar(?)}", parametros, false);
   }
   
   public void modificar(Inventario inventario){
       Map parametros = new HashMap();
       parametros.put("idMovimiento",inventario.getIdMovimiento());
       parametros.put("fechaInventario",inventario.getFechaInventario());
       parametros.put("idProducto",inventario.getProducto().getIdProducto());
       parametros.put("idTipoMovimiento",inventario.getTipoMovimiento().getIdTipoMovimiento());
       parametros.put("idFactura",inventario.getIdFactura());
       parametros.put("idCompra",inventario.getIdCompra());
       parametros.put("lineaNoInventario",inventario.getLineaNoInventario());
       parametros.put("cantidad",inventario.getCantidad());
       parametros.put("precioCompraUnitario",inventario.getPrecioCompraUnitario());      
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_inventarioProducto(?,?,?,?,?,?,?,?,?)}", parametros, false);
   }

}    
