package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.Stock;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorStock {
    
    private ArrayList<Stock> lista = new ArrayList<Stock>();

    public ManejadorStock() {
        
    }

    public ArrayList<Stock> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_stock");
        try{
            while(datos.next()){
                lista.add( new Stock(datos.getInt("idStock"),datos.getInt("stock")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    
    
    public void agregar(Stock stock){
       Map parametros = new HashMap();
       parametros.put("stock",stock.getStock());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_stockAgregar(?)}", parametros, false);
   }
   
   public void eliminar(Stock stock){
       Map parametros = new HashMap();
       parametros.put("idStock",stock.getIdStock());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_stockEliminar(?)}", parametros, false);
   }
   
   public void modificar(Stock stock){
       Map parametros = new HashMap();
       parametros.put("idStock",stock.getIdStock());
       parametros.put("stock",stock.getStock());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_stockModificar(?,?)}", parametros, false);
   }

} 
    

