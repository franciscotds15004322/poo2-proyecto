package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TelefonoCliente;
import org.franciscoestrada.bean.Cliente;
import org.franciscoestrada.bean.TipoTelefonoCliente;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorTelefonoCliente {

    private ArrayList<TelefonoCliente> lista = new ArrayList<TelefonoCliente>();

    public ManejadorTelefonoCliente() {
        
    }

    public ArrayList<TelefonoCliente> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_TelefonoCliente");
        try{
            while(datos.next()){
                lista.add( new TelefonoCliente(datos.getInt("idTelefono"),
                new Cliente(datos.getInt("idCliente"),datos.getString("nit"),datos.getString("dpi"),datos.getString("nombre")),
                new TipoTelefonoCliente(datos.getInt("idTipo"),datos.getString("descripcion")),datos.getString("telefono")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    
    
    public void agregar(TelefonoCliente telefonoCliente){
       Map parametros = new HashMap();
       parametros.put("idCliente",telefonoCliente.getCliente().getIdCliente());
       parametros.put("idTipo",telefonoCliente.getTipoTelefonoCliente().getIdTipo());
       parametros.put("telefono",telefonoCliente.getTelefono());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_telefonoClienteAgregar(?,?,?)}", parametros, false);
   }
   
   public void eliminar(TelefonoCliente telefonoCliente){
       Map parametros = new HashMap();
       parametros.put("idTelefono",telefonoCliente.getIdTelefono());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_telefonoClienteEliminar(?)}", parametros, false);
   }
   
   public void modificar(TelefonoCliente telefonoCliente){
       Map parametros = new HashMap();
       parametros.put("idTelefono",telefonoCliente.getIdTelefono());
       parametros.put("idCliente",telefonoCliente.getCliente().getIdCliente());
       parametros.put("idTipo",telefonoCliente.getTipoTelefonoCliente().getIdTipo());
       parametros.put("telefono",telefonoCliente.getTelefono());       
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_telefonoClienteModificar(?,?,?,?)}", parametros, false);
   }
}     



