package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TelefonoProveedor;
import org.franciscoestrada.bean.Proveedor;
import org.franciscoestrada.bean.TipoTelefonoProveedor;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorTelefonoProveedor {   
 
    private ArrayList<TelefonoProveedor> lista = new ArrayList<TelefonoProveedor>();

    public ManejadorTelefonoProveedor() {
        
    }

    public ArrayList<TelefonoProveedor> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_TelefonoProveedor");
        try{
            while(datos.next()){
                lista.add( new TelefonoProveedor(datos.getInt("idTelefono"),
                new Proveedor(datos.getInt("idProveedor"),datos.getString("nit"),datos.getString("nombre"),datos.getString("paginaWeb"),datos.getString("contacto")),
                new TipoTelefonoProveedor(datos.getInt("idTipo"),datos.getString("descripcion")),datos.getString("telefono")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }
    
    public void agregar(TelefonoProveedor telefonoProveedor){
       Map parametros = new HashMap();
       parametros.put("idProveedor",telefonoProveedor.getProveedor().getIdProveedor());
       parametros.put("idTipo",telefonoProveedor.getTipoTelefonoProveedor().getIdTipo());
       parametros.put("telefono",telefonoProveedor.getTelefono());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_telefonoProveedorAgregar(?,?,?)}", parametros, false);
   }
   
   public void eliminar(TelefonoProveedor telefonoProveedor){
       Map parametros = new HashMap();
       parametros.put("idTelefono",telefonoProveedor.getIdTelefono());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_telefonoProveedorEliminar(?)}", parametros, false);
   }
   
   public void modificar(TelefonoProveedor telefonoProveedor){
       Map parametros = new HashMap();
       parametros.put("idTelefono",telefonoProveedor.getIdTelefono());
       parametros.put("idProveedor",telefonoProveedor.getProveedor().getIdProveedor());
       parametros.put("idTipo",telefonoProveedor.getTipoTelefonoProveedor().getIdTipo());
       parametros.put("telefono",telefonoProveedor.getTelefono());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_telefonoProveedorModificar(?,?,?,?)}", parametros, false);
   }
}    

