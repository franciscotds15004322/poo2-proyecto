package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TipoDireccionCliente;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorTipoDireccionCliente {

    private ArrayList<TipoDireccionCliente> lista = new ArrayList<TipoDireccionCliente>();

    public ManejadorTipoDireccionCliente() {
        
    }

    public ArrayList<TipoDireccionCliente> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_tipoDireccionCliente");
        try{
            while(datos.next()){
                lista.add( new TipoDireccionCliente(datos.getInt("idTipo"),datos.getString("descripcion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    
    
    public void agregar(TipoDireccionCliente tipoDireccionCliente){
       Map parametros = new HashMap();
       parametros.put("descripcion",tipoDireccionCliente.getDescripcion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoDireccionClienteAgregar(?)}", parametros, false);
   }
   
   public void eliminar(TipoDireccionCliente tipoDireccionCliente){
       Map parametros = new HashMap();
       parametros.put("idTipo",tipoDireccionCliente.getIdTipo());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoDireccionClienteEliminar(?)}", parametros, false);
   }
   
   public void modificar(TipoDireccionCliente tipoDireccionCliente){
       Map parametros = new HashMap();
       parametros.put("idTipo",tipoDireccionCliente.getIdTipo());
       parametros.put("descripcion",tipoDireccionCliente.getDescripcion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoDireccionClienteModificar(?,?)}", parametros, false);
   }

}
