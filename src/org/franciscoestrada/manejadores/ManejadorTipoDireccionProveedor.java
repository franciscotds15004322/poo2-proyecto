package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TipoDireccionProveedor;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorTipoDireccionProveedor {
    
    private ArrayList<TipoDireccionProveedor> lista = new ArrayList<TipoDireccionProveedor>();

    public ManejadorTipoDireccionProveedor() {
        
    }

    public ArrayList<TipoDireccionProveedor> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_tipoDireccionProveedor");
        try{
            while(datos.next()){
                lista.add( new TipoDireccionProveedor(datos.getInt("idTipo"),datos.getString("descripcion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    
    
    public void agregar(TipoDireccionProveedor tipoDireccionProveedor){
       Map parametros = new HashMap();
       parametros.put("descripcion",tipoDireccionProveedor.getDescripcion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoDireccionProveedorAgregar(?)}", parametros, false);
   }
   
   public void eliminar(TipoDireccionProveedor tipoDireccionProveedor){
       Map parametros = new HashMap();
       parametros.put("idTipo",tipoDireccionProveedor.getIdTipo());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoDireccionProveedorEliminar(?)}", parametros, false);
   }
   
   public void modificar(TipoDireccionProveedor tipoDireccionProveedor){
       Map parametros = new HashMap();
       parametros.put("idTipo",tipoDireccionProveedor.getIdTipo());
       parametros.put("descripcion",tipoDireccionProveedor.getDescripcion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoDireccionProveedorModificar(?,?)}", parametros, false);
   }

}