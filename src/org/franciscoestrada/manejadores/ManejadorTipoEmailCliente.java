package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TipoEmailCliente;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorTipoEmailCliente {
    
    private ArrayList<TipoEmailCliente> lista = new ArrayList<TipoEmailCliente>();

    public ManejadorTipoEmailCliente() {
        
    }

    public ArrayList<TipoEmailCliente> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_tipoEmailClientes");
        try{
            while(datos.next()){
                lista.add( new TipoEmailCliente(datos.getInt("idTipo"),datos.getString("descripcion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    
    
    public void agregar(TipoEmailCliente tipoEmailCliente){
       Map parametros = new HashMap();
       parametros.put("descripcion",tipoEmailCliente.getDescripcion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoEmailClienteAgregar(?)}", parametros, false);
   }
   
   public void eliminar(TipoEmailCliente tipoEmailCliente){
       Map parametros = new HashMap();
       parametros.put("idTipo",tipoEmailCliente.getIdTipo());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoEmailClienteEliminar(?)}", parametros, false);
   }
   
   public void modificar(TipoEmailCliente tipoEmailCliente){
       Map parametros = new HashMap();
       parametros.put("idTipo",tipoEmailCliente.getIdTipo());
       parametros.put("descripcion",tipoEmailCliente.getDescripcion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoEmailClienteModificar(?,?)}", parametros, false);
   }

}
