package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TipoEmailProveedor;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorTipoEmailProveedor {    
    
    private ArrayList<TipoEmailProveedor> lista = new ArrayList<TipoEmailProveedor>();

    public ManejadorTipoEmailProveedor() {
        
    }

    public ArrayList<TipoEmailProveedor> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_tipoEmailProveedor");
        try{
            while(datos.next()){
                lista.add( new TipoEmailProveedor(datos.getInt("idTipo"),datos.getString("descripcion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    
    
    public void agregar(TipoEmailProveedor tipoEmailProveedor){
       Map parametros = new HashMap();
       parametros.put("descripcion",tipoEmailProveedor.getDescripcion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoEmailProveedorAgregar(?)}", parametros, false);
   }
   
   public void eliminar(TipoEmailProveedor tipoEmailProveedor){
       Map parametros = new HashMap();
       parametros.put("idTipo",tipoEmailProveedor.getIdTipo());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoEmailProveedorEliminar(?)}", parametros, false);
   }
   
   public void modificar(TipoEmailProveedor tipoEmailProveedor){
       Map parametros = new HashMap();
       parametros.put("idTipo",tipoEmailProveedor.getIdTipo());
       parametros.put("descripcion",tipoEmailProveedor.getDescripcion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoEmailProveedorModificar(?,?)}", parametros, false);
   }

} 

