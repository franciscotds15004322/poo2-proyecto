package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TipoMovimiento;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorTipoMovimiento {
    
    private ArrayList<TipoMovimiento> lista = new ArrayList<TipoMovimiento>();

    public ManejadorTipoMovimiento() {
        
    }

    public ArrayList<TipoMovimiento> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_tipoMovimiento");
        try{
            while(datos.next()){
                lista.add( new TipoMovimiento(datos.getInt("idTipoMovimiento"),datos.getString("descripcion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    
    
    public void agregar(TipoMovimiento tipoMovimiento){
       Map parametros = new HashMap();
       parametros.put("descripcion",tipoMovimiento.getDescripcion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_TipoMovimientoAgregar(?)}", parametros, false);
   }
   
   public void eliminar(TipoMovimiento tipoMovimiento){
       Map parametros = new HashMap();
       parametros.put("idTipoMovimiento",tipoMovimiento.getIdTipoMovimiento());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_TipoMovimientoEliminar(?)}", parametros, false);
   }
   
   public void modificar(TipoMovimiento tipoMovimiento){
       Map parametros = new HashMap();
       parametros.put("idTipoMovimiento",tipoMovimiento.getIdTipoMovimiento());
       parametros.put("descripcion",tipoMovimiento.getDescripcion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_TipoMovimientoModificar(?,?)}", parametros, false);
   }

} 