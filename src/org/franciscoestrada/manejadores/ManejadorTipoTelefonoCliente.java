package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TipoTelefonoCliente;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorTipoTelefonoCliente {

    private ArrayList<TipoTelefonoCliente> lista = new ArrayList<TipoTelefonoCliente>();

    public ManejadorTipoTelefonoCliente() {
        
    }

    public ArrayList<TipoTelefonoCliente> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_tipoTelefonoClientes");
        try{
            while(datos.next()){
                lista.add( new TipoTelefonoCliente(datos.getInt("idTipo"),datos.getString("descripcion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    
    
    public void agregar(TipoTelefonoCliente tipoTelefonoCliente){
       Map parametros = new HashMap();
       parametros.put("descripcion",tipoTelefonoCliente.getDescripcion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoTelefonoClienteAgregar(?)}", parametros, false);
   }
   
   public void eliminar(TipoTelefonoCliente tipoTelefonoCliente){
       Map parametros = new HashMap();
       parametros.put("idTipo",tipoTelefonoCliente.getIdTipo());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoTelefonoClienteEliminar(?)}", parametros, false);
   }
   
   public void modificar(TipoTelefonoCliente tipoTelefonoCliente){
       Map parametros = new HashMap();
       parametros.put("idTipo",tipoTelefonoCliente.getIdTipo());
       parametros.put("descripcion",tipoTelefonoCliente.getDescripcion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoTelefonoClienteModificar(?,?)}", parametros, false);
   }

}

