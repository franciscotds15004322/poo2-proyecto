package org.franciscoestrada.manejadores;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.franciscoestrada.bean.TipoTelefonoProveedor;
import org.franciscoestrada.db.ConexionDB;

public class ManejadorTipoTelefonoProveedor {

    private ArrayList<TipoTelefonoProveedor> lista = new ArrayList<TipoTelefonoProveedor>();

    public ManejadorTipoTelefonoProveedor() {
        
    }

    public ArrayList<TipoTelefonoProveedor> getLista() {
        ResultSet datos = ConexionDB.getInstancia().hacerConsulta("select * from vw_tipoTelefonoProveedor");
        try{
            while(datos.next()){
                lista.add( new TipoTelefonoProveedor(datos.getInt("idTipo"),datos.getString("descripcion")));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }            
        return lista;
    }    
    
    public void agregar(TipoTelefonoProveedor tipoTelefonoProveedor){
       Map parametros = new HashMap();
       parametros.put("descripcion",tipoTelefonoProveedor.getDescripcion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoTelefonoProveedorAgregar(?)}", parametros, false);
   }
   
   public void eliminar(TipoTelefonoProveedor tipoTelefonoProveedor){
       Map parametros = new HashMap();
       parametros.put("idTipo",tipoTelefonoProveedor.getIdTipo());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoTelefonoProveedorEliminar(?)}", parametros, false);
   }
   
   public void modificar(TipoTelefonoProveedor tipoTelefonoProveedor){
       Map parametros = new HashMap();
       parametros.put("idTipo",tipoTelefonoProveedor.getIdTipo());
       parametros.put("descripcion",tipoTelefonoProveedor.getDescripcion());
       ConexionDB.getInstancia().ejecutarProcedimiento("{call sp_tipoTelefonoProveedorModificar(?,?)}", parametros, false);
   }

} 
