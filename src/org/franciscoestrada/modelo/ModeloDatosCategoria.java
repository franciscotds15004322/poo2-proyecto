package org.franciscoestrada.modelo;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorDeCategoria;
import org.franciscoestrada.bean.Categoria;

public class ModeloDatosCategoria extends AbstractTableModel{
    
    private String[] encabezados = {"ID.CATEGORIA","CATEGORIA"};
    private ArrayList<Categoria> listaCategoria = null;
    private ManejadorDeCategoria manejador = new ManejadorDeCategoria();

    public ModeloDatosCategoria() {
        listaCategoria = manejador.getLista();
    }
  
   public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaCategoria.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        Categoria elemento = listaCategoria.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getIdCategoria());
            break;
            case 1:
                resultado= elemento.getDescripcionCategoria();
            break;
        }
        return resultado;
    }
    
    public Categoria getElemento(int fila){
        return listaCategoria.get(fila);
    }
    
    //Agregar
    public void agregar(Categoria categoria){
        manejador.agregar(categoria);
        listaCategoria.removeAll(listaCategoria);
        listaCategoria = manejador.getLista();
        fireTableDataChanged();// MOdelo cambios y refrescar
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaCategoria.get(fila));
        listaCategoria.removeAll(listaCategoria);
        listaCategoria = manejador.getLista();
        fireTableDataChanged();// MOdelo cambios y refrescar
    }
    // Modificar
    public void modificar(Categoria categoria){
        manejador.modificar(categoria);
        listaCategoria.removeAll(listaCategoria);
        listaCategoria = manejador.getLista();
        fireTableDataChanged();// MOdelo cambios y refrescar
    }
}