package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorDeCliente;
import org.franciscoestrada.bean.Cliente;

public class ModeloDatosCliente extends AbstractTableModel{
    
    private String[] encabezados = {"ID CLIENTE","NIT","DPI","NOMBRE"};
    private ArrayList<Cliente> listaCliente = null;
    private ManejadorDeCliente manejador = new ManejadorDeCliente();

    public ModeloDatosCliente() {
        listaCliente = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaCliente.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        Cliente elemento = listaCliente.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getIdCliente()); 
                break;
            case 1:
                resultado= elemento.getNit(); //String.valueOf(elemento.getNit()); si fuera string
                break;
            case 2:
                resultado= elemento.getDpi();
                break;
            case 3:
                resultado= elemento.getNombre();
                break;
        }
        return resultado;
    }

    public Cliente getElemento(int fila){
        return listaCliente.get(fila);
    }
    
    //Agregar
    public void agregar(Cliente cliente){
        manejador.agregar(cliente);
        listaCliente.removeAll(listaCliente);
        listaCliente = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaCliente.get(fila));
        listaCliente.removeAll(listaCliente);
        listaCliente = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(Cliente cliente){
        manejador.modificar(cliente);
        listaCliente.removeAll(listaCliente);
        listaCliente = manejador.getLista();
        fireTableDataChanged();
    }
    
}
