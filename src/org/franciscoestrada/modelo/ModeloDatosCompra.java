package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorCompra;
import org.franciscoestrada.bean.Compra;

public class ModeloDatosCompra extends AbstractTableModel{
    private String[] encabezados = {"NUMERO DE COMRA","FECHA","NIT","NOMBRE","CONTACTO","DESCRIPCION","TOTAL"};
    private ArrayList<Compra> listaCompra = null;
    private ManejadorCompra manejador = new ManejadorCompra();

    public ModeloDatosCompra() {
        listaCompra = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaCompra.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        Compra elemento = listaCompra.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getNumeroDeCompra()); 
                break;
            case 1:
                resultado= String.valueOf(elemento.getFechaCompra()); 
                break;                
            case 2:
                resultado= String.valueOf(elemento.getProveedor().getNit()); 
                break;
            case 3:
                resultado= elemento.getProveedor().getNombre(); 
                break;
            case 4:
                resultado= elemento.getProveedor().getContacto();
                break;
            case 5:
                resultado= elemento.getDescripcionCompra();
                break;
            case 6:
                resultado= String.valueOf(elemento.getTotalCompra());
            break;
        }
        return resultado;
    }
           
    public Compra getElemento(int fila){
        return listaCompra.get(fila);
    }
    
    //Agregar
    public void agregar(Compra compra){
        manejador.agregar(compra);
        listaCompra.removeAll(listaCompra);
        listaCompra = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaCompra.get(fila));
        listaCompra.removeAll(listaCompra);
        listaCompra = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(Compra compra){
        manejador.modificar(compra);
        listaCompra.removeAll(listaCompra);
        listaCompra = manejador.getLista();
        fireTableDataChanged();
    }
    
}

