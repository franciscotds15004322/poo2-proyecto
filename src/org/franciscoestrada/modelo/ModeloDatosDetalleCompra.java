package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorDetalleCompra;
import org.franciscoestrada.bean.DetalleCompra;

public class ModeloDatosDetalleCompra extends AbstractTableModel{
    private String[] encabezados = {"ID DETALLE","NO. COMPRA","LINEA NO","PRODUCTO","DESCRIPCION","CANTIDAD","PRECIO","TOTAL LINEA"};
    private ArrayList<DetalleCompra> listaDetalleCompra = null;
    private ManejadorDetalleCompra manejador = new ManejadorDetalleCompra();

    public ModeloDatosDetalleCompra() {
        listaDetalleCompra = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaDetalleCompra.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        DetalleCompra elemento = listaDetalleCompra.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getIdDetalleCompra()); 
                break;
            case 1:
                resultado= String.valueOf(elemento.getCompra().getNumeroDeCompra()); 
                break;
            case 2:
                resultado= String.valueOf(elemento.getLineaNoCompra());
                break;
            case 3:
                resultado= elemento.getProducto().getNombreProducto();
                break;
            case 4:
                resultado= elemento.getDescripcionProductoDC();
            break;
            case 5:
                resultado= String.valueOf(elemento.getUnidadesCompradas()); 
                break;
            case 6:
                resultado= String.valueOf(elemento.getPrecioCompraUnidad());
                break;
            case 7:
                resultado= String.valueOf(elemento.getTotalLineaCompra());
                break;                
        }
        return resultado;
    }
      
    public DetalleCompra getElemento(int fila){
        return listaDetalleCompra.get(fila);
    }
    
    //Agregar
    public void agregar(DetalleCompra detalleCompra){
        manejador.agregar(detalleCompra);
        listaDetalleCompra.removeAll(listaDetalleCompra);
        listaDetalleCompra = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaDetalleCompra.get(fila));
        listaDetalleCompra.removeAll(listaDetalleCompra);
        listaDetalleCompra = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(DetalleCompra detalleCompra){
        manejador.modificar(detalleCompra);
        listaDetalleCompra.removeAll(listaDetalleCompra);
        listaDetalleCompra = manejador.getLista();
        fireTableDataChanged();
    }
    
}