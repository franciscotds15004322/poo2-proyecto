package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorDetalleFactura;
import org.franciscoestrada.bean.DetalleFactura;

public class ModeloDatosDetalleFactura extends AbstractTableModel{
    private String[] encabezados = {"ID DETALLE","NO. FACTURA","LINEA NO","PRODUCTO","DESCRIPCION","CANTIDAD","PRECIO","TOTAL LINEA"};
    private ArrayList<DetalleFactura> listaDetalleFactura = null;
    private ManejadorDetalleFactura manejador = new ManejadorDetalleFactura();

    public ModeloDatosDetalleFactura() {
        listaDetalleFactura = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaDetalleFactura.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        DetalleFactura elemento = listaDetalleFactura.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getIdDetalle()); 
                break;
            case 1:
                resultado= String.valueOf(elemento.getFactura().getNumeroDeFactura()); 
                break;
            case 2:
                resultado= String.valueOf(elemento.getLineaNo());
                break;
            case 3:
                resultado= elemento.getProducto().getNombreProducto();
                break;
            case 4:
                resultado= elemento.getDescripcionDF();
            break;
            case 5:
                resultado= String.valueOf(elemento.getCantidad()); 
                break;
            case 6:
                resultado= String.valueOf(elemento.getPrecio());
                break;
            case 7:
                resultado= String.valueOf(elemento.getTotalLinea());
                break;                
        }
        return resultado;
    }
      
    public DetalleFactura getElemento(int fila){
        return listaDetalleFactura.get(fila);
    }
    
    //Agregar
    public void agregar(DetalleFactura detalleFactura){
        manejador.agregar(detalleFactura);
        listaDetalleFactura.removeAll(listaDetalleFactura);
        listaDetalleFactura = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaDetalleFactura.get(fila));
        listaDetalleFactura.removeAll(listaDetalleFactura);
        listaDetalleFactura = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(DetalleFactura detalleFactura){
        manejador.modificar(detalleFactura);
        listaDetalleFactura.removeAll(listaDetalleFactura);
        listaDetalleFactura = manejador.getLista();
        fireTableDataChanged();
    }
    
}