package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorDireccionCliente;
import org.franciscoestrada.bean.DireccionCliente;

public class ModeloDatosDireccionCliente extends AbstractTableModel{
    private String[] encabezados = {"NIT","DPI","NOMBRE","DIRECCION","TIPO DE DIRECCION"};
    private ArrayList<DireccionCliente> listaDireccionCliente = null;
    private ManejadorDireccionCliente manejador = new ManejadorDireccionCliente();

    public ModeloDatosDireccionCliente() {
        listaDireccionCliente = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaDireccionCliente.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        DireccionCliente elemento = listaDireccionCliente.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getCliente().getNit()); 
                break;
            case 1:
                resultado= elemento.getCliente().getDpi(); 
                break;
            case 2:
                resultado= elemento.getCliente().getNombre();
                break;
            case 3:
                resultado= elemento.getDireccion();
                break;
            case 4:
                resultado= elemento.getTipoDireccionCliente().getDescripcion();
            break;
        }
        return resultado;
    }
      
    public DireccionCliente getElemento(int fila){
        return listaDireccionCliente.get(fila);
    }
    
    //Agregar
    public void agregar(DireccionCliente direccionCliente){
        manejador.agregar(direccionCliente);
        listaDireccionCliente.removeAll(listaDireccionCliente);
        listaDireccionCliente = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaDireccionCliente.get(fila));
        listaDireccionCliente.removeAll(listaDireccionCliente);
        listaDireccionCliente = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(DireccionCliente direccionCliente){
        manejador.modificar(direccionCliente);
        listaDireccionCliente.removeAll(listaDireccionCliente);
        listaDireccionCliente = manejador.getLista();
        fireTableDataChanged();
    }
    
}

