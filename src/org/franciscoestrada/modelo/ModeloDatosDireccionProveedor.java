package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorDireccionProveedor;
import org.franciscoestrada.bean.DireccionProveedor;

public class ModeloDatosDireccionProveedor extends AbstractTableModel{
    
    private String[] encabezados = {"NIT","NOMBRE","CONTACTO","DIRECCION","TIPO DE DIRECCION"};
    private ArrayList<DireccionProveedor> listaDireccionProveedor = null;
    private ManejadorDireccionProveedor manejador = new ManejadorDireccionProveedor();

    public ModeloDatosDireccionProveedor() {
        listaDireccionProveedor = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaDireccionProveedor.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        DireccionProveedor elemento = listaDireccionProveedor.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getProveedor().getNit()); 
                break;
            case 1:
                resultado= elemento.getProveedor().getNombre(); 
                break;
            case 2:
                resultado= elemento.getProveedor().getContacto();
                break;
            case 3:
                resultado= elemento.getDireccion();
                break;
            case 4:
                resultado= elemento.getTipoDireccionProveedor().getDescripcion();
            break;
        }
        return resultado;
    }
      
    public DireccionProveedor getElemento(int fila){
        return listaDireccionProveedor.get(fila);
    }
    
    //Agregar
    public void agregar(DireccionProveedor direccionProveedor){
        manejador.agregar(direccionProveedor);
        listaDireccionProveedor.removeAll(listaDireccionProveedor);
        listaDireccionProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaDireccionProveedor.get(fila));
        listaDireccionProveedor.removeAll(listaDireccionProveedor);
        listaDireccionProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(DireccionProveedor direccionProveedor){
        manejador.modificar(direccionProveedor);
        listaDireccionProveedor.removeAll(listaDireccionProveedor);
        listaDireccionProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    
}
