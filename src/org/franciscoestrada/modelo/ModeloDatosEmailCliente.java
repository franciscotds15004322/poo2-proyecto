package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorEmailCliente;
import org.franciscoestrada.bean.EmailCliente;

public class ModeloDatosEmailCliente extends AbstractTableModel{
    private String[] encabezados = {"NIT","DPI","NOMBRE","EMAIL","TIPO DE EMAIL"};
    private ArrayList<EmailCliente> listaEmailCliente = null;
    private ManejadorEmailCliente manejador = new ManejadorEmailCliente();

    public ModeloDatosEmailCliente() {
        listaEmailCliente = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaEmailCliente.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        EmailCliente elemento = listaEmailCliente.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getCliente().getNit()); 
                break;
            case 1:
                resultado= elemento.getCliente().getDpi(); 
                break;
            case 2:
                resultado= elemento.getCliente().getNombre();
                break;
            case 3:
                resultado= elemento.getEmail();
                break;
            case 4:
                resultado= elemento.getTipoEmailCliente().getDescripcion();
            break;
        }
        return resultado;
    }
      
    public EmailCliente getElemento(int fila){
        return listaEmailCliente.get(fila);
    }
    
    //Agregar
    public void agregar(EmailCliente emailCliente){
        manejador.agregar(emailCliente);
        listaEmailCliente.removeAll(listaEmailCliente);
        listaEmailCliente = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaEmailCliente.get(fila));
        listaEmailCliente.removeAll(listaEmailCliente);
        listaEmailCliente = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(EmailCliente emailCliente){
        manejador.modificar(emailCliente);
        listaEmailCliente.removeAll(listaEmailCliente);
        listaEmailCliente = manejador.getLista();
        fireTableDataChanged();
    }
    
}

