package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorEmailProveedor;
import org.franciscoestrada.bean.EmailProveedor;

public class ModeloDatosEmailProveedor extends AbstractTableModel{
    
    private String[] encabezados = {"NIT","NOMBRE","CONTACTO","EMAIL","TIPO DE EMAIL"};
    private ArrayList<EmailProveedor> listaEmailProveedor = null;
    private ManejadorEmailProveedor manejador = new ManejadorEmailProveedor();

    public ModeloDatosEmailProveedor() {
        listaEmailProveedor = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaEmailProveedor.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        EmailProveedor elemento = listaEmailProveedor.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getProveedor().getNit()); 
                break;
            case 1:
                resultado= elemento.getProveedor().getNombre(); 
                break;
            case 2:
                resultado= elemento.getProveedor().getContacto();
                break;
            case 3:
                resultado= elemento.getEmail();
                break;
            case 4:
                resultado= elemento.getTipoEmailProveedor().getDescripcion();
            break;
        }
        return resultado;
    }
      
    public EmailProveedor getElemento(int fila){
        return listaEmailProveedor.get(fila);
    }
    
    //Agregar
    public void agregar(EmailProveedor emailProveedor){
        manejador.agregar(emailProveedor);
        listaEmailProveedor.removeAll(listaEmailProveedor);
        listaEmailProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaEmailProveedor.get(fila));
        listaEmailProveedor.removeAll(listaEmailProveedor);
        listaEmailProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(EmailProveedor emailProveedor){
        manejador.modificar(emailProveedor);
        listaEmailProveedor.removeAll(listaEmailProveedor);
        listaEmailProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    
}