package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorEmpaque;
import org.franciscoestrada.bean.Empaque;

public class ModeloDatosEmpaque extends AbstractTableModel{
    
    private String[] encabezados = {"ID.EMPAQUE","DESCRIPCION EMPAQUE"};
    private ArrayList<Empaque> listaEmpaque = null;
    private ManejadorEmpaque manejador = new ManejadorEmpaque();

    public ModeloDatosEmpaque() {
        listaEmpaque = manejador.getLista();
    }
  
   public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaEmpaque.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        Empaque elemento = listaEmpaque.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getIdEmpaque());
            break;
            case 1:
                resultado= elemento.getDescripcionEmpaque();
            break;
        }
        return resultado;
    }
    
    public Empaque getElemento(int fila){
        return listaEmpaque.get(fila);
    }
    
    public void agregar(Empaque empaque){
        manejador.agregar(empaque);
        listaEmpaque.removeAll(listaEmpaque);
        listaEmpaque = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaEmpaque.get(fila));
        listaEmpaque.removeAll(listaEmpaque);
        listaEmpaque = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(Empaque empaque){
        manejador.modificar(empaque);
        listaEmpaque.removeAll(listaEmpaque);
        listaEmpaque = manejador.getLista();
        fireTableDataChanged();
    }
    
}