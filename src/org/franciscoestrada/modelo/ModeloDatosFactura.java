package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorFactura;
import org.franciscoestrada.bean.Factura;

public class ModeloDatosFactura extends AbstractTableModel{
    private String[] encabezados = {"NUMERO DE FACTURA","FECHA","NIT","DPI","NOMBRE","DESCRIPCION","TOTAL"};
    private ArrayList<Factura> listaFactura = null;
    private ManejadorFactura manejador = new ManejadorFactura();

    public ModeloDatosFactura() {
        listaFactura = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaFactura.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        Factura elemento = listaFactura.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getNumeroDeFactura()); 
                break;
            case 1:
                resultado= String.valueOf(elemento.getFecha()); 
                break;                
            case 2:
                resultado= String.valueOf(elemento.getCliente().getNit()); 
                break;
            case 3:
                resultado= elemento.getCliente().getDpi(); 
                break;
            case 4:
                resultado= elemento.getCliente().getNombre();
                break;
            case 5:
                resultado= elemento.getDescripcion();
                break;
            case 6:
                resultado= String.valueOf(elemento.getTotal());
            break;
        }
        return resultado;
    }
      
    public Factura getElemento(int fila){
        return listaFactura.get(fila);
    }
    
    //Agregar
    public void agregar(Factura factura){
        manejador.agregar(factura);
        listaFactura.removeAll(listaFactura);
        listaFactura = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaFactura.get(fila));
        listaFactura.removeAll(listaFactura);
        listaFactura = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(Factura factura){
        manejador.modificar(factura);
        listaFactura.removeAll(listaFactura);
        listaFactura = manejador.getLista();
        fireTableDataChanged();
    }
    
}


