package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorInventario;
import org.franciscoestrada.bean.Inventario;

public class ModeloDatosInventario extends AbstractTableModel{
    private String[] encabezados = {"FECHA","PRODUCTO","TIPO MOVIMIENTO","FACTURA","COMPRA","LINEA","CANTIDAD","PRECIO"};
    private ArrayList<Inventario> listaInventario = null;
    private ManejadorInventario manejador = new ManejadorInventario();

    public ModeloDatosInventario() {
        listaInventario = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaInventario.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
       Inventario elemento = listaInventario.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getFechaInventario()); 
                break;
            case 1:
                resultado= elemento.getProducto().getNombreProducto(); 
                break;                
            case 2:
                resultado= elemento.getTipoMovimiento().getDescripcion(); 
                break;
            case 3:
                resultado= String.valueOf(elemento.getIdFactura()); 
                break;
            case 4:
                resultado= String.valueOf(elemento.getIdCompra());
                break;
            case 5:
                resultado= String.valueOf(elemento.getLineaNoInventario());
                break;
            case 6:
                resultado= String.valueOf(elemento.getCantidad());
            break;
            case 7:
                resultado= String.valueOf(elemento.getPrecioCompraUnitario());
            break;
        }
        return resultado;
    }
      
    public Inventario getElemento(int fila){
        return listaInventario.get(fila);
    }
    
    //Agregar
    public void agregar(Inventario inventario){
        manejador.agregar(inventario);
        listaInventario.removeAll(listaInventario);
        listaInventario = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaInventario.get(fila));
        listaInventario.removeAll(listaInventario);
        listaInventario = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(Inventario inventario){
        manejador.modificar(inventario);
        listaInventario.removeAll(listaInventario);
        listaInventario = manejador.getLista();
        fireTableDataChanged();
    }
    
}

