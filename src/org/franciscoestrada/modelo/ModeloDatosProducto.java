package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorDeProducto;
import org.franciscoestrada.bean.Producto;

public class ModeloDatosProducto extends AbstractTableModel{
    
    private String[] encabezados = {"NOMBRE","DESCR.","P.UNIDAD","P.DOCENA","P.MAYOR","EMPAQUE","CATEGORIA","STOCK"};
    private ArrayList<Producto> listaProducto = null;
    private ManejadorDeProducto manejador = new ManejadorDeProducto();

    public ModeloDatosProducto() {
        listaProducto = manejador.getLista();
    }
  
   public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaProducto.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        Producto elemento = listaProducto.get(fila);
        switch(columna){
             case 0:
                resultado= elemento.getNombreProducto();
            break;
            case 1:
                resultado= elemento.getDescripcionProducto();
                break;
            case 2:
                resultado= String.valueOf(elemento.getPrecioUnitario());
            break;                
            case 3:
                resultado= String.valueOf(elemento.getPrecioPorDocena());
            break;                
            case 4:
                resultado= String.valueOf(elemento.getPrecioPorMayor());
            break;               
            case 5:
                resultado= elemento.getEmpaque().getDescripcionEmpaque();
            break;              
            case 6:
                resultado= elemento.getCategoria().getDescripcionCategoria();
                break;                
            case 7:
                resultado= String.valueOf(elemento.getStock().getStock());
            break;                             
        }
        return resultado;
    }
      
    public Producto getElemento(int fila){
        return listaProducto.get(fila);
    }
    
    //Agregar
    public void agregar(Producto producto){
        manejador.agregar(producto);
        listaProducto.removeAll(listaProducto);
        listaProducto = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaProducto.get(fila));
        listaProducto.removeAll(listaProducto);
        listaProducto = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(Producto producto){
        manejador.modificar(producto);
        listaProducto.removeAll(listaProducto);
        listaProducto = manejador.getLista();
        fireTableDataChanged();
    }
    
}

    