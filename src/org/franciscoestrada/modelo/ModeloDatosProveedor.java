package org.franciscoestrada.modelo;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorDeProveedor;
import org.franciscoestrada.bean.Proveedor;

public class ModeloDatosProveedor extends AbstractTableModel{
    
    private String[] encabezados = {"ID PROVEEDOR","NIT","NOMBRE","PAGINA WEB", "CONTACTO"};
    private ArrayList<Proveedor> listaProveedor = null;
    private ManejadorDeProveedor manejador = new ManejadorDeProveedor();

    public ModeloDatosProveedor() {
        listaProveedor = manejador.getLista();
    }

   public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaProveedor.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        Proveedor elemento = listaProveedor.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getIdProveedor()); 
            break;
            case 1:
                resultado= elemento.getNit();
            break;
            case 2:
                resultado= elemento.getNombre();
                break;
            case 3:
                resultado= elemento.getPaginaWeb();
            break;                
            case 4:
                resultado= elemento.getContacto();
            break;                

        }
        return resultado;
    }
    
    public Proveedor getElemento(int fila){
        return listaProveedor.get(fila);
    }
    
    //Agregar
    public void agregar(Proveedor proveedor){
        manejador.agregar(proveedor);
        listaProveedor.removeAll(listaProveedor);
        listaProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaProveedor.get(fila));
        listaProveedor.removeAll(listaProveedor);
        listaProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(Proveedor proveedor){
        manejador.modificar(proveedor);
        listaProveedor.removeAll(listaProveedor);
        listaProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    
}
