package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorStock;
import org.franciscoestrada.bean.Stock;

public class ModeloDatosStock extends AbstractTableModel{
    
    private String[] encabezados = {"ID.STOCK","STOCK"};
    private ArrayList<Stock> listaStock = null;
    private ManejadorStock manejador = new ManejadorStock();

    public ModeloDatosStock() {
        listaStock = manejador.getLista();
    }
  
   public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaStock.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        Stock elemento = listaStock.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getIdStock());
            break;
            case 1:
                resultado= String.valueOf(elemento.getStock());
            break;
        }
        return resultado;
    }
 
    public Stock getElemento(int fila){
        return listaStock.get(fila);
    }
    
    public void agregar(Stock stock){
        manejador.agregar(stock);
        listaStock.removeAll(listaStock);
        listaStock = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaStock.get(fila));
        listaStock.removeAll(listaStock);
        listaStock = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(Stock stock){
        manejador.modificar(stock);
        listaStock.removeAll(listaStock);
        listaStock = manejador.getLista();
        fireTableDataChanged();
    }
    
}
    