package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorTelefonoCliente;
import org.franciscoestrada.bean.TelefonoCliente;

public class ModeloDatosTelefonoCliente extends AbstractTableModel{
    private String[] encabezados = {"NIT","DPI","NOMBRE","TELEFONO","TIPO DE TELEFONO"};
    private ArrayList<TelefonoCliente> listaTelefonoCliente = null;
    private ManejadorTelefonoCliente manejador = new ManejadorTelefonoCliente();

    public ModeloDatosTelefonoCliente() {
        listaTelefonoCliente = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaTelefonoCliente.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        TelefonoCliente  elemento = listaTelefonoCliente.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getCliente().getNit()); 
                break;
            case 1:
                resultado= elemento.getCliente().getDpi(); 
                break;
            case 2:
                resultado= elemento.getCliente().getNombre();
                break;
            case 3:
                resultado= elemento.getTelefono();
                break;
            case 4:
                resultado= elemento.getTipoTelefonoCliente().getDescripcion();
            break;
        }
        return resultado;
    }
      
    public TelefonoCliente getElemento(int fila){
        return listaTelefonoCliente.get(fila);
    }
    
    //Agregar
    public void agregar(TelefonoCliente telefonoCliente){
        manejador.agregar(telefonoCliente);
        listaTelefonoCliente.removeAll(listaTelefonoCliente);
        listaTelefonoCliente = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaTelefonoCliente.get(fila));
        listaTelefonoCliente.removeAll(listaTelefonoCliente);
        listaTelefonoCliente = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(TelefonoCliente telefonoCliente){
        manejador.modificar(telefonoCliente);
        listaTelefonoCliente.removeAll(listaTelefonoCliente);
        listaTelefonoCliente = manejador.getLista();
        fireTableDataChanged();
    }
    
}

