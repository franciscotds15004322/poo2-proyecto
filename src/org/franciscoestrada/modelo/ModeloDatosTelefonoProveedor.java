package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorTelefonoProveedor;
import org.franciscoestrada.bean.TelefonoProveedor;

public class ModeloDatosTelefonoProveedor extends AbstractTableModel{
    
    private String[] encabezados = {"NIT","NOMBRE","CONTACTO","TELEFONO","TIPO DE TELEFONO"};
    private ArrayList<TelefonoProveedor> listaTelefonoProveedor = null;
    private ManejadorTelefonoProveedor manejador = new ManejadorTelefonoProveedor();

    public ModeloDatosTelefonoProveedor() {
        listaTelefonoProveedor = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaTelefonoProveedor.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        TelefonoProveedor elemento = listaTelefonoProveedor.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getProveedor().getNit()); 
                break;
            case 1:
                resultado= elemento.getProveedor().getNombre(); 
                break;
            case 2:
                resultado= elemento.getProveedor().getContacto();
                break;
            case 3:
                resultado= elemento.getTelefono();
                break;
            case 4:
                resultado= elemento.getTipoTelefonoProveedor().getDescripcion();
            break;
        }
        return resultado;
    }
      
    public TelefonoProveedor getElemento(int fila){
        return listaTelefonoProveedor.get(fila);
    }
    
    //Agregar
    public void agregar(TelefonoProveedor telefonoProveedor){
        manejador.agregar(telefonoProveedor);
        listaTelefonoProveedor.removeAll(listaTelefonoProveedor);
        listaTelefonoProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaTelefonoProveedor.get(fila));
        listaTelefonoProveedor.removeAll(listaTelefonoProveedor);
        listaTelefonoProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(TelefonoProveedor telefonoProveedor){
        manejador.modificar(telefonoProveedor);
        listaTelefonoProveedor.removeAll(listaTelefonoProveedor);
        listaTelefonoProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    
}
