package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorTipoDireccionCliente;
import org.franciscoestrada.bean.TipoDireccionCliente;

public class ModeloDatosTipoDireccionCliente extends AbstractTableModel{
    
    private String[] encabezados = {"ID","TIPO"};
    
    private ArrayList<TipoDireccionCliente> listaTipoDireccionCliente = null;
    private ManejadorTipoDireccionCliente manejador = new ManejadorTipoDireccionCliente();

    public ModeloDatosTipoDireccionCliente() {
        listaTipoDireccionCliente = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaTipoDireccionCliente.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        TipoDireccionCliente elemento = listaTipoDireccionCliente.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getIdTipo()); 
                break;
            case 1:
                resultado= elemento.getDescripcion(); 
                break;
        }
        return resultado;
    }
      
    public TipoDireccionCliente getElemento(int fila){
        return listaTipoDireccionCliente.get(fila);
    }
    
    public void agregar(TipoDireccionCliente tipoDireccionCliente){
        manejador.agregar(tipoDireccionCliente);
        listaTipoDireccionCliente.removeAll(listaTipoDireccionCliente);
        listaTipoDireccionCliente = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaTipoDireccionCliente.get(fila));
        listaTipoDireccionCliente.removeAll(listaTipoDireccionCliente);
        listaTipoDireccionCliente = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(TipoDireccionCliente tipoDireccionCliente){
        manejador.modificar(tipoDireccionCliente);
        listaTipoDireccionCliente.removeAll(listaTipoDireccionCliente);
        listaTipoDireccionCliente = manejador.getLista();
        fireTableDataChanged();
    }
    
}
