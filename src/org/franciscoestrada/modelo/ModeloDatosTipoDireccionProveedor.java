package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorTipoDireccionProveedor;
import org.franciscoestrada.bean.TipoDireccionProveedor;

public class ModeloDatosTipoDireccionProveedor extends AbstractTableModel{
    
    private String[] encabezados = {"ID","TIPO"};
    
    private ArrayList<TipoDireccionProveedor> listaTipoDireccionProveedor = null;
    private ManejadorTipoDireccionProveedor manejador = new ManejadorTipoDireccionProveedor();

    public ModeloDatosTipoDireccionProveedor() {
        listaTipoDireccionProveedor = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaTipoDireccionProveedor.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        TipoDireccionProveedor elemento = listaTipoDireccionProveedor.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getIdTipo()); 
                break;
            case 1:
                resultado= elemento.getDescripcion(); //String.valueOf(elemento.getNit()); si fuera string
                break;
        }
        return resultado;
    }
      
    public TipoDireccionProveedor getElemento(int fila){
        return listaTipoDireccionProveedor.get(fila);
    }
    
    public void agregar(TipoDireccionProveedor tipoDireccionProveedor){
        manejador.agregar(tipoDireccionProveedor);
        listaTipoDireccionProveedor.removeAll(listaTipoDireccionProveedor);
        listaTipoDireccionProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaTipoDireccionProveedor.get(fila));
        listaTipoDireccionProveedor.removeAll(listaTipoDireccionProveedor);
        listaTipoDireccionProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(TipoDireccionProveedor tipoDireccionProveedor){
        manejador.modificar(tipoDireccionProveedor);
        listaTipoDireccionProveedor.removeAll(listaTipoDireccionProveedor);
        listaTipoDireccionProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    
}
