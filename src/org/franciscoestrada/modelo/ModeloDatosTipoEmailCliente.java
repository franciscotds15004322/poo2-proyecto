package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorTipoEmailCliente;
import org.franciscoestrada.bean.TipoEmailCliente;

public class ModeloDatosTipoEmailCliente extends AbstractTableModel{
    
    private String[] encabezados = {"ID","TIPO"};
    
    private ArrayList<TipoEmailCliente> listaTipoEmailCliente = null;
    private ManejadorTipoEmailCliente manejador = new ManejadorTipoEmailCliente();

    public ModeloDatosTipoEmailCliente() {
        listaTipoEmailCliente = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaTipoEmailCliente.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        TipoEmailCliente elemento = listaTipoEmailCliente.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getIdTipo()); 
                break;
            case 1:
                resultado= elemento.getDescripcion(); 
                break;
        }
        return resultado;
    }
      
    public TipoEmailCliente getElemento(int fila){
        return listaTipoEmailCliente.get(fila);
    }
    
    public void agregar(TipoEmailCliente tipoEmailCliente){
        manejador.agregar(tipoEmailCliente);
        listaTipoEmailCliente.removeAll(listaTipoEmailCliente);
        listaTipoEmailCliente = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaTipoEmailCliente.get(fila));
        listaTipoEmailCliente.removeAll(listaTipoEmailCliente);
        listaTipoEmailCliente = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(TipoEmailCliente tipoEmailCliente){
        manejador.modificar(tipoEmailCliente);
        listaTipoEmailCliente.removeAll(listaTipoEmailCliente);
        listaTipoEmailCliente = manejador.getLista();
        fireTableDataChanged();
    }
    
}
