package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorTipoEmailProveedor;
import org.franciscoestrada.bean.TipoEmailProveedor;

public class ModeloDatosTipoEmailProveedor extends AbstractTableModel{
    
    private String[] encabezados = {"ID","TIPO"};
    
    private ArrayList<TipoEmailProveedor> listaTipoEmailProveedor = null;
    private ManejadorTipoEmailProveedor manejador = new ManejadorTipoEmailProveedor();

    public ModeloDatosTipoEmailProveedor() {
        listaTipoEmailProveedor = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaTipoEmailProveedor.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        TipoEmailProveedor elemento = listaTipoEmailProveedor.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getIdTipo()); 
                break;
            case 1:
                resultado= elemento.getDescripcion(); 
                break;
        }
        return resultado;
    }
      
    public TipoEmailProveedor getElemento(int fila){
        return listaTipoEmailProveedor.get(fila);
    }
    
    public void agregar(TipoEmailProveedor tipoEmailProveedor){
        manejador.agregar(tipoEmailProveedor);
        listaTipoEmailProveedor.removeAll(listaTipoEmailProveedor);
        listaTipoEmailProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaTipoEmailProveedor.get(fila));
        listaTipoEmailProveedor.removeAll(listaTipoEmailProveedor);
        listaTipoEmailProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(TipoEmailProveedor tipoEmailProveedor){
        manejador.modificar(tipoEmailProveedor);
        listaTipoEmailProveedor.removeAll(listaTipoEmailProveedor);
        listaTipoEmailProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    
}
