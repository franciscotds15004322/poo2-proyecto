package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorTipoMovimiento;
import org.franciscoestrada.bean.TipoMovimiento;

public class ModeloDatosTipoMovimiento extends AbstractTableModel{
    
    private String[] encabezados = {"ID.TIPO MOVIMIENTO","DESCRIPCION"};
    private ArrayList<TipoMovimiento> listaTipoMovimiento = null;
    private ManejadorTipoMovimiento manejador = new ManejadorTipoMovimiento();

    public ModeloDatosTipoMovimiento() {
        listaTipoMovimiento = manejador.getLista();
    }
  
   public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaTipoMovimiento.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        TipoMovimiento elemento = listaTipoMovimiento.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getIdTipoMovimiento());
            break;
            case 1:
                resultado= elemento.getDescripcion();
            break;
        }
        return resultado;
    }
      
    public TipoMovimiento getElemento(int fila){
        return listaTipoMovimiento.get(fila);
    }
    
    public void agregar(TipoMovimiento tipoMovimiento){
        manejador.agregar(tipoMovimiento);
        listaTipoMovimiento.removeAll(listaTipoMovimiento);
        listaTipoMovimiento = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaTipoMovimiento.get(fila));
        listaTipoMovimiento.removeAll(listaTipoMovimiento);
        listaTipoMovimiento = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(TipoMovimiento tipoMovimiento){
        manejador.modificar(tipoMovimiento);
        listaTipoMovimiento.removeAll(listaTipoMovimiento);
        listaTipoMovimiento = manejador.getLista();
        fireTableDataChanged();
    }
    
}