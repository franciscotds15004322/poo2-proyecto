package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorTipoTelefonoCliente;
import org.franciscoestrada.bean.TipoTelefonoCliente;

public class ModeloDatosTipoTelefonoCliente extends AbstractTableModel{
    
    private String[] encabezados = {"ID","TIPO"};
    
    private ArrayList<TipoTelefonoCliente> listaTipoTelefonoCliente = null;
    private ManejadorTipoTelefonoCliente manejador = new ManejadorTipoTelefonoCliente();

    public ModeloDatosTipoTelefonoCliente() {
        listaTipoTelefonoCliente = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaTipoTelefonoCliente.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        TipoTelefonoCliente elemento = listaTipoTelefonoCliente.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getIdTipo()); 
                break;
            case 1:
                resultado= elemento.getDescripcion(); 
                break;
        }
        return resultado;
    }
      
    public TipoTelefonoCliente getElemento(int fila){
        return listaTipoTelefonoCliente.get(fila);
    }
    
    public void agregar(TipoTelefonoCliente tipoTelefonoCliente){
        manejador.agregar(tipoTelefonoCliente);
        listaTipoTelefonoCliente.removeAll(listaTipoTelefonoCliente);
        listaTipoTelefonoCliente = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaTipoTelefonoCliente.get(fila));
        listaTipoTelefonoCliente.removeAll(listaTipoTelefonoCliente);
        listaTipoTelefonoCliente = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(TipoTelefonoCliente tipoTelefonoCliente){
        manejador.modificar(tipoTelefonoCliente);
        listaTipoTelefonoCliente.removeAll(listaTipoTelefonoCliente);
        listaTipoTelefonoCliente = manejador.getLista();
        fireTableDataChanged();
    }
    
}
