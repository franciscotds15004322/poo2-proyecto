package org.franciscoestrada.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.franciscoestrada.manejadores.ManejadorTipoTelefonoProveedor;
import org.franciscoestrada.bean.TipoTelefonoProveedor;

public class ModeloDatosTipoTelefonoProveedor extends AbstractTableModel{
    
    private String[] encabezados = {"ID","TIPO"};
    
    private ArrayList<TipoTelefonoProveedor> listaTipoTelefonoProveedor = null;
    private ManejadorTipoTelefonoProveedor manejador = new ManejadorTipoTelefonoProveedor();

    public ModeloDatosTipoTelefonoProveedor() {
        listaTipoTelefonoProveedor = manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezados[columna];
    }
    public int getColumnCount(){
        return encabezados.length;
    }
    public int getRowCount(){
        return listaTipoTelefonoProveedor.size();
    }
    public String getValueAt(int fila, int columna){
        String resultado ="";
        TipoTelefonoProveedor elemento = listaTipoTelefonoProveedor.get(fila);
        switch(columna){
            case 0:
                resultado= String.valueOf(elemento.getIdTipo()); 
                break;
            case 1:
                resultado= elemento.getDescripcion(); 
                break;
        }
        return resultado;
    }
      
    public TipoTelefonoProveedor getElemento(int fila){
        return listaTipoTelefonoProveedor.get(fila);
    }
    
    public void agregar(TipoTelefonoProveedor tipoTelefonoProveedor){
        manejador.agregar(tipoTelefonoProveedor);
        listaTipoTelefonoProveedor.removeAll(listaTipoTelefonoProveedor);
        listaTipoTelefonoProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    // Eliminar
    public void eliminar(int fila){
        manejador.eliminar(listaTipoTelefonoProveedor.get(fila));
        listaTipoTelefonoProveedor.removeAll(listaTipoTelefonoProveedor);
        listaTipoTelefonoProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    // Modificar
    public void modificar(TipoTelefonoProveedor tipoTelefonoProveedor){
        manejador.modificar(tipoTelefonoProveedor);
        listaTipoTelefonoProveedor.removeAll(listaTipoTelefonoProveedor);
        listaTipoTelefonoProveedor = manejador.getLista();
        fireTableDataChanged();
    }
    
}
