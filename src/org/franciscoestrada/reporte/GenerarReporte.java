package org.franciscoestrada.reporte;

import java.io.InputStream;
import java.util.Map;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.franciscoestrada.db.ConexionDB;

public class GenerarReporte {

    private static GenerarReporte instancia;
    public GenerarReporte() {
    }
    
    public static GenerarReporte getInstancia(){
        if(instancia == null){
            instancia = new GenerarReporte();
        }
        return instancia;
    }
    public void generarReporte(Map parametros, String nombreReporte, String titulo){
        InputStream reporte = GenerarReporte.class.getResourceAsStream(nombreReporte);
        try{
            JasperReport reporteMaestro = null;
            reporteMaestro = (JasperReport)JRLoader.loadObject(reporte);
            JasperPrint reporteVisual = JasperFillManager.fillReport(reporteMaestro, parametros, 
                    ConexionDB.getInstancia().getConexion());
            JasperViewer visor = new JasperViewer(reporteVisual,false);
            visor.setTitle(titulo);
            visor.setVisible(true);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
}
