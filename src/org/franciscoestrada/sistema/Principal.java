package org.franciscoestrada.sistema;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.franciscoestrada.iu.VentanaPrincipal;

public class Principal {   
    public static void main(String args[]){
        SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin.BusinessBlueSteelSkin");
        new VentanaPrincipal();
    }
} 
